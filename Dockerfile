FROM ubuntu:18.10

# Segment the RUN steps to enable better caching

# Build deps
RUN apt-get update \
    && apt-get install -y -qq --no-install-recommends \
        cmake \
        g++ \
        make

# Qt deps
RUN apt-get install -y -qq --no-install-recommends \
        qt5-default \
        qtbase5-dev \
        qttools5-dev

# Container GUI / X-server deps
RUN apt-get install -y -qq --no-install-recommends \
	xvfb

# Install tools for grabbing tarballs and required certificates for secure authentication
RUN apt-get install -y -qq --no-install-recommends \
    ca-certificates \
    wget \ 
    curl

# Get the YAML library from Git Hub releases - version 0.6.2
RUN wget -c https://github.com/jbeder/yaml-cpp/archive/yaml-cpp-0.6.2.tar.gz -O - | tar -xz \
    && cd yaml-cpp-yaml-cpp-0.6.2 && mkdir build && cd build \
    && cmake .. && make && make install

