# stpa-documentation-tool

This tool aims to organize STPA analysis results in a tree format, and then produce analysis documentation automatically. The numbering of item references and cross references is handled automatically by the tool, greatly reducing rework.

The options for running this tool are presented below; the most convenient are presented first.

## Run under flatpak

### Installing and running the application

First, install flatpak from the official [flatpak website][3].

Second, install this application using the  [flathub application browser][4].

Simply launch the application from your desktop browser.

#### Installing and Running from the command line

_The application flatpak can also be installed from CLI, if preferred_ 

    flatpak install flathub io.trustable.stpadocumentationtool
 
_Also, you can run the application directly from the command line with_

    flatpak run io.trustable.stpadocumentationtool
    
#### Updates

Updates are available from the flathub website, and also may be automatically offered to you by your linux distribution.

_You can check for updates manually from the command line_

    flatpak update io.trustable.stpadocumentationtool

## Command line build instructions (Debian / Ubuntu Linux)

_If you are running a version of Debian older than Buster or a version of Ubuntu older than 18.10, you will not have access to the required packages. The alternative is to build in a Docker container, described below_

### Build locally from CLI (Debian >= Buster, Ubuntu >= 18.10)

To build requirements are git, build-essentials, cmake, and Qt5 build dependencies. The following instructions will:

  - Install the build requirements
  - clone the stpa tool repository to your home drive
  - build the stpa tool
 
 Execute the following instructions in a terminal:
 
    sudo apt update
    sudo apt install git build-essential cmake qtbase5-dev qt5-default
    cd ~
    git clone https://gitlab.com/trustable/stpa-documentation-tool.git
    cd stpa-documentation-tool
    sudo ./localbuild.sh

#### Updating the software version

When a new version of the tool is available in master, the following instructions will pull and build it:

    cd ~/stpa-documentation-tool
    git pull
    sudo ./localbuild.sh

#### Running the tool 

The tool can be launched with:

    ./stpa_documentation_tool.sh

### Build in a Docker Container


Building in a Docker container will require as a minimum Docker and Git. The following commands will:

 - Install the docker and git,
 - clone the stpa tool repository to your home drive
 - Pull an Ubuntu 18.10 docker image and install the stpa tool build requirements on it
 - Run the docker container which will build the stpa tool

_If you are installing docker, or pulling Ubuntu 18.10, for the first time, this setup procedure will take a few minutes. Future updates will build faster because the image will be cached_

    sudo apt update && sudo apt install docker git
    cd ~
    git clone https://gitlab.com/trustable/stpa-documentation-tool.git
    cd stpa-documentation-tool
    sudo ./dockerlocalbuild.sh

#### Updating the software version

This requires to pull the latest version of master from Git, and rebuild.

    cd ~/stpa-documentation-tool
    git pull
    sudo ./dockerlocalbuild.sh

#### Running the tool within DOCKER

_The script requires sudo to share folders, add docker to the x-server group and to run docker itself (unless you have added yourself to the docker group). It will make no changes outside of the repo folder._

*Save your files in the SaveDirectory folder: This will be accessible from the repo folder in your host*

The executable file will now be located in the build folder, and can be executed by e.g.

    sudo ./dockerlocalrun.sh

## Building with Qt Creator

Obtain the version of Qt Creator applicable to your platform from the [Qt website][1] (the open source version will work). Create a suitable folder to hold the source code, and git clone the repository into it from

    https://gitlab.com/trustable/stpa-documentation-tool.git

 - Launch Qt Creator, 
 - choose 'Open File or Project...' from the file menu. 
 - Navigate to the source code folder and select the file 'CMakeLists.txt'. 

The project should open with the title stpa_documentation_tool; the software can be built and run by pressing the green 'Play' icon on the bottom left of the interface.

## Notes on usage

Full documentation on usage is provided in this repository, in UserGuide.md. Simply navigate to this file in your browser and read all about how to use the Stpa Documentation tool.

This software uses [yaml-cpp][5], an Open Source YAML parser by Jesse Beder (refer to LICENSE file for copyright details.)

This product is supplied as source code with no warranty express or implied, under the BSD software license - All Codethink and Qt copyright notices must be included in any subsequent redistribution of this software, in either source code or binary format.

[1]:https://www.qt.io/download
[2]:https://cmake.org/download/
[3]:https://flatpak.org/setup/
[4]:https://flathub.org/apps
[5]:https://github.com/jbeder/yaml-cpp


