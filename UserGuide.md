# STPA Documentation Tool User Guide
## Introduction

The STPA documentation tool is designed to simplify the creation of documentation resulting from the STPA process.

To that end, the tool is designed to provide a hierarchical method for organsing the data - and ultimately presenting it.

This guide does not aim to describe the STPA process. However, we will describe the data types used in the tool in order to clarify exactly how they relate to 'textbook' STPA analysis - and by textbook analysis, we mean the techniques presented in the STPA Handbook, section 2.

### Analysis data

This section describes the different data-types as used in the STPA documentation tool. These types to cover the core data common to all STPA analyses and the representation of scenarios used here.

#### Core data: Losses, Hazards, and (Unsafe) Control Actions

The STPA process typically produces a list of Losses, which are high level outcomes that are unacceptable to the relevant stakeholders; Hazards, which are events that lead to those unacceptable outcomes; and Unsafe Control Actions, which, in the right scenario, cause those Hazards.

Unsafe Control Actions (UCAs) fall into four categories: Unsafe by providing, unsafe by not providing, unsafe by providing too early / late / out of order, unsafe by application too short or too long. However, all of these are unsafe control actions, and therefore are the same type of data.

#### Representation of the Loss Scenario

Having identified Losses, Hazards, and UCAs, the next step is to identify scenarios in which each given UCA could cause a hazard. These need to be described and presented by the STPA document. 

The format in which these scenarios may be presented is not actually standardized: In this STPA tool the data can include the following information:

 - a description of the actual 'true' physical situation
 - an erroneous controller 'belief'
 - A cause of that belief, such as incorrect 'feedback' (or algorithm failure)
 - A description of 'how' that feedback or failure could occur
 - A 'requirement' on the system that prevents the 'how' scenario from occurring, given the 'true' situation.
 
And so this describes the situation outside the controller (true), the situation internal to the controller (belief), what data / event lead to that internal situation (feedback), and how that event could occur. Together, these describe what lead to an unsafe control action (UCA).
 
Also possible is the category where an _unsafe action_ occurs, not as the result of an unsafe _control_ action, but rather as the result of a correct control action (CA) which is implemented incorrectly: e.g. the hardware system could apply an action when no action was required and was not  requested by the controller.

#### Other data presentations

New methods of representing the loss scenario are being developed all the time, however, this software cannot accommodate all of them. The advantage of a software tool is that by standardizing data format and presentation it makes application of the technique simpler and comparable; as new methods are developed, they will be incorporated into the tool and the documentation updated.

## Creating STPA files (File Menu)

This tool stores STPA data as a YAML file (.yml). The tool can output data in a range of formats, including markdown.

When the tool is opened, an basic STPA data tree will appear - to revert to this basic tree use the 'New File' function. Once data has been entered into the tree, it can be stored using 'Save File...', where a name for the file can be entered and it will be saved with a '.yml' extension.

The data can be retrieved with the 'Load File...' function, and will be exactly the same as it was stored: the editor is designed to prevent the entry of data that cannot be stored.

It is possible, but strongly **not recommended** to edit the save file outside of the tool - if you have recommendations for more efficient methods of data entry, please feel free to contact the development team, raise an issue, or contribute to this open source project.

#### Legacy load

Before version 1.0, there was a file format called .stpa that did not support the full features of the editor as it is now. 

If you have files stored in that format, load them using 'Legacy Load...' - the data in the file will automatically be converted to the new format, and can then be saved down using 'Save...' as normal.

## Entering data into the tool (Actions menu)

The data entry into the tool follows a hierarchical format: Every data type inserts a child of a specific format by standard. This encourages the user to follow the standard format. However, it is possible to override this and insert objects of a specific type.

### Insert Child and Insert Row

For each category of data, 'Insert Child' will insert the expected data type that refers to that type of parent, for example
 
  - Losses: _inserts_ Loss
  - Hazards: _inserts_ Hazard
  - Controls: _inserts_ Control
  - Control: _inserts_ Unsafe Control Action
  - Unsafe Control Action: _inserts_ True Statement
  ... and so on

Insert Row will insert an object of the default type for the currently selected object's parent, at the same level in the hierarchy, beneath the existing siblings. 

### Losses and Hazards

Losses are entered as described above. Losses have no place in the Controls hierarchy, but rather are _referred to_ by Hazards - multiple controls have loss scenarios that can result in a Hazard that engenders a Loss. Each 'Loss' should be caused by at least one Hazard. 

Hazards, likewise, do not sit above or below Controls, because multiple Controls can cause the same Hazard and each Control can cause multiple Hazards. Hazards are _referred to_ by Unsafe Control Actions.

### Controls

This is the section in which the different Controls are listed. A 'Control' may be freely entered as a child of the 'Controls' section header.

Each Control may be completely independent of the other controls, and actually may be initiated by one or more controllers. Any interaction between controllers, or controls, is accounted for by the Scenario description. 

### Unsafe Control Actions

For each Control, the user must identify Unsafe Control Actions. The user should make it clear from the UCA wording to which of the four categories of UCA that the UCA belongs.

UCAs are unique within each Control. Multiple loss scenarios may exist, with unique or non-unique physical 'True Statements', but UCAs are never duplicated.

### Loss Scenarios: True Statement, Belief, Cause, Feedback, How This Could Occur

These data items together describe a loss scenario. It may be considered that some of these items are unnecessary, in which case they can be left blank or omitted by inserting a custom child.

### Editing Data Fields

The first column of the tree view is intended to store the STPA data.  Editable fields are updated by double clicking on them, or by selecting them and pressing 'F2'. The data is committed by pressing 'Enter'.

The second column (reference number) and fourth column (comment) are manually editable, but the third column is not.

If the data reverts to its previous value when 'Enter' is pressed this is because the cell is _not manually editable_ or _the entered data is invalid_.

### Editor Menu

#### Speed Entry Mode

When Speed Entry mode is active, committing data results in the cursor moving down to the next visible cell that is expanded in the hierarchy, and this next cell being opened for data entry. Non-expanded rows are skipped.

Normally, when data is entered into a cell, the cell editor closes and the cursor remains on that cell. Speed Entry mode can be enabled and disabled using the 'Speed Entry' checkbox in the Editor menu.

#### Auto Display Reference Options

By default, this feature is disabled, the 'Auto Display Reference Options' feature can be enabled in the 'Editor' menu. When active, any valid reference targets for the currently selected item will automatically appear in the lower portion of the view without needing to select 'Add Remove / References' from the context menu.

References and their function are explained below.

#### Custom Hierarchy: Insert Custom Children & Insert Custom Peer Rows

In order to support simplified STPA analysis and arbitrary data hierarchies, the editor allows the user to insert a range of data types for *some* of the object types. 

By default, the insertion of non-standard *child* types is enabled; both the insertion of non-standard *child* and *peer* types can be enabled and disabled using the check box in the 'Editor' drop-down menu.

|Inserter | Types Inserted |
| --- | --- |
|Controls | Control *only* |
|Control | Control Action *or* Unsafe Control Action |
|UCA | True Statement, Belief, Cause, Feedback, How, Requirement | 
| CA | True Statement, Belief, Cause, Feedback, How, Requirement |
| True Statement | True Statement, Belief, Cause, Feedback, How, Requirement |
| Belief | True Statement, Belief, Cause, Feedback, How, Requirement  |
| Cause | True Statement, Belief, Cause, Feedback, How, Requirement |
| Feedback | True Statement, Belief, Cause, Feedback, How, Requirement |
| How | True Statement, Belief, Cause, Feedback, How, Requirement |
| Requirement | N/ A |

The insertion of non-standard types is achieved using the Actions menu, or the right-click context menu.

## Reference Numbers, Links and References

All of the features described in this section are accessed by selecting an item and opening the right-click context menu.

### Reference Numbers

As each data item is created, it will be assigned a unique reference ID, for example [L-1] or [UCA-3.22]. This allows the final documentation to be formatted and referred to in a clear, consistent, and unambiguous manner.

Reference numbers must remain unique for some data types, but for others the item can be 'linked' to another item of the same type, described below.

The reference number will be assigned will always be +1 greater than the highest reference number of that class _currently in use_. For example, if three items are created with reference numbers [H-1], [H-2] and [H-3], but then [H-2] is deleted and a new item added, then the new item will be assigned to number [H-4] - this is intentional.

Old numbers can be 'back-filled', or ranges skipped, by renumbering an object.

Because UCAs and CAs exist uniquely in the context of their parent control, the main number assigned in the reference is the number of the parent. For example, [UCA-3.22] is the 22nd unsafe control action of control [C-3].

#### Renumbering a reference number

A reference is renumbered by editing it. It must be renumbered to a reference of the same type, for example, [L-2] --> [L-5]; the new reference number must not be already in use, and must be smaller than 10000. 

_If two items share the same reference number, then they are linked, see below._

 Suppose we wish to renumber [H-9] to be [H-6]: the data can be entered either as
 
   - just a number: 6
   - a letter-hyphen-number: H-6
   - the full reference with brackets: [H-6]
  ... all other formats are invalid.
  
Where a UCA or CA is renumbered, double clicking in and entering a number renumbers the UCA of CA part of the number. For example, 

  - [UCA-6.22] is edited: user enters 24 --> reference becomes [UCA-6.24]

This is because it is the UCA that was renumbered, _not_ the parent control. When a control is numbered, the references of the dependent UCAs and CAs are updated.

### Links

Linking two data items of the same type causes them to share Item data and reference numbers. 

#### Making or breaking a link

Right click on the object to be linked, and then double-click a linkage target from the lower window in the tool. If the object was already linked, the other linked items will not be affected.

If the option to link is greyed-out in the context menu, then it is not applicable to this type.

#### Detailed description of Links

When one item is linked to another, the Item Data of the object linked will be updated to match that of the target object that it is linked to, and its reference number will also be updated to match. 

Any number of items may be linked. When the item data of a linked item is updated, _all_ linked items will be updated. Likewise, if the reference number of a linked item is changed, all linked items will be renumbered to match.

Linked objects exist independently: one can be deleted without affecting the others, they have unique and independent locations in the data tree hierarchy, and can have completely different child data items.

An item link can be broken, in which case the tool will attempt to restore the previous data and reference number that the item had during _this session_ (previous data is not saved in the save file) - if this is not possible [No Data] and a new reference number will be assigned.

### References

Some item types can make reference to other types. Typically, the references made by an object will appear alongside it in the analysis document.

#### Making or removing reference

Right-click on the object that will make the reference, and double-click the target in the lower window of the editor to add or remove a reference target; one can also directly check the check box.

If the 'add / remove' reference option is greyed-out, then it is not available for this data type.

#### Detailed description of references

References allow the STPA document produced to have traceability between unsafe control actions and their consequences. 

This includes:

 - Hazards must refer to at least one Loss
 - Each UCA must refer to at least one Hazard
 
The tool allows for
 
   - Making references only to the appropriate class, and preventing other assignments
   - Automatically renumbering the references made if the target is renumbered
   - Deleting references made to any target that is itself deleted
    
Automatic renumbering means that extensive changes can be made to the numbering of the hazards without having to update the UCA / CA data.

## View menu

This menu contains functions to manipulate the presentation of the data within the editor.

### Expand functions

#### Expand All

Expand all will expand every item in the entire tree.

#### Collapse All

Will collapse the tree to the three root items: Losses, Hazards, Controls.

#### Expand View to and Highlight...

Select an item type in the sub-menu to the right, and the view depth will be expanded to show all objects of that type in the data tree. Items deeper down than the selected type will not be collapsed; using the 'Collapse All' function first will achieve that effect if desired. Additionally, the selected type will be highlighted.

### Highlight functions

#### Apply highlighting to...

Without altering the current expansion state of the tree, highlighting will be applied to all objects of the type selected from the sub-menu on the right, which shows the current highlighting state using check-boxes.

#### Clear highlighting

Clears the highlighting from all items.

## Output STPA

This menu contains functions to output the analysis document, and to control the formatting of that document.

### Markdown

#### Write Handbook Full Markdown...

This function outputs all data in the data tree apart from comments. It contains all levels of the data tree, and indents and formats data to achieve a clear human-readable format.

This format closely resembles the format used on page 48 of the 'STPA Handbook' when the hierarchy is composed of 'standard' child items. 

However, if the user has built the data tree using custom inserts, then the format will be presented according to how the user has laid out their data tree. (Future versions of this software may include flexible formatting options). 

#### Write  Requirements as Markdown...

This function will list out the requirements from the tree, in ascending order of reference number, and nothing else.

## Renumber Menu

There is only one option in this menu - it is segregated so that it is not used accidentally.

### Renumber this Category

This function will renumber *all* items in the reference category of the currently selected item. It will start with the first object of that type in the data tree, renumbering it to '1', and then all items in the tree of that category will be numbered in ascending order.

#### Reference 'Category'

A 'Category' of reference items are all references with the same type and parent number. For example, [L-1], [L-2], [L-5] are all in the same category, as are [BEL-1], [BEL-4], [BEL-7] and so are [UCA-6.1], [UCA-6.3], [UCA-6-4]; but [UCA-1.1] and [UCA-2.1] are NOT in the same category because they differ in parent number.