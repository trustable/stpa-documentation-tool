#!/bin/sh

xhost +local:docker
docker run --rm -e DISPLAY=unix$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v$(pwd):/home/build stpa-builder /bin/sh -c "cd /home/build; ./stpa_documentation_tool"

