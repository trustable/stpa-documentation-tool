## Flathub hosting

To host on flathub, the user must provide a flatpak manifest that can be built by Flathub. The manifest, must therefore contain

 - 1) .appdata.xml file (including a screen shot url, the url must be valid and have a .png file!)
 - 2) .desktop file
 - 3) icon 48x48
 - 4) icon 64x64
 - 5) icon 128x128

Additionally, the .appdata file must point to

 - 6) a screenshot

Non of these files should be in the flathub repo, but the manifest in the flathub repo should target the resources in this folder.

 - The manifest .json file here is maintained as a copy of flathub/io.trustable.stpadocumentationtool - it is info only.
 - The share folder represents contents that must be installed in the FLATPAK at /app - this is dictated through the manifest
 _NOTE: this is a non-standard location for a normal desktop installation._
 - The .png file is targetted by the .appdata.xml file in share.

 _If the stable links referenced by the active manifest in the [flathub repository][2] are broken, the application distribution will break, so please add news tags rather than updating existing ones when making a new release._

All of these resources must be stable - currently tag v1.0 - if they are moved in a future tag e.g. v1.1, then the flathub manifest url paths must be updated, in addition to stable tag reference to this repository itself.

### Making new releases

New releases are made by committing an updated manifest to the [flathub repositiory][2].

### Useful links:
 - Get the application from [flathub][1]
 - Maintain the [flathub repository][2]

_Work TODO: it may be possible to simplify the manifest by having metafiles installed directly by the cmake, however, this would have to be done in such a way as to not conflict with users using 'make install' from the command line. It is a nice to have, not a requirement - whereas upstreaming resources is a requirement._

[1]:https://flathub.org/apps/details/io.trustable.stpadocumentationtool
[2]:https://github.com/flathub/io.trustable.stpadocumentationtool
