#!/bin/sh

# This script will run locally on the CLI or in the docker, and conduct the build / clean up
# process for local builds. This script is not used by the CI.

# We take an argument to differentiate between local docker builds other builds
BUILDTYPE=${1:-DEFAULT}

echo Running local build with parameters: $BUILDTYPE

rm -fr stpa_documentation_tool lib

# Allow passing of a build parameter to the underlying cmake file
if ([ $BUILDTYPE = "DOCKER" ]) then
echo Running build as local Docker build

# Further actions required by the cmake can be entered here
cmake . -DBUILD=NO_UNIT_TESTS -Bbuild

else
# Take no special action - assume all build dependencies and runtime libraries are available
cmake . -DBUILD=DEFAULT -Bbuild
fi

# Actually make the build
cmake --build build

# If an installation is required
if ([ $BUILDTYPE = "INSTALL" ]) then
echo Installing the executable to /usr/local/bin
cd build
make install
cd ..
fi

# Copy out the executable
cp ./build/src/stpa_documentation_tool ./

exit
