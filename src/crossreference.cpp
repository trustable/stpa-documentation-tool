/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#include "crossreference.h"

CrossReference::CrossReference(const QVector<QVariant> &data,
                               const QString &derivedType,
                               TreeItem *parent,
                               int reference,
                               bool linkable,
                               bool makesReferenceOnParent)
                    : TreeItem(data, parent),
                      ReferenceItem(data, derivedType, parent,
                                    reference, linkable, makesReferenceOnParent)
{
    // Reference parameters are established in ReferenceItem
}

// -----------------------------------------------------------------------------

// Return a list of all the objects that refer to a given refernence Target
OptionsMap CrossReference::getReferrers()
{
    OptionsMap referrers;
    // This slightly unusual lookup avoids a const_cast to <T*>
    for (auto it = getXRefMap()->begin();
         it != getXRefMap()->end(); ++it) {
        if ((it.key() == this)) {
            for (auto referrer : getXRefMap()->values(it.key())) {
                referrers.insert(referrer->getReferenceAsString(), referrer);
            }
            break;
        }
    }
    return referrers;
}

// -----------------------------------------------------------------------------

void CrossReference::registerReference(TreeItem *targetReference,
                                                 TreeItem *xRefMaker)
{
    if (!getXRefMap()->contains(targetReference, xRefMaker)) {
        getXRefMap()->insert(targetReference, xRefMaker);
    }
}

// -----------------------------------------------------------------------------

void CrossReference::deregisterReference(TreeItem *targetReference,
                                                   TreeItem *deregistrant)
{
    getXRefMap()->remove(targetReference, deregistrant);
}

// -----------------------------------------------------------------------------

void CrossReference::refreshReferences()
{
    // Update reference string in referring items upon change to the referred
    for (auto value : getXRefMap()->values(this)) {
        value->updateReferencesInModel();
    }
}

// -----------------------------------------------------------------------------

// delete references to a specific target - used when deleting an object T

void CrossReference::deregisterAllReferencesToTarget()
{
    for (auto value : getXRefMap()->values(this)) {
        getXRefMap()->remove(this, value);
        value->updateReferencesInModel();
    }
}

// -----------------------------------------------------------------------------

void CrossReference::deregisterAllReferencesFromSource(TreeItem *deregistrant)
{
    // Remove references from one specific item (value U) to any target (Key T)
    for (auto key : getXRefMap()->keys(deregistrant)) {
        getXRefMap()->remove(key, deregistrant);
    }
}

// -----------------------------------------------------------------------------

// Return a list of all references made by a referencing object
OptionsMap CrossReference::getAllReferencesFromSource(const TreeItem *referrer,
                                                          bool asOptions) const
{
    // references will be self-ordering on getReference() as its keys
    OptionsMap references;

    // Const iteration instead of passing referrer using a const-cast to .keys()
    for (auto it = getXRefMap()->begin(); it != getXRefMap()->end(); ++it) {
        if (it.value() == referrer) {
            QString referenceText = it.key()->getReferenceAsString();
            if (asOptions) {
                referenceText += " " + it.key()->data(0).toString();
            }
            references.insert(referenceText, it.key());
        }
    }
    return references;
}

// -----------------------------------------------------------------------------

// -------------------- REFERENCE MAKING ITEM FUNCTIONS -------------------------

// -----------------------------------------------------------------------------

// Generates a string of references and inserts it into the model
void CrossReference::updateReferencesInModel()
{
    // The setData override does not permit entry of data into column 2 - in
    // order to prevent it being manually edited. Where the data is altered
    // programatically here, we use the base class to bypass that restriction.
    TreeItem::setData(2, getReferencesMadeAsString());
}

// -----------------------------------------------------------------------------
QString CrossReference::getReferencesMadeAsString()
{
    QString newDataString;
    QMap<int, QString> orderedReferences;
    for (const TreeItem* item : getReferencesMade()) {
        orderedReferences.insert(item->getReferenceNumber(),
                                 item->getReferenceAsString());
    }
    for (const QString &reference : orderedReferences) {
        newDataString += reference + " ";
    }
    return newDataString.trimmed();
}

// -----------------------------------------------------------------------------

// Associates a reference with the item, such as a Loss with a Hazard
void CrossReference::makeReference(TreeItem *referenceTarget)
{
    makeReference(referenceTarget->getReference());
}

// Full reference information must be supplied to use this function
void CrossReference::makeReference(Reference targetReference)
{
    if (permittedReferenceTargets().contains(targetReference.type)) {
        // At present, reference targets are unique, but in future they may not be
        for (TreeItem *target : getReferenceMap()->values(targetReference)) {
            registerReference(target, this);
        }
        updateReferencesInModel();
    }
}

// -----------------------------------------------------------------------------

// Remove an association
void CrossReference::deleteReference(TreeItem *referenceTargetItem)
{
    // If we delete a xReference to an object, delete all xrefs to other objects
    // that have the same reference number
    Reference targetReference = referenceTargetItem->getReference();
    for (auto item : getReferenceMap()->values(targetReference)) {
        deregisterReference(item, this);
    }
    updateReferencesInModel();
}

// -----------------------------------------------------------------------------

// Remove all xReferences, used in deleting items
void CrossReference::deleteAllReferences()
{
    // Delete all references made by this object
    deregisterAllReferencesFromSource(this);
    updateReferencesInModel();
}

// -----------------------------------------------------------------------------

// Gets the references associated (made by) this item
OptionsMap CrossReference::getReferencesMade(bool asOptions) const
{
    return getAllReferencesFromSource(this, asOptions);
}

// -----------------------------------------------------------------------------

// Gets all the possible references that could be made by this item
OptionsMap CrossReference::getReferenceTargetOptions()
{
    // Get references in this tree, of the specific type T
    return getPeerReferences(defaultReferenceType());
}
