/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#ifndef CROSSREFERENCE_H
#define CROSSREFERENCE_H

#include "treeitem.h"
#include "referenceitem.h"

/* crossreference.h/cpp
 * --------------------------------------------------------------------------------------
 * This class deals with the ability of one item to make a cross reference to another.
 * Cross references, or xRefs, are informational only - only the makes reference column
 * of the reference maker is affected.
 *
 * CROSS REFERENCES ARE MADE: between items when target / referencemaker pairs are
 * inserted into a the cross reference map.
 *
 * If TreeItem *object1 and TreeItem *object2 are inserted into the map, e.g.
 *    map.insert(object1, object2);
 * this means that object2 is making a xreference to object 1. (this is done in register
 * reference). Each item can both make and receive multiple x-references, but each
 * target / maker pair must be unique; an object cannot enter an x-reference to the same
 * target twice.
 *
 * A x-reference is removed simply by deleting the target, maker entry from the map.
 *
 * The x-references are made on a per object basis. Thus, if a particular object refers
 * to a linked object, logically it must be referring to all objects with the same
 * linked reference. Therefore, multiple x-references will be established to all
 * targets with that reference. This is so that if one of the target linked objects is
 * deleted, then the x-references to the other linked objects will remain valid.
 *
 * When a particular target item is deleted, all xreferences to that target will be
 * deleted. Likewise, when an x-reference maker is deleted, all its x-reference
 * targets will be deleted from the map.
 *
 * The vast majority of the functionality above is implemented in the form of simple
 * map operations, with certain checks and bounds.
*/

class CrossReference : public ReferenceItem
{
public:
    using TreeItem::getXRefMap;
    CrossReference(const QVector<QVariant> &data,
                   const QString &derivedType,
                   TreeItem *parent = nullptr,
                   int reference = 0,
                   bool linkable = false,
                   bool makesReferenceOnParent = false
                   );

    virtual ~CrossReference() {deregisterAllReferencesToTarget();
                               deleteAllReferences();}
    bool isValidreferenceTarget() override {return true;}
    bool isReferencingItem() override {return true;}

    // Data is tracked in the referenced class
    void makeReference(TreeItem *referenceTarget) override;
    void makeReference(Reference targetReference) override;
    OptionsMap getReferencesMade(bool asOptions = false) const override;
    OptionsMap getReferenceTargetOptions() override;
    void deleteReference(TreeItem *referenceTarget) override;
    void deleteAllReferences() override;
    void updateReferencesInModel() override;
    QString getReferencesMadeAsString() override;

    friend class CrossReferenceTests;
private:
    // Add and remove references from a specific source
    void registerReference(TreeItem *targetReference, TreeItem *registrant);
    void deregisterReference(TreeItem *targetReference, TreeItem *deregistrant);
    void deregisterAllReferencesToTarget();
    void deregisterAllReferencesFromSource(TreeItem *deregistrant);

    OptionsMap getAllReferencesFromSource(const TreeItem* referrer,
                                          bool asOptions = false) const;
    OptionsMap getReferrers();
    void refreshReferences() override;
};


#endif // CROSSREFERENCE_H
