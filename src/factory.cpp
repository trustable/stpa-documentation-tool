/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include "factory.h"
#include "treeitemtypes.h"

// The manufacture function for the type RootItem<T> is in the RootItem<T> template
template<typename T>
void Factory::registerRoot()
{
    rootItemTypes.insert(RootItem<T>::classParentOf(),
                         RootItem<T>::manufacture);
}

Factory::Factory()
{
    // This constructor will register the relevant types, and pointers to their
    // manufacture functions, so they can be called easily - also, remember that
    // this is not for RootItem<T>, because RootItem<T> is not arbitrarily insertable

    registerType<Loss>();
    registerType<Hazard>();
    registerType<Control>();
    registerType<UCA>();
    registerType<ControlAction>();
    registerType<TrueStatement>();
    registerType<Belief>();
    registerType<Cause>();
    registerType<InformationReceived>();
    registerType<HowThisCouldOccur>();
    registerType<Requirement>();

    // Register the template instantiations of RootItem<T>
    registerRoot<Loss>();
    registerRoot<Hazard>();
    registerRoot<Control>();
}

TreeItem *Factory::create(const QString &type,
                          const QVector<QVariant> &data,
                          TreeItem *parent,
                          int uniqueID,
                          int reference)
{
    TreeItem *newObject;
    // This extacts a pointer to the manufacture function and calls with supplied args
    if (treeItemTypes.contains(type)) {
        manufacturePointer classFactory = treeItemTypes.value(type);
        newObject = classFactory(data, parent, uniqueID, reference);
    } else {
        newObject = nullptr;
    }
    return newObject;
}

// The root system should probably not be extended so it is implemented in a less
// flexible way.
TreeItem *Factory::generateRoot(const QString &parentOf,
                                const QVector<QVariant> &data,
                                TreeItem *parent,
                                int uniqueID)
{
    TreeItem *newRootItem;
    // This extacts a pointer to the manufacture function and calls with supplied args
    if (rootItemTypes.contains(parentOf)) {
        rootGeneratorPointer rootFactory = rootItemTypes.value(parentOf);
        newRootItem = rootFactory(data, parent, uniqueID);
    } else {
        newRootItem = nullptr;
    }
    return newRootItem;
}

QString Factory::fullname(const QString &type)
{
    QString fullname;
    for (const auto &pair : registeredNames) {
        if (pair.first == type) {
            fullname = pair.second;
            break;
        }
    }
    return fullname;
}

Factory *Factory::factory()
{
    if (!singleton) {
        singleton = new Factory;
    }
    return singleton;
}

void Factory::decommission()
{
    delete singleton;
    singleton = nullptr;
}

