/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing
 * terms of this software.
 *
*/

/*
 factory .h/.cpp
 -------------------------------------------------------------------------------
 The Factory class exists to allow the easy insert of arbitrary types - this
 enables an arbitrary hierarchy and simplifies object generation by type.

 It also provides a factory for root items, but these a handled through
 a set of mirror functions to prevent the insertation of RootItem<T> in
 in a non-root location

 Any type T that uses the factory must provide T::classType, and a 4 argument
 constructor. The manufacture / registration functions will be generated
 automatically when the user calls Factory::registerType<T>();

 Add registration functions to the constructor for types to be registered
 automatically.
*/

#ifndef FACTORY_H
#define FACTORY_H

#include "treeitem.h"
#include "QDebug"

class Factory
{
public:

    // For clarity, manufacturePointer is a pointer to a function that takes
    // data, parent, etc and returns a pointer to a TreeItem
    using manufacturePointer = TreeItem * (*)(const QVector<QVariant> &data,
                                              TreeItem *parent,
                                              int uniqueID,
                                              int reference);
    using rootGeneratorPointer = TreeItem * (*)(const QVector<QVariant> &data,
                                                TreeItem *parent,
                                                int uniqueID);

    // Call factory to register the standard types
    static Factory* factory();
    static void decommission();

    // Allow outsiders to register new classes if need be
    template<typename T>
    void registerType();

    const auto registrations() const {return registeredNames;}
    QString fullname(const QString &type);

    TreeItem *create(const QString &type,
                     const QVector<QVariant> &data,
                     TreeItem *parent,
                     int uniqueID,
                     int reference);

    TreeItem *generateRoot(const QString &parentOf,
                           const QVector<QVariant> &data,
                           TreeItem *parent,
                           int uniqueID);

    Factory(const Factory& rhs) = delete;
    Factory& operator=(const Factory&) = delete;

private:
    Factory();
    ~Factory() = default;

    // These functions hold the types and register them
    QMap<QString, manufacturePointer> treeItemTypes;
    QMap<QString, rootGeneratorPointer> rootItemTypes;

    // This is one of the times that insert order matters - get them out
    // in the order that we put them in, not alphabetical
    QVector<std::pair<QString, QString>> registeredNames;

    static inline Factory *singleton = nullptr;

    // Restrict registration of new root items
    template<typename T>
    void registerRoot();

    template<typename T>
    static TreeItem *manufacture(const QVector<QVariant> &data,
                                 TreeItem *parent,
                                 int uniqueID,
                                 int reference);
};

template<typename T>
void Factory::registerType()
{
    // Call the first function, take pointer to second
    QString type = T::classType();

    // This map contains the manufacture function ptrs for creation
    treeItemTypes.insert(type, manufacture<T>);

    // This map contains the registered names for the outside world to use
    registeredNames.push_back(std::pair{type, T::classCellType()});
    qDebug() << "Registering type: " << type.toStdString().data();
}

template<typename T>
TreeItem *Factory::manufacture(const QVector<QVariant> &data,
                               TreeItem *parent,
                               int uniqueID,
                               int reference)
{
    return new T(data, parent, uniqueID, reference);
}



#endif // FACTORY_H
