/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include "listwidget.h"
#include <iostream>

ListWidget::ListWidget(QObject *parent) : QListWidget ()
{
    Q_UNUSED(parent);
}

