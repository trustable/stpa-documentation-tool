/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

/*listwidget.h / .cpp
 * ----------------------------------------------------------------------------
 * Overrides for the ListWidget (the lower part of the application view) can
 * be implemented here.
*/

#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include <QListWidget>

// This class exists to override the basic functionality the QListWidget
class ListWidget : public QListWidget
{
    Q_OBJECT

public:
    explicit ListWidget(QObject *parent = nullptr);

signals:

};

#endif // LISTVIEW_H
