/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

// The functionality of each translational unit is recorded in the appropriate header files,
// where required the methodology is explained in the .cpp file.

// The sole function of main is to start the application.

#include <QApplication>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    Q_INIT_RESOURCE(resourcelist);

    QApplication app(argc, argv);
    MainWindow window;
    window.show();
    return QApplication::exec();
}
