﻿/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

/*******************************************************************************************************
 * mainwindow.cpp
 *******************************************************************************************************
 *
 * This file deals with
 *
 * - linking the UI to implementation code,
 * - Dealing with file access for save and load files,
 * - Populating / updating the list window
 * - implementing user links / references by calling the model
 *
 */

#include "mainwindow.h"

#include <QString>
#include <QStringList>

#include <QFile>
#include <QTemporaryFile>
#include <QTemporaryDir>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QModelIndex>
#include <QListWidgetItem>
#include <QAction>
#include <exception>
#include <QItemSelectionModel>

#include <QDebug>

#include "palette.h"

/* The main role of the MainWindow constructor is to load base data and
 * set up the UI
 *  a) Load a default file (create a model) and set that model
 *  b) Link UI items to functions in the MainWindow::
 *  c) Build the context menu by creating actions and adding them
 *  d) connect the view selection changed to MainWindow so that objects
 *     can be updated
 *  e) Set the item delegate for the view (in principal, MyTreeView could do this)
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setupUi(this);

    // Core behaviour --------------------------------------------------------------
    // Hard-coded headers prevent unfortunate errors from incorrect file format
    headers << tr("Item")
            << tr("Reference")
            << tr("Refers To")
            << tr("Comment");

    // Read in the default file - the file in the qrc wrapper must be copied out
    // to a temporary location so that the YAML library can see it
    QFile file(":/resources/default.yml");
    tempfile = QTemporaryFile::createNativeFile(file);

    try {
        model = new TreeModelYaml(headers, tempfile->fileName(), this);
    } catch (std::exception &e) {
        QByteArray mesg("An exception was thrown while loading the default file:\n  ");
        mesg += e.what();
        mesg += "\n\nA corrupted base file requires reinstallation of the application.";
        QMessageBox::information(this, tr("Error while loading file!"), mesg);
    }

    view->setModel(model);
    view->setColumnWidth(0, 600);
    view->setColumnWidth(2, 150);
    clearAllHighlighting();

    // Allow quit
    connect(exitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

    // Allows update of the model on selection change
    connect(view->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &MainWindow::updateActions);

    connect(model, &TreeModel::updateIndex, view, &MyTreeView::updateSelection);


    // DROP DOWNS ----------------------------------------------------------------

    // ---------------------------------------------------------------------------
    // File Menu
    // ---------------------------------------------------------------------------

    // Legacy save / load options
    connect(actionLegacy_Load, &QAction::triggered, this,
            [this] {loadStpaFile(QString());});

    // .yaml options for save / load
    connect(loadAction, &QAction::triggered, this,
            [this] {loadYamlFile(QString());});
    connect(saveAction, &QAction::triggered, this,
            [this] {saveYamlFile();});
    connect(newFileAction, &QAction::triggered, this,
            [this] {loadYamlFile(tempfile->fileName());});

    // ---------------------------------------------------------------------------
    // Actions Menu
    // ---------------------------------------------------------------------------

    // Default actions
    connect(actionsMenu, &QMenu::aboutToShow, this, &MainWindow::updateActions);
    connect(removeRowAction, &QAction::triggered, this, &MainWindow::removeRow);

    connect(insertRowAction, &QAction::triggered, this,
                [this] {insertRow();});
    connect(insertChildAction, &QAction::triggered, this,
                [this] {insertChild();});

    // Custom Inserts -----------------------------------------------------------

    // Derive from Factory: create the names, the actions, store the actions ptrs in a
    // convenience container, and connect the actions to their targets
    for (const auto &registration : Factory::factory()->registrations()) {
        QString rowText("Insert Peer: ");
        QString childText("Insert Child: ");
        rowText += registration.second;
        childText += registration.second;
        auto *menuRow = new QAction(rowText, actionsMenu);
        auto *menuChild = new QAction(childText, actionsMenu);
        auto *contextRow = new QAction(rowText, this);
        auto *contextChild = new QAction(childText, this);
        insertMenuActionsRow.insert(registration.first, menuRow);
        insertMenuActionsChild.insert(registration.first, menuChild);
        insertContextActionsRow.insert(registration.first, contextRow);
        insertContextActionsChild.insert(registration.first, contextChild);
        connect(menuRow, &QAction::triggered, this,
                [=] {insertRow(registration.first);});
        connect(menuChild, &QAction::triggered, this,
                [=] {insertChild(registration.first);});
        connect(contextRow, &QAction::triggered, this,
                [=] {insertRow(registration.first);});
        connect(contextChild, &QAction::triggered, this,
                [=] {insertChild(registration.first);});
    }

    // Add actions to Actions Menu - children first then rows - and connect them
    actionsMenu->addSeparator();
    for (const auto &registration : Factory::factory()->registrations()) {
        actionsMenu->addAction(insertMenuActionsChild.value(registration.first));
    }
    actionsMenu->addSeparator();
    for (const auto &registration : Factory::factory()->registrations()) {
        actionsMenu->addAction(insertMenuActionsRow.value(registration.first));
    }

    // ---------------------------------------------------------------------------
    // Editor menu
    // ---------------------------------------------------------------------------

    connect(actionSpeed_Entry, &QAction::toggled, this,
                [this] {emit sendSpeedMode(actionSpeed_Entry->isChecked());});
    connect(this, &MainWindow::sendSpeedMode, view, &MyTreeView::setSpeedMode);
    connect(actionAutoRef, &QAction::triggered,
            this, &MainWindow::updateActions);
    connect(actionInsert_Custom_Children, &QAction::triggered,
            this, &MainWindow::updateActions);
    connect(actionInsert_Custom_Peer_Rows, &QAction::triggered,
            this, &MainWindow::updateActions);

    // ---------------------------------------------------------------------------
    // View Menu
    // ---------------------------------------------------------------------------

    // Expand / Collapse ---------------------------------------------------------

    // Collapse All
    connect(actionCollapse_All, &QAction::triggered, this,
                [this] {view->collapseAll();});
    // Expand All
    connect(actionExpand_All, &QAction::triggered, this,
                [this] {view->expandAll();});

    // Create Expand to & Add / Remove highlighting based directly on Palette ----

    // Get the colours type name, create a action for it, store it, link it up,
    // and put it in the menu
    for (const auto &colorType : Palette::instance()->registeredHighlights()) {
        auto *hightlightAction = new QAction(colorType.second, menuApply_highlighting_to);
        auto *expandAction = new QAction(colorType.second, menuExpand_view_to);
        // Both the highlight and expand functions access the highlight checkbox
        connect(expandAction, &QAction::triggered, this,
                [=] {expandToType(colorType.first, hightlightAction);});
        connect(hightlightAction, &QAction::triggered, this,
                [=] {toggleHighlighting(colorType.first, hightlightAction);});
        hightlightAction->setCheckable(true);
        menuApply_highlighting_to->addAction(hightlightAction);
        menuExpand_view_to->addAction(expandAction);
    }

    // View Menu: Clear Highlighting --------------------------------------------
    connect(actionClear_Highlighting, &QAction::triggered, this,
                [this] {clearAllHighlighting();});

    // ---------------------------------------------------------------------------
    // Output STPA menu
    // ---------------------------------------------------------------------------

    connect(actionWrite_Handbook_Markdown, &QAction::triggered, this,
                [this] {writeHandbookMarkdown(OutputType::FullMarkdown);});
    connect(actionWrite_Requirements_as_Markdown, &QAction::triggered, this,
                [this] {writeHandbookMarkdown(OutputType::Requirements);});

    // ---------------------------------------------------------------------------
    // Renumber menu
    // ---------------------------------------------------------------------------
    connect(actionRenumber_This_Category, &QAction::triggered, this,
                &MainWindow::renumberCategory);

    // -----------------------------------------------------------------------------
    // The context menu
    // SetContextMenuPolidcy --> ActionsContextMenu causes the actions of the widget
    // (I.e. the tree view) to be presented in the context menu.
    // Below, the individual actions are created, and added to the widget using addAction.

    setContextMenuPolicy(Qt::ActionsContextMenu);

    // Create context actions with default texts
    contextCreateTargetLink
            = new QAction("Create Link to an Item of class: ", this);
    contextSeverLink
            = new QAction("Break Link(s) for this item", this);
    contextAddRemoveReferences
            = new QAction("Add / Remove references for this item", this);
    contextInsertRow
            = new QAction("Insert Peer Row (peer object)", this);
    contextInsertChild
            = new QAction("Insert Child Row (child object)", this);
    contextDeleteRow
            = new QAction("Delete Row (peer object)", this);

     // Add the actions to the widget in the correct order - they will appear in the context
    addAction(contextCreateTargetLink);
    addAction(contextSeverLink);
    addAction(separator());
    addAction(contextAddRemoveReferences);
    addAction(separator());
    addAction(contextInsertRow);
    addAction(contextDeleteRow);
    addAction(separator());
    addAction(contextInsertChild);
    addAction(separator());

    for (const auto &registration : Factory::factory()->registrations()) {
        addAction(insertContextActionsChild.value(registration.first));
    }
    addAction(separator());
    for (const auto &registration : Factory::factory()->registrations()) {
        addAction(insertContextActionsRow.value(registration.first));
    }

    // Connect the actions to the correct underlying functionality - use Lambdas for parameters
    connect(contextCreateTargetLink, &QAction::triggered, this,
            &MainWindow::createLinkToTarget);
    connect(contextAddRemoveReferences, &QAction::triggered, this,
            &MainWindow::showReferenceTargetsAddRemove);
    connect(contextSeverLink, &QAction::triggered, this,
            &MainWindow::severLink);
    connect(contextInsertRow, &QAction::triggered, this,
            [this] {insertRow();});
    connect(contextDeleteRow, &QAction::triggered, this,
            &MainWindow::removeRow);
    connect(contextInsertChild, &QAction::triggered, this,
            [this] {insertChild();});

    // -----------------------------------------------------------------------------
    // Resize delegate - responsible for row heights & highlighting
    delegate = new ItemDelegate(view, model, this);
    view->setItemDelegate(delegate);
    updateActions();
}

MainWindow::~MainWindow()
{
    // delete the temporary file that was created for ::resources/default.yml
    delete tempfile;

    // Nominal clean up
    qDeleteAll(options);
    delete delegate;
    delete model;
    Factory::decommission();
}

// --------------------------------------------------------------------------------
// Provide a context menu separator with 'this' as a parent, receiver MUST own it!
QAction *MainWindow::separator()
{
    auto newSeparator = new QAction(this);
    newSeparator->setSeparator(true);
    return newSeparator;
}

// --------------------------------------------------------------------------------
// Menu Items: File - fileName is requested in function if none is supplied
void MainWindow::loadStpaFile(QString filename)
{
    if (filename.isEmpty()) {
        filename = QFileDialog::getOpenFileName(this,
            tr("Open STPA Analysis File"), "",
            tr("STPA (*.stpa);;All Files (*)"));
    }
    if (!filename.isEmpty()) {
        try {
            newModel = new TreeModelLegacy(headers, filename, this);
        } catch (std::exception &e) {
            QByteArray mesg("An exception was thrown while loading the file:\n  ");
            mesg += e.what();
            QMessageBox::information(this, tr("Error while loading file!"), mesg);
        }
        if (newModel != nullptr) {
            view->setModel(newModel);
            delegate->resetTreeModel(newModel);
            connect(view->selectionModel(), &QItemSelectionModel::selectionChanged,
                    this, &MainWindow::updateActions);
            delete model;
            model = newModel;
            newModel = nullptr;
        } else {
            QMessageBox::information(this, tr("Error reading file!"), "Unable to parse file!");
        }
    }
    clearAllHighlighting();
}

void MainWindow::loadYamlFile(QString fileName)
{
    qDebug() << "YAML load file function, filename: " << fileName.toStdString().data();
    if (fileName.isEmpty()) {
        fileName = QFileDialog::getOpenFileName(this,
            tr("Open STPA Analysis File"), "",
            tr("STPA YAML (*.yml);;All Files (*)"));
    }
    if (!fileName.isEmpty()) {
        // If the file is parsed here and passed to the model, then both files need to know about .yaml
        // so the filename will be passed alone
        try {
            newModel = new TreeModelYaml(headers, fileName, this);
        } catch (std::exception &e) {
            QByteArray mesg("An exception was thrown while loading the file:\n  ");
            mesg += e.what();
            QMessageBox::information(this, tr("Error while loading file!"), mesg);
        }
        if (newModel != nullptr) {
            view->setModel(newModel);
            delegate->resetTreeModel(newModel);
            connect(view->selectionModel(), &QItemSelectionModel::selectionChanged,
                    this, &MainWindow::updateActions);
            delete model;
            model = newModel;
            newModel = nullptr;
        }
    }
    clearAllHighlighting();
}

void MainWindow::saveYamlFile(bool nullBool)
{
    // null bool potentially used for back-up implementation
    Q_UNUSED(nullBool);

    // Backup implementation should use a temporary file in the local scope,
    // so that it will be correctly deleted.

    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save STPA Analysis File"), "",
                                                    tr("STPA (*.yml);;All Files (*)"));
    if (filename.isEmpty()) {
        // Will return from single point of return
    } else {
        if (!filename.contains(".")) {
            filename += ".yml";
        }
        // The yaml library throws - therefore errors from save / load must be processed
        // as exceptions
        try {
            model->createYamlSaveFile(filename, this);
        } catch (std::exception &e) {
            QByteArray mesg("An exception was thrown while saving the file:\n  ");
            mesg += e.what();
            QMessageBox::information(this, tr("Error saving file!"), mesg);
        }
    }
}

void MainWindow::saveStpaFile(bool backup)
{
    // This function is present for maintenance purposes only - this
    // function is not accessible from the GUI
    QStringList modelAsStrings;
    QString filename;
    if (backup) {
        filename = QString(":/.backup.stpa");
    } else {
        filename = QFileDialog::getSaveFileName(this,
                                tr("Save STPA Analysis File"), "",
                                tr("STPA (*.stpa);;All Files (*)"));
    }
    if (filename.isEmpty()) {
        // Will return from single point of return
    } else {
        if (!filename.contains(".")) {
            filename += ".stpa";
        }
        QFile file(filename);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
        } else {
            QTextStream fileStream(&file);
            for (const QString& string : model->convertModelDataToStrings()) {
                fileStream << string << endl;
            }
            file.close();
        }
    }
}

// --------------------------------------------------------------------------------
// Slots that are available to receive trigger signals from context menus

void MainWindow::showReferenceTargetsAddRemove()
{
    QModelIndex index = view->selectionModel()->currentIndex();
    currentlyActiveList = ListActive::AddOrRemoveReferences;
    if (model->isReferencingItemAt(index)) {
        OptionsMap availableOptions = model->referenceTargetOptions(index);
        OptionsMap currentOptions = model->currentReferenceTargets(index);
        populateOptions(availableOptions, currentOptions, true);
    } else {
        OptionsMap availableOptions = model->referenceTargetOptions(index);
        populateOptions(availableOptions);
        statusBar()->showMessage(tr("This data type does not make references!"));
    }
}

void MainWindow::createLinkToTarget()
{
    currentlyActiveList = ListActive::CreateLink;
    QModelIndex index = view->selectionModel()->currentIndex();

    // Options in the peer category, minus itself
    OptionsMap options = model->referenceOptionsInThisCategory(index);
    options.remove(model->getItemReferenceAsOption(index));
    if (options.isEmpty()) {
        statusBar()->showMessage(tr("Nothing available to Link!"));
    } else {
        populateOptions(options);
    }
}

void MainWindow::severLink()
{
    QModelIndex index = view->selectionModel()->currentIndex();
    if (!model->severLinkOnRow(index)) {
        statusBar()->showMessage(tr("Item is not linked!"));
    } else {
        model->layoutChanged();
    }
}
// --------------------------------------------------------------------------------
// Menu Items: Editor

    // Add functions for the editor here

// --------------------------------------------------------------------------------
// Menu Items: Collapse / Expand

// Send the associated checking function to this action - the expand to depth
// function highlights objects of type 'type' - the highlight checkbox will be set
void MainWindow::expandToType(const QString &type, QAction* associatedAction)
{
    model->setHighlightForType(type);
    view->iterateExpandToType(view->rootIndex(), type);
    if (associatedAction) {
        associatedAction->setChecked(true);
    }
}

// Highlights now implemented in terms of object types / private members
void MainWindow::setHighlighting(const QString &type)
{
    model->setHighlightForType(type);
}

void MainWindow::clearHighlighting(const QString &type)
{
    model->clearHighlightForType(type);
}

void MainWindow::toggleHighlighting(const QString &type, QAction *associatedAction)
{
    if (associatedAction && associatedAction->isChecked()) {
        setHighlighting(type);
    } else {
        clearHighlighting(type);
    }
}

void MainWindow::clearAllHighlighting()
{
    for (const auto &type: Palette::instance()->registeredHighlights()) {
        model->clearHighlightForType(type.first);
    }
    for (auto action : menuApply_highlighting_to->actions()) {
        action->setChecked(false);
    }
}

// --------------------------------------------------------------------------------
// Output STPA analysis formatted as markdown
void MainWindow::writeHandbookMarkdown(const OutputType output)
{
    QByteArray title;
    QString filename;
    switch (output) {
    case OutputType::Requirements:
        filename = QFileDialog::getSaveFileName(this,
                                tr("Enter Filename for Requirements output"),
                                tr("StpaRequirements"),
                                tr("Mark-down (*.md);;All Files (*)"));
        title = QByteArray("# STPA Requirements");
        break;
    case OutputType::FullMarkdown:
        filename = QFileDialog::getSaveFileName(this,
                                tr("Enter Filename for Analysis output"),
                                tr("StpaMarkdown"),
                                tr("Mark-down (*.md);;All Files (*)"));
        title = QByteArray("# STPA Analysis");
        break;
    }


    if (filename.isEmpty()) {
        // Will return from single point of return
    } else {
        if (!filename.contains(".")) {
            filename += ".md";
        }
        QFile file(filename);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
        } else {
            // Action the request
            QTextStream fileStream(&file);
            fileStream << title << endl;

            // Get the appropriate content
            QStringList fileOutput;
            switch (output) {
            case OutputType::Requirements:
                fileOutput = model->createMarkdownRequirementsFromModel();
                break;
            case OutputType::FullMarkdown:
                fileOutput = model->createMarkdownStringsFromModel();
                break;
            }

            for (const QString& string : fileOutput) {
                // An end of line will not be added here as (multiple) endls are
                // already added in this format
                fileStream << string;
            }
            file.close();
        }
    }
}

// --------------------------------------------------------------------------------
// Interface the the renumber function
void MainWindow::renumberCategory()
{
    QModelIndex index = view->selectionModel()->currentIndex();
    if (index.isValid()) {
        model->renumumberAllReferencesThisCategory(index);
    }
}

// --------------------------------------------------------------------------------
// Menu Items: Actions

void MainWindow::insertChild(QString specialType)
{
    QModelIndex index = view->selectionModel()->currentIndex();

    if (model->columnCount(index) == 0) {
        if (!model->insertColumn(0, index))
            return;
    }

    // Here the QAbstractItemModel is replaced with a new version
    // that has some knowledge about what is being inserted
    if (!model->insertRowByType(0, index, specialType))
        return;

    for (int column = 0; column < model->columnCount(index); ++column) {
        QModelIndex child = model->index(0, column, index);
        if (column == 0) {
            model->setData(child, QVariant("[No data]"), Qt::EditRole);
        }
        if (!model->headerData(column, Qt::Horizontal).isValid())
            model->setHeaderData(column, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);
    }
    model->sortChildrenForRow(index);
    view->selectionModel()->setCurrentIndex(model->index(0, 0, index),
                                            QItemSelectionModel::ClearAndSelect);
    model->layoutChanged();
    updateActions();
}

bool MainWindow::insertColumn()
{
    int column = view->selectionModel()->currentIndex().column();

    bool changed = model->insertColumn(column + 1);
    if (changed)
        model->setHeaderData(column + 1, Qt::Horizontal, QVariant("[No header]"), Qt::EditRole);

    updateActions();
    return changed;
}

void MainWindow::insertRow(QString specialType)
{
    QModelIndex index = view->selectionModel()->currentIndex();

    if (!model->insertRowByType(index.row()+1, index.parent(), specialType))
        return;

    updateActions();

    for (int column = 0; column < model->columnCount(index.parent()); ++column) {
        QModelIndex child = model->index(index.row()+1, column, index.parent());
        if (column == 0) {
            model->setData(child, QVariant("[No data]"), Qt::EditRole);
        }
    }
    model->sortChildrenForRow(index.parent());
    model->layoutChanged();
}

bool MainWindow::removeColumn()
{
    int column = view->selectionModel()->currentIndex().column();
    bool changed = model->removeColumn(column);

    if (changed)
        updateActions();

    return changed;
}

void MainWindow::removeRow()
{
    // Retrieve the index of the selected row
    QModelIndex index = view->selectionModel()->currentIndex();

    // call to remove row is overridden in TreeModel
    if (model->removeRow(index.row(), index.parent())) {
        updateActions();
    } else {
        statusBar()->showMessage(tr("Cannot delete this item!"));
    }
}

// --------------------------------------------------------------------------------
// Update options etc following a new item being selected
void MainWindow::updateActions()
{
    // Determine based on whether a selection is active whether certain actions are valid
    bool hasSelection = !view->selectionModel()->selection().isEmpty();
    removeRowAction->setEnabled(hasSelection);
    removeColumnAction->setEnabled(hasSelection);

    bool hasCurrent = view->selectionModel()->currentIndex().isValid();
    insertRowAction->setEnabled(hasCurrent);
    insertColumnAction->setEnabled(hasCurrent);

    if (hasCurrent) {
        QModelIndex currentIndex = view->selectionModel()->currentIndex();
        view->closePersistentEditor(currentIndex);

        int row = currentIndex.row();
        int column = currentIndex.column();
        QString fullCelltype
                = model->itemType(currentIndex);
        QString childType
                = model->defaultChildType(currentIndex);
        QString regType
                = model->getItemType(currentIndex);
        QStringList permittedChildInserts
                = model->getPermittedInsertTypes(currentIndex);
        QStringList permittedRowInserts
                = model->getPermittedInsertTypes(currentIndex.parent());

        // Show the item type in the status banner
        if (currentIndex.parent().isValid()) {
            statusBar()->showMessage(
                        tr("Position: (%1,%2) item type is %3")
                                     .arg(row).arg(column).arg(fullCelltype));
        } else {
            statusBar()->showMessage(
                        tr("Position: (%1,%2) in top level item type is %3")
                                     .arg(row).arg(column).arg(fullCelltype));
        }
        // Ensure the options list is clear upon a new selection
        // (auto options must repopulate the list)
        currentlyActiveList = ListActive::None;
        optionsList->clear();

        // Update the context menu
        QString contextText1 = QString("Create Link to another %1").arg(fullCelltype);
        contextCreateTargetLink->setText(contextText1);
        contextCreateTargetLink->setEnabled(model->isLinkableItem(currentIndex));

        QString contextText2 = QString("Break Link(s) for this %1").arg(fullCelltype);
        contextSeverLink->setText(contextText2);
        contextSeverLink->setEnabled(model->isLinkableItem(currentIndex));

        QString contextText3 = QString("Add / Remove references for this %1").arg(fullCelltype);
        contextAddRemoveReferences->setText(contextText3);
        contextAddRemoveReferences->setEnabled(model->isReferencingItemAt(currentIndex));
        if (actionAutoRef->isChecked() && model->isReferencingItemAt(currentIndex)) {
            showReferenceTargetsAddRemove();
        }

        QString insertPeer = QString("Insert Standard Peer Row (%1)").arg(fullCelltype);
        contextInsertRow->setText(insertPeer);
        contextInsertRow->setEnabled(model->isEditable(currentIndex));

        QString childFullName = Factory::factory()->fullname(childType);
        QString insertChild = QString("Insert Standard Child Row (%1)").arg(childFullName);
        contextInsertChild->setText(insertChild);
        contextInsertChild->setEnabled(model->isParentCapable(currentIndex));

        QString contextText6 = QString("Delete Row");
        contextDeleteRow->setText(contextText6);
        contextDeleteRow->setEnabled(model->isDeletable(currentIndex));

        // Update the actions menu
        removeRowAction->setEnabled(model->isDeletable(currentIndex));

        insertRowAction->setEnabled(model->isEditable(currentIndex));
        insertRowAction->setText(insertPeer);

        insertChildAction->setEnabled(model->isParentCapable(currentIndex));
        insertChildAction->setText(insertChild);

        // Examine the insert Row action map - if the key exists in permitted rows, set visible
        // the action, but avoid repeating the option to insert the same type
        for (auto it = insertMenuActionsRow.begin();
                 it != insertMenuActionsRow.end();
                 ++it) {
            it.value()->setVisible(actionInsert_Custom_Peer_Rows->isChecked()
                                   && permittedRowInserts.contains(it.key())
                                   && it.key() != regType);
        }
        // Similarly for the insert child option (again, avoid option duplication)
        for (auto it = insertMenuActionsChild.begin();
                 it != insertMenuActionsChild.end();
                 ++it) {
            it.value()->setVisible(actionInsert_Custom_Children->isChecked()
                                   && permittedChildInserts.contains(it.key())
                                   && it.key() != childType);
        }
        // And finally for the context menu children / row insert options
        for (auto it = insertContextActionsChild.begin();
                 it != insertContextActionsChild.end();
                 ++it) {
            it.value()->setVisible(actionInsert_Custom_Children->isChecked()
                                   && permittedChildInserts.contains(it.key())
                                   && it.key() != childType);
        }
        for (auto it = insertContextActionsRow.begin();
                 it != insertContextActionsRow.end();
                 ++it) {
            it.value()->setVisible(actionInsert_Custom_Peer_Rows->isChecked()
                                   && permittedChildInserts.contains(it.key())
                                   && it.key() != childType);
        }

    } else {
        currentlyActiveList = ListActive::None;
    }
}

// --------------------------------------------------------------------------------
// OPTIONS WINDOW

// Options list: Item double clicked
void MainWindow::populateOptions(OptionsMap availableOptions,
                                 OptionsMap currentSelections,
                                 bool checkBoxType)
{
    // Other objects need to know about the item associated with the option
    options = availableOptions;
    optionsList->clear();

    // Convert to presentation order
    QMap<int, QString> presentationOrder;
    for (auto iter = options.keyValueBegin();
         iter != options.keyValueEnd();
         ++iter) {
        int order = (*iter).second->getReferenceNumber();
        QString optionText = (*iter).first;
        presentationOrder.insert(order, optionText);
    }

    // The widget will delete the new items
    for (auto option : presentationOrder) {
        QListWidgetItem* listOption = new QListWidgetItem(option, optionsList);
        if (checkBoxType) {
            if (currentSelections.keys().contains(option)) {
                listOption->setCheckState(Qt::Checked);
            } else {
                listOption->setCheckState(Qt::Unchecked);
            }
        }
    }
}

void MainWindow::on_optionsList_itemDoubleClicked(QListWidgetItem *item)
{
    TreeItem *target = options.value(item->text());
    QModelIndex currentIndex = view->selectionModel()->currentIndex();

    switch (currentlyActiveList) {
    case ListActive::None:
        break;
    case ListActive::CreateLink:
        model->createLink(currentIndex, target);
        optionsList->clear();
        break;
    case ListActive::AddOrRemoveReferences:
        if (model->isReferencingItemAt(currentIndex)) {
            if (item->checkState()) {
                item->setCheckState(Qt::Unchecked);
            } else {
                item->setCheckState(Qt::Checked);
            }
        }
        break;
    }
    model->layoutChanged();
}

void MainWindow::on_optionsList_itemChanged(QListWidgetItem *item)
{
    TreeItem *target = options.value(item->text());
    QModelIndex currentIndex = view->selectionModel()->currentIndex();
    if (item->checkState()) {
        model->makeReference(currentIndex, target);
    } else {
        model->removeReference(currentIndex, target);
    }
    model->layoutChanged();
}
