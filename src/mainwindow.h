/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include "treemodeltypes.h"

#include <QMainWindow>

 /*
 This application allows the entry of data in a tree format
 with numbering, renumbering, linking and cross referencing.
 (Refer to the readme and manual for a full description of
 the functionality.)

 The data is stored down in a human readable save file, in YAML
 format. YAML parsing is non-trivial, therefore this software
 leverages the open source software library 'yaml-cpp'.

 The structure is  view <----> model
                                 ^
                                 |
                   controller<---'

 (There is only very limited interaction between the view
 and the controller directly)

 The GUI data entry and viewing model is implemented in
 MyTreeView, which customises QTreeView by inheritance.
 Some aspects of control, such as user data entry, are
 handled by the MyTreeView class, which is also entirely
 responsible for the 'view' of the tree. Other aspects
 of control, such as context and drop-down items, are
 dealt with by main window, which can be thought of as the
 'controller'.

 The GUI is designed by opening mainwindow.ui. Compiling after
 design changes will make addtional variables available.

  The 'view' stores data in the 'model', and the 'controller'
 interacts with the model by the following methods
 a) that the view is 'set' on the TreeModel 'model'
    ( : QAbstractItemModel)
 b) the TreeModel implements certain key overrides of
    QAbstractItemModel, and these govern how the data
    is entered into the model - they are called by the
    view when it has new data - and also allow the view
    to retrieve the data from the model.
 c) The TreeModel provides several other functions, besides
    the overrides, which allow the MainWindow to interact
    with the model (make references, insert rows, etc)
 d) The TreeModel instance stores the data into a tree
    structure of type TreeItem - this a data structure
    that allows for nested data storage - the data is stored
    in items produced by the factory.
 e) The Factory: is a singleton that will produce an object
    of any registered type on behalf of the tree model
 f) Classes that inherit TreeItem:
    <--ReferenceItem:  Implements reference numbers, renumbering, linking
    <--CrossReference: Implements cross-referencing if required
    <--DerivedClasses: Implement type specific properties.

 Special properties of the view (highlighting cells, resizing rows, etc)
 are implemented through either overrides of QTreeView or overrides
 of the QAbstractItemDelegate that is 'set' by the view.

 The Main Window can call load functions that use the derived types of
 TreeModel. These load the relevant data in from file during construction.

 The YAML load and save make calls to the YAML library - this must be
 installed for the program to run.

 Finally, there are the output methods. These will output the analysis
 as a markdown document. At the moment, these are simple text files,
 but these could be expanded into something more elaborate, such as
 HTML.

*/

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    using ActionMap = QMap<QString, QAction*>;

    MainWindow(QWidget *parent = nullptr);
    enum ListActive {None, CreateLink, AddOrRemoveReferences };
    ~MainWindow();

    typedef QMap<QString, TreeItem *> OptionsMap;
    enum OutputType { FullMarkdown, Requirements };

private:
    TreeModel *model = nullptr;
    TreeModel *newModel = nullptr;
    ItemDelegate *delegate = nullptr;
    OptionsMap options;
    QTemporaryFile *tempfile = nullptr;
    QString rowType = QString();

    QStringList headers;
    QAction* contextCreateTargetLink;
    QAction* contextAddRemoveReferences;
    QAction* contextSeverLink;
    QAction* contextInsertChild;
    QAction* contextInsertRow;
    QAction* contextDeleteRow;

    ActionMap insertMenuActionsRow;
    ActionMap insertMenuActionsChild;
    ActionMap insertContextActionsChild;
    ActionMap insertContextActionsRow;

    ListActive currentlyActiveList = ListActive::None;

    void populateOptions(OptionsMap availableOptions,
                         OptionsMap currentSelections = OptionsMap(),
                         bool checkBoxType = false);

    QAction *separator();
signals:
    void sendSpeedMode(bool);

public slots:
    void updateActions();

private slots:
    void insertChild(QString specialType = QString());
    bool insertColumn();
    void insertRow(QString specialType = QString());
    bool removeColumn();
    void removeRow();
    void loadStpaFile(QString fileName = QString());
    void saveStpaFile(bool backup = false);
    void loadYamlFile(QString fileName = QString());
    void saveYamlFile(bool backup = false);
    void createLinkToTarget();
    void severLink();
    void on_optionsList_itemDoubleClicked(QListWidgetItem *item);
    void showReferenceTargetsAddRemove();
    void on_optionsList_itemChanged(QListWidgetItem *item);
    void expandToType(const QString &type, QAction *associatedAction);
    void setHighlighting(const QString &type);
    void clearHighlighting(const QString &type);
    void toggleHighlighting(const QString &type, QAction *associatedAction);
    void clearAllHighlighting();
    void writeHandbookMarkdown(const OutputType output);
    void renumberCategory();
};

#endif // MAINWINDOW_H
