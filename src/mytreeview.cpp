/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

/*********************************************************************************************
 * mytreeview.cpp
 *********************************************************************************************
 *
 * This file holds the overrides for QTreeView / QStyledItemDelegate
 *
 * Any customizations of the view, highlighting, selections etc are found here.
 *
 *
 */

#include "mytreeview.h"
#include <QHeaderView>
#include <QAction>
#include <QPainter>
#include <QDebug>

#include <iostream>

#include "palette.h"

// The delegate exists to retreive size hints for the view
void ItemDelegate::resetTreeModel(TreeModel* newModel) {
    if (newModel != nullptr) {
        model = newModel;
    }
}

/*
 * ItemDelegate::SizeHint
 * ----------------------------------------------------------------------------
 * Every time a column is resized, the view calls sizeHint on all the items
 * visible in that column, passing their model indices as an argument.
 *
 * This function calculates the available column width, accounting for
 * indentation of column zero. Then it returns the required height for a box of
 * width availableWidth to be able to display the text contained at that model
 * index - it uses QFontMetrics for this, which takes into account font / size.
 *
 * The view then adopts the height component of the QSize object returned by
 * this function as the row height for that QModelIndex.
 */
QSize ItemDelegate::sizeHint(const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    // First calculate the available width taking into account identation
    int availableWidth;
    if (index.column() == 0) {
        int depth = 1;
        QModelIndex depthIndex = index;
        while (depthIndex.parent().isValid()) {
            ++depth;
            depthIndex = depthIndex.parent();
        }
        availableWidth = view->columnWidth(0) - depth * view->indentation();
    } else {
        availableWidth = view->columnWidth(index.column());
    }
    availableWidth -= 10;
    int nominalMaxHeight = 200;

    // Calculate the required height based on the font, data, and available width
    QFontMetrics metrics = option.fontMetrics;
    QString data = model->data(index, Qt::EditRole).toString();
    QRect availableRectangle = QRect(0 ,0, availableWidth, nominalMaxHeight);
    QRect bounds = metrics.boundingRect(availableRectangle, Qt::TextWordWrap, data);

    // Return the bounding rectangle as a QSize size hint, but first,
    //  scale it a little and add some padding to clarify the view
    QSize preferredSize(0, bounds.height() * 8 / 7 + 3);
    return preferredSize;
}

/*
 * ItemDelegate::iterateVisible
 * ----------------------------------------------------------------------------
 * This function calculates how many rows of data are displayed in the current
 * view, up to the row of the QModelIndex in question.
 *
 * This can change without reference to the underlying data model, because rows
 * can be expanded in the view, and collapsed, at will. Also, this is not the
 * same as position, because taller rows take up more space.
 */
bool ItemDelegate::iterateVisible(const QModelIndex& index,
                                int& visible,
                                int endItem) const
{
    // Takes the root index, and counts all visible items until endItem
    ++visible;
    bool found = false;
    if (model->getUnique(index) == endItem) {
        found = true;
    } else if (!model->hasChildren(index)) {
        found = false;
    } else if (view->isExpanded(index) || index == view->rootIndex()) {
        for (int i = 0; i < model->rowCount(index); ++i) {
            found = iterateVisible(model->index(i,0, index), visible, endItem);
            if (found)
                break;
        }
    }
    return found;
}

/*
 * ItemDelegate::paint
 * ----------------------------------------------------------------------------
 * This function is passed a QPainter object in order to 'paint', or draw, the
 * cell in the view.
 *
 * (If the view has a delegate set, the view calls the paint function of its
 * set style delegate. The delegate has a default implementation of paint).
 *
 * This override does not need to fully implement the drawing. It simply uses
 * the QPainter to set the cell's colour, and then calls the default
 * implementation, in the base class, to complete the drawing process.
 */
void ItemDelegate::paint (QPainter *painter,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const
{
    // Implementation accesses member variables of underlying object to detect highlight
    QStyleOptionViewItem op(option);

    if (index.internalPointer()) {
        // Check the model index has a valid pointer, if so, convert it
        auto item = static_cast<TreeItem*>(index.internalPointer());

        if (item->isHighlighted()) {
            int visible = 0;
            const QModelIndex rootIndex = view->rootIndex();
            iterateVisible(rootIndex, visible, model->getUnique(index));

            if (index.column() < 2) {
                QColor color = Palette::instance()->getColorForType(item->getReference().type);
                if (visible % 2) {
                    // Leave the color unchanged
                } else {
                    int brightValue = color.value() + 30;
                    if (brightValue > 250) {
                        brightValue = 255;
                    }
                    color = QColor::fromHsv(color.hsvHue(), color.hsvSaturation(), brightValue);
                }
                painter->fillRect(op.rect, color);
            }
        }
    }
    QStyledItemDelegate::paint(painter, op, index);
}

void MyTreeView::setSpeedMode(bool state)
{
    speedModeActive = state;
}

/*
 * MyTreeView::closeEditor
 * ----------------------------------------------------------------------------
 * The view deals with data entry and committing it to the model. When data is
 * entered, the view opens an editor, and later closes it.
 *
 * Here we wish to intercept the closure of the editor to find if the editor
 * is being closed while submitting data, while speedModeActive is true.
 *
 * In either case, the editor is closed - if the above condition is true, the
 * model index of the 'next' cursor position is calculated, that cursor position
 * is selected, and the position is opened for editiing.
 */
// This function allows the sub class to do things like skipping to the next cell, etc
void MyTreeView::closeEditor(QWidget *editor, QAbstractItemDelegate::EndEditHint hint)
{
    // This method is called when speed mode is checked or unchecked
    if (speedModeActive) {
        QTreeView::closeEditor(editor, hint);
        if (hint == QAbstractItemDelegate::SubmitModelCache) {
            QModelIndex index = moveCursor(MoveNext, Qt::NoModifier);
            if (index.isValid()) {
                QPersistentModelIndex persistent(index);
                selectionModel()->setCurrentIndex(persistent, QItemSelectionModel::ClearAndSelect);
                edit(persistent);
            }
        }
    } else {
        QTreeView::closeEditor(editor, QAbstractItemDelegate::NoHint);
    }
}


// Constructor, which allows us to apply custom changes to the underlying class
MyTreeView::MyTreeView(QWidget *parent)
    : QTreeView(parent)
{
    connect(this->header(), &QHeaderView::sectionResized, this, &MyTreeView::onColumnResized);
}

/*
 * MyTreeView::onColumnResized()
 * ----------------------------------------------------------------------------
 * The model emits layoutChanged() when its data changes in such a way that
 * the layout requires update, e.g. data size changed so it might be too big
 * for the column.
 *
 * In this case, the column size has changed - this is a simple mechanism to
 * get the size hints for the rows to be recalculated so that the view is
 * updated correctly.
 */
void MyTreeView::onColumnResized()
{
    // The view requires size hints to be recalculated
    // - the layout has changed because word wrap is applied
    if (model() != nullptr) {
        model()->layoutChanged();
    }
}

/*
 * MyTreeView::onColumnResized()
 * ----------------------------------------------------------------------------
 * Selects the cell with QModelIndex index.
 *
 * The view is responsible for updating the selection model when the user
 * selectes something. Sometimes though, the program itself needs to select a
 * specific cell.
 *
 * This function is called by signal from the model when the user renumbers an
 * item, becuase that results in the item selected being moved in the view. The
 * cursor is moved with the renumbered item, in order to a) re-validate the
 * selected model index, b) avoid confusing the user.
 */
void MyTreeView::updateSelection(const QModelIndex &index)
{
    selectionModel()->clearSelection();
    selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select);
}

bool MyTreeView::iterateExpandToType(const QModelIndex &iterationIndex, const QString &type)
{
    bool found = false;
    // Takes the root index, and counts all visible items until endItem
    for (int i = 0; i < model()->rowCount(iterationIndex); ++i) {
        found |= iterateExpandToType(model()->index(i,0, iterationIndex), type);
    }
    // At this point, found indicates a 'type' within the children -> expand this row
    if (found) {
        expand(iterationIndex);
    }
    // If found now, parents higher up in the recursion will expand
    if (iterationIndex.internalPointer()) {
        auto item = static_cast<TreeItem*>(iterationIndex.internalPointer());
        found |= item->getReference().type == type;
    }
    return found;
}
