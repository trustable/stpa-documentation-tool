/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#ifndef MYTREEVIEW_H
#define MYTREEVIEW_H

#include <QObject>
#include <QTreeView>
#include <QStyledItemDelegate>
#include <QItemDelegate>

#include "treemodel.h"

// -------------------------------------------------------

// This inheritance allows us to specialise key functionality
class MyTreeView : public QTreeView
{
    Q_OBJECT
public:
    MyTreeView(QWidget *parent = nullptr);
//    void resizeEvent(QResizeEvent *e) override;

    void closeEditor(QWidget *editor, QAbstractItemDelegate::EndEditHint hint);

public slots:
    void setSpeedMode(bool state);
    void updateSelection(const QModelIndex &index);
    bool iterateExpandToType(const QModelIndex &iterationIndex, const QString &type);

private slots:
    void onColumnResized();

private:
    bool speedModeActive = false;
    bool dataChangeTriggered = false;

};

// -------------------------------------------------------

// The delegate exists to retreive size hints for the view
class ItemDelegate : public QStyledItemDelegate
{
public:
    ItemDelegate(MyTreeView* viewFromMainWindow,
                 TreeModel* modelFromMainWindow,
                 QObject *parent = nullptr)
                    : QStyledItemDelegate(parent),
                      view(viewFromMainWindow),
                      model(modelFromMainWindow) {}

    QSize sizeHint(const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override;
    void resetTreeModel(TreeModel* newModel);
    void paint(QPainter *painter,
               const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

private:
    bool iterateVisible(const QModelIndex &index, int &visible, int endItem) const;

private:
    MyTreeView *view;
    TreeModel *model;
};





#endif // MYTREEVIEW_H
