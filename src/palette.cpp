/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

// N.B: the spelling 'color' is used in variable names for alignment with QColor

/*
 *  This is a singleton class for colour management.
*/

#include "palette.h"
#include "treeitemtypes.h"

Palette::Palette()
{
    // Default registrations can be located here
    registerTypeColor<UCA>(QColor::fromHsv(0, 70, 220));
    registerTypeColor<ControlAction>(QColor::fromHsv(330, 70, 220));
    registerTypeColor<TrueStatement>(QColor::fromHsv(30, 120, 220));
    registerTypeColor<Belief>(QColor::fromHsv(60, 80, 240));
    registerTypeColor<Cause>(QColor::fromHsv(110, 60, 220));
    registerTypeColor<InformationReceived>(QColor::fromHsv(180, 60, 220));
    registerTypeColor<HowThisCouldOccur>(QColor::fromHsv(230, 60, 220));
    registerTypeColor<Requirement>(QColor::fromHsv(280, 60, 220));
}

Palette* Palette::instance()
{
    if (!singleton) {
        singleton = new Palette;
    }
    return singleton;
}

void Palette::destroy()
{
    delete singleton;
    singleton = nullptr;
}

QColor Palette::getColorForType(const QString& type)
{
    QColor color;
    if (currentColors.contains(type)) {
        color = currentColors.value(type);
    } else {
        // Unregistered type returns default white
        color = QColor::fromHsv(0, 0, 220);
    }
    return color;
}

void Palette::setColorForType(const QString& type, const QColor& color)
{
    if (currentColors.contains(type)) {
        currentColors.remove(type);
        currentColors.insert(type, color);
    } else {
        // If the type isn't registered, null effect
    }
}

void Palette::restoreDefaultColor(const QString& type)
{
    if (currentColors.contains(type)) {
        currentColors.remove(type);
        currentColors.insert(type, defaultColors.value(type));
    } else {
        // If the type isn't registered, null effect
    }
}

bool Palette::validType(const QString& type)
{
    return currentColors.contains(type);
}

auto Palette::registeredHighlights() -> decltype (registeredNames)
{
    return registeredNames;
}



