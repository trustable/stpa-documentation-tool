/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#ifndef COLORS_H
#define COLORS_H

#include <QColor>
#include <QMap>
#include <QVector>

// N.B: the spelling 'color' is used in variable names for alignment with QColor

/* palette.h / .cpp
 * ----------------------------------------------------------------------------
 * This is a singleton class for colour management. The purpose of this class is
 * remove implementation / maintainence workload from derived classes, and provide
 * a convenient mechanism to centralize palette control.
 */


class Palette
{
public:
    using ColorMap = QMap<QString, QColor>;
    QColor getColorForType(const QString& type);
    void setColorForType(const QString& type, const QColor& color);
    void restoreDefaultColor(const QString& type);

    template<typename T>
    void registerTypeColor(QColor color);

    template<typename T>
    void deregisterTypeColor();

    bool validType(const QString& type);
    static Palette *instance();
    static void destroy();

private:
    Palette();
    ~Palette() = default;

    static inline Palette *singleton = nullptr;
    ColorMap defaultColors;
    ColorMap currentColors;
    int nextAllocation = 0;
    int allocationStep = 30;
    QVector<std::pair<QString, QString>> registeredNames;
public:
    decltype(registeredNames) registeredHighlights();

};

template <typename T>
void Palette::registerTypeColor(QColor color)
{
    QString type = T::classType();
    // Check the type isn't already registered
    if (!currentColors.contains(type)) {
        // Current color is furnished with default color at init
        defaultColors.insert(type, color);
        currentColors.insert(type, color);
        registeredNames.push_back(std::pair{type, T::classCellType()});
    }
}

template <typename T>
void Palette::deregisterTypeColor()
{
    QString type = T::classType();
    defaultColors.remove(type);
    currentColors.remove(type);
    auto it = registeredNames.begin();
    for (it; it != registeredNames.end(); ++it) {
        if (it->first == type) {
            break;
        }
    }
    registeredNames.erase(it);
}


#endif // COLORS_H
