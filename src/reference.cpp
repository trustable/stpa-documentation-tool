/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#include "reference.h"

bool Reference::operator<(const Reference &right) const
{
    bool retVal;
    if (type == right.type) {
        if (parentNumber == right.parentNumber) {
            retVal = number < right.number;
        } else {
            retVal = parentNumber < right.parentNumber;
        }
    } else {
        retVal = type < right.type;
    }
    return retVal;
}

bool Reference::operator==(const Reference &rhs)
{
    return ((type == rhs.type)
             && (parentNumber == rhs.parentNumber)
             && (number == rhs.number));
    // The operator is used to compare the reference information,
    //  the id number plays no role in this
}

bool Reference::operator!=(const Reference &rhs)
{
    return (!operator==(rhs));
}

// To facilitate getting objects of a specific type
bool Reference::typeComp(const Reference &left, const Reference &right)
{
   return left.type < right.type;
};
