/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

/*
 * reference.h / .cpp
 * ----------------------------------------------------------------------------
 * This is a utility class for handling references. In order for uniquely defined
 * references to exist as keys in a map, they must take into account type and
 * parent.
 *
 * This simple class provides three mathematical operators so that simple
 * comparisons and map searches can be carried out, and typeComp, which is
 * a comparator that ignores parent and number.
 *
 */

#ifndef REFERENCE_H
#define REFERENCE_H

#include <QString>

struct Reference
{
    Reference() = default;
    explicit Reference(const QString &argType, int argParentReference, int argReferenceNumber)
        : type(argType),
          parentNumber (argParentReference),
          number (argReferenceNumber)
    {}
    QString type;
    int parentNumber = 0;
    int number = 0;

    bool operator==(const Reference &rhs);
    bool operator!=(const Reference &rhs);
    bool operator< (const Reference &rhs) const;

    static bool typeComp(const Reference &left, const Reference &right);
};

#endif // REFERENCE_H
