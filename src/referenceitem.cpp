/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#include "referenceitem.h"

#include <QMultiMap>
#include <QMap>

#include <QDebug>


// -----------------------------------------------------------------------------

ReferenceItem::ReferenceItem(const QVector<QVariant> &data,
                                QString derivedType,
                                TreeItem *parent,
                                int reference,
                                bool linkable,
                                bool referenceOnParent)
                    : TreeItem (data, parent),
                      type(derivedType),
                      itemIsLinkable(linkable),
                      makesReferenceOnParent(referenceOnParent)
{
    // Definition: A 'category' of keys are those with a common type and parent
    //  - new keys are added to a category with reference monotonic from 1

    // If the parent exists and is required, then include the parent ref num
    int parentRefNum = parent && setupReferencesOnParent() ? parent->getReferenceNumber() : 0;

    Reference key(derivedType,
                     parentRefNum,
                     reference);

    bool referenceExists = getReferenceMap()->keys().contains(key);

    // If the reference number is zero or pre-existing non linkable, get a new one
    if (reference == 0 || (referenceExists && !isLinkable())) {
        key = createNextAvailableReference(key);
    }
    setItemReferenceNumber(key);
}

ReferenceItem::~ReferenceItem()
{
    getReferenceMap()->remove(reference, this);
}

// -----------------------------------------------------------------------------

Reference ReferenceItem::createNextAvailableReference(const Reference &key)
{
    Reference newReference = key;

    // Bounding keys will find the iterators of the 1/2 open range of this category
    Reference upperBoundKey(key.type, key.parentNumber + 1, 0);
    Reference lowerBoundKey(key.type, key.parentNumber, 0);

    // upperIt points to one past the max value in the category, or .end()
    auto upperIt = getReferenceMap()->lowerBound(upperBoundKey);
    // lowerIt points to the lowest reference number in the category, or .end()
    auto lowerIt = getReferenceMap()->lowerBound(lowerBoundKey);

    // This means there are no keys in this category
    if (lowerIt == upperIt) {
        newReference.number = 1;
    } else {
        auto iter = upperIt;
        // In principal, take the last number in the category an add 1
        --iter;
        int maxReference = iter.key().number + 1;
        // The code below exists to accommodate extended range values

        // Check that 'extended range' values are not present when adding
        // non-extended range values
        if (key.number <= maxStdMapIndex) {
            while (iter.key().number > maxStdMapIndex
                    && lowerIt != iter ) {
                // Scroll back to the smallest unused non-exteneded range value
                --iter;
            }
            if (iter.key().number > maxStdMapIndex) {
                // iter hit the lower-bound of the range before a lower range value
                newReference.number = 1;
            } else if (iter.key().number < maxStdMapIndex) {
                // A normal value has been found and the range is not full
                newReference.number = iter.key().number + 1;
            } else {
                // The normal range is full, so the value just has to go on the end
                newReference.number = maxReference;
            }
        } else {
            // new extended range values always go on the end
            newReference.number = maxReference;
        }
    }
    return newReference;
}
// -----------------------------------------------------------------------------

const QString ReferenceItem::getReferenceAsString(bool brackets) const
{
    QString text = referenceText;
    if (!brackets) {
        text.remove('[');
        text.remove(']');
    }
    return text;
}

// -----------------------------------------------------------------------------

// This exists to account for the parent number presence in parent dependent types
QString ReferenceItem::getFullReferenceStem() const
{
    QString fullStem = getReferenceStem();
    if (setupReferencesOnParent() && parent()) {
        fullStem += QString("%1.").arg(parent()->getReferenceNumber());
    }
    return fullStem;
}

// -----------------------------------------------------------------------------

// This is for class internal use: It plucks one Reference out of the map and
// enters another.
void ReferenceItem::setItemReferenceNumber(Reference key)
{
    // refNumber zero indicates no map entry, so cannot be assigned
    if (key.number) {
        // Delete the old map entry and replace it
        if (reference.number) {
            getReferenceMap()->remove(reference, this);
        }
        reference = key;
        getReferenceMap()->insert(key, this);
    }
    updateItemReference();
}

// Convenience: supply the new item number only
void ReferenceItem::setItemReferenceNumber(int newNumber)
{
    Reference newKey = reference;
    newKey.number = newNumber;
    return setItemReferenceNumber(newKey);
}

// Sometimes the item reference is updated without renumbering the item
// For example: when the parent Control number changes
void ReferenceItem::updateItemReference()
{
    // Update the text / data for the model
    QString referenceStem = getFullReferenceStem();
    referenceText = QString("[%1%2]").arg(referenceStem).arg(reference.number);
    TreeItem::setData(1, referenceText);
}

// -----------------------------------------------------------------------------

// Get the reference option currently associated with this object
QString ReferenceItem::getReferenceAsOption()
{
    QString option = getReferenceAsString() + " " + data(0).toString();
    return option;
}

// -----------------------------------------------------------------------------

// Populates a map of with all references with Reference.type == type
OptionsMap ReferenceItem::getPeerReferences(const QString &type)
{
    // This function knows the type that is sought, but not which type is 'next'
    // in the map - this makes use of upperBound impossible - a custom comparator
    // is invoked that considers Reference.type only

    OptionsMap options;
    Reference typeKey;
    if (!type.isEmpty()) {
        typeKey = Reference(type, 0, 0);

        // The lowerBound the first key for which key < typeKey is false
        auto lowerIt = getReferenceMap()->lowerBound(typeKey);
        for (auto it = lowerIt; it != getReferenceMap()->end(); ++it) {
            if (Reference::typeComp(typeKey, it.key())) {
                break;
            }
            TreeItem *treeItem = it.value();
            QString option = treeItem->getReferenceAsString() + " " + treeItem->data(0).toString();
            options.insert(option, treeItem);
        }
    }
    return options;
}

// -----------------------------------------------------------------------------

// Items with the same reference are updated simultaneously
bool ReferenceItem::setData(int column, const QVariant &value)
{
    bool returnValue = true;
    if (isLinkable()) {
        // Find each item (value) in this category that has the key refNumber
        for (auto item : getReferenceMap()->values(reference)) {
            returnValue &= item->TreeItem::setData(column, value);
        }
    } else {
        returnValue = TreeItem::setData(column, value);
    }
    return returnValue;
}

// -----------------------------------------------------------------------------

bool ReferenceItem::isLinked()
{
    return (getReferenceMap()->values(reference).count() > 1);
}

// -----------------------------------------------------------------------------

bool ReferenceItem::renumberReference(int newNumber)
{
    Reference newKey = reference;
    newKey.number = newNumber;
    return renumberReference(newKey);
}

bool ReferenceItem::renumberReference(Reference newReference)
{
    bool returnValue = true;
    if (getReferenceMap()->keys().contains(newReference)) {
        returnValue = false;
    } else {
        // It's actually a 4 stage process:
        QSet<TreeItem*> parents;
        for (auto item : getReferenceMap()->values(reference)) {
            // Renumber all items with this reference number individually
            item->setItemReferenceNumber(newReference);

            // Check for children that need a reference update
            if (!childItems.isEmpty()) {
                for (auto child : childItems) {
                    if (child->setupReferencesOnParent()) {
                        child->updateChildReferenceOnParent();
                    }
                }
            }
            // Note which parent items are affected for later
            parents.insert(item->parent());

            // Update items referring to this item - if the item inherits
            // CrossReference then the appropriate override will be called
            refreshReferences();
        }

        // re-sort the renumbered items among their siblings
        for (TreeItem * parent : parents) {
            parent->sortChildrenByReferenceNumbers();
        }
    }
    return returnValue;
}

// Change the reference of an object to a new parent reference - update parent first
void ReferenceItem::updateChildReferenceOnParent()
{
    Reference newReference = reference;
    newReference.parentNumber = parent()->getReferenceNumber();
    setItemReferenceNumber(newReference);
    updateItemReference();
}

// -----------------------------------------------------------------------------

void ReferenceItem::renumberAllReferences()
{
    // We want this to be category specific - for example, renumber all
    // UCA-6.x or CA-2.x or H-x

    // Unlike getPeerReferences, we do not want all references in the type,
    // just the category

    Reference lowerBoundKey(reference.type, reference.parentNumber, 0);
    Reference upperBoundKey(reference.type, reference.parentNumber + 1, 0);
    auto upperIt = getReferenceMap()->lowerBound(upperBoundKey);
    auto lowerIt = getReferenceMap()->lowerBound(lowerBoundKey);

    int currentReference = 0;
    // The renumber function will take care of all keys with the same value
    for (auto it = lowerIt; it != upperIt; ++it) {
        // Do not renumber special items
        if(it.key().number > maxStdMapIndex) {
            break;
        }
        if (++currentReference < it.key().number) {
            it.value()->renumberReference(currentReference);
        }
    }
}

// -----------------------------------------------------------------------------

// This function allows one item to link to another item of the same type
bool ReferenceItem::createLink(TreeItem *targetItem)
{
    if (isLinkable()) {
        // Update the reference number to match the target
        previousReference = reference;
        previousData = data(0).toString();

        // Use TreeItem::setData to avoid a loop
        setItemReferenceNumber(targetItem->getReferenceNumber());
        TreeItem::setData(0, targetItem->data(0));
    }
    return isLinkable();
}

// -----------------------------------------------------------------------------

bool ReferenceItem::severLink()
{
    int returnValue;
    // Can only sever a link if there is a link to sever!
    Reference reference;
    if (isLinked()) {
        // If the item's previous reference is available, restore it
        if ((previousReference == Reference())
                || (getReferenceMap()->keys().contains(previousReference))) {
            reference = createNextAvailableReference(previousReference);
            previousData = QString("[No data]");
        } else {
            reference = previousReference;
        }
        TreeItem::setData(0, previousData);
        setItemReferenceNumber(reference);
        returnValue = true;
    } else {
        returnValue = false;
    }
    return returnValue;
}





