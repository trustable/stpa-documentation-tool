/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

/* referenceitem.h
 * -----------------------------------------------------------------------------
 * This class is responsible for reference functionality.
 *
 * A Reference consists of a type, parent number, and item number. RefererableItems
 * constructor will generate a Reference based on the parameters available.
 *
 * 1) The type is supplied by the constructor in terms of the item's class.
 * 2) The parent number is supplied by the item's parent, but is set to zero if the
 *    types objects do not use references dependent on their parents.
 * 3) The 'number' is either specified, or left as zero to indicate 'generate a new one'.
 *
 * The reference is inserted to a map of <Reference, TreeItem*> on the following rules:
 *
 * Reference supplied does not exist ----------> Reference is inserted in the map
 * Reference supplied exists, linkable class --> Reference is inserted in the map
 * Reference supplied exists, non-linkable ----> New Reference is generated & inserted
 * Reference supplied with zero item number ---> New Reference is generated & inserted
 *
 * THIS MEANS: 'Object pointed by TreeItem * exists in the tree with reference 'Reference'
 *
 * New references are generated with the same type and parent, with item number +1 greater
 * than the highest reference in use of that type. (References numbers greater than
 * 10000 (max) are considered a separate order, and do not paricipate in output.)
 *
 * The map can be used to look up all items with a specific type, e.g. "H-x"; all items
 * with a specific type and parent, e.g. "UCA-6.x" or all items with the same reference,
 * e.g. "H-4", or "UCA-6.4".
 *
 * Items with an identical Reference number are 'linked'. When a linked item is updated
 * or renumbered, all other items with the same reference number are updated.
 *
 * References: Can be retrieved as Reference, int, or string; the type and stem type
 * can also be retrieved.
 *
 * Renumber: this is a little more complex - First, an item cannot be renumbered
 * to a Reference that is already in use, because that would link them. Second, given
 * a valid target:
 *  1) All items with the same reference will also be renumbered
 *  2) All children that have a reference depending on any item renumbered in step 1
 *     will have their parent component of their reference renumbered.
 *  3) All cross references to any item renumbered in step 1 must be updated.
 *  4) Cross references to child items will also be updated.
 *
 * Linking: isLinkable(), isLinked(), createLink(), severLink() functions facilitate
 * an item being linked: renumbered to have the same Reference as that of an existing
 * item.
 *
 * getPeerReferences(type) returns a map containing all the objects within a particular
 * type, keyed by their reference as a string. This allows for options to be presented
 * to the GUI for:
 *  1) linking, all items in this class;
 *  2) xreferencing, all items in the target class.
 */

#ifndef REFERABLEITEM_H
#define REFERABLEITEM_H

#include "treeitem.h"

enum LinkStatus { Success, NotLinkable, NoLink };

class ReferableTests;

class ReferenceItem : virtual public TreeItem
{
public:
    // Receive this information from the derived class
    ReferenceItem(const QVector<QVariant> &data,
                  QString derivedType,
                  TreeItem *parent = nullptr,
                  int reference = 0,
                  bool linkable = false,
                  bool makesReferenceOnParent = false
                  );
    ~ReferenceItem() override;
    bool isReferenceType() const override {return true;}

    // Critical in reference creation
    Reference createNextAvailableReference(const Reference &key);

    // Reference data
    const QString getReferenceAsString(bool brackets = true) const override;
    int getReferenceNumber() const override {return reference.number;}
    Reference getReference() const override {return reference;}

    // Class data provided from the derived class constructor
    QString getReferenceStem() const override {return type + "-";}
    QString getReferenceType() const override {return type;}

    // For renumbering
    bool renumberReference(int newNumber) override;
    bool renumberReference(Reference newReference) override;
    void renumberAllReferences() override;
    void updateChildReferenceOnParent() override;

    // Renumbering is achieved through this override of setdata
    bool setData(int column, const QVariant &value) override;

    // Information from the derived class constructor
    bool isLinkable() override {return itemIsLinkable;}

    // Allows one item to adopt the reference of another
    bool isLinked() override;
    bool createLink(TreeItem* targetItem) override;
    bool severLink() override;

    // Get all references of type 'type' - defaults to *this.reference.type
    OptionsMap getPeerReferences(const QString &type = QString()) override;
    QString getReferenceAsOption() override;

    // Mechanism for testing the private functions
    friend ReferableTests;

protected:
    // Supports multiple values and multiple subcategories
    static const int maxStdMapIndex = 10000;

    QString getFullReferenceStem() const;
private:
    // To actually change the data that is returned to the GUI
    void setItemReferenceNumber(Reference key) override;
    void setItemReferenceNumber(int newNumber) override;
    bool setupReferencesOnParent() const override {return makesReferenceOnParent;}
    Reference reference;
    QString referenceText;

    // For severing links
    QString previousData = QString("[No data]");
    Reference previousReference;

    // This information is supplied by the (derived) constructor
    QString type;
    bool itemIsLinkable;
    bool makesReferenceOnParent;

    // To update a reference text without changin reference.number
    void updateItemReference();
};

#endif // REFERABLEITEM_H

