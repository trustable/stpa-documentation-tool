/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

/*
    treeitem.cpp

    A container for items of data supplied by the simple tree model.
*/

#include "treeitem.h"
#include "factory.h"
#include "palette.h"

#include <QStringList>
#include <QString>

// ----------------------------------------------------------------------------

TreeItem::TreeItem(const QVector<QVariant> &data,
                   TreeItem *parent,
                   int uniqueID,
                   int reference)
     : itemData(data), parentItem(parent)
{
    Q_UNUSED(reference);
    // The root will be passed down from the parent, if the parent is nullptr
    // then this node *is* a root
    if (parent) {
        root = parent->root;
    } else {
        itemMap = new ItemMap;
        referenceMap = new ReferenceMap;
        xRefMap = new CrossRefMap;
        root = this;
    }

    if (getItemMap()->isEmpty()) {
        uniqueID = 0;
    } else {
        if (uniqueID == 0 || getItemMap()->keys().contains(uniqueID)) {
            uniqueID = getItemMap()->lastKey() + 1;
        }
    }
    getItemMap()->insert(uniqueID, this);
    internalUniqueID = uniqueID;
}

TreeItem::~TreeItem()
{
    getItemMap()->remove(internalUniqueID);
    qDeleteAll(childItems);

    // For items deleted independtly of parent, check the parent is updated
    if (parentItem) {
        int index = parentItem->childItems.indexOf(this);
        if (index != -1) {
            parentItem->childItems.removeAt(index);
        }
    }
    if (this == root) {
        delete itemMap;
        delete referenceMap;
        delete xRefMap;
    }
}

// ----------------------------------------------------------------------------
// Critical map functions

// (returning nullptr can be checked in the calling function)
TreeItem::ItemMap *TreeItem::getItemMap()
{
    if (root)
        return root->itemMap;
    return nullptr;
}

TreeItem::ReferenceMap *TreeItem::getReferenceMap()
{
    if (root)
        return root->referenceMap;
    return nullptr;
}

QMultiMap<TreeItem*, TreeItem*>* TreeItem::getXRefMap() const
{
    if (root)
        return root->xRefMap;
    return nullptr;
}

// ----------------------------------------------------------------------------

TreeItem *TreeItem::child(int number)
{
    return childItems.value(number);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}


int TreeItem::columnCount() const
{
    return itemData.count();
}

QVariant TreeItem::data(int column) const
{
    return itemData.value(column);
}

// This function is overridden in the derrived classes to ensure that the
// correct type of child is inserted
bool TreeItem::insertChildren(int position, int count, int columns, QString type,
                              int reference, int uniqueID)
{
    // Generic base does not use references or stems, but the derived types do
    bool retVal = true;
    if (insertsTypes().isEmpty()) {
        retVal = false;
    } else if (position < 0 || position > childItems.size()) {
        retVal = false;
    } else {
        if (type.isEmpty()) {
            type = defaultChildType();
        }
        for (int row = 0; row < count; ++row) {
            QVector<QVariant> data(columns);
            TreeItem * item = Factory::factory()->create(type, data, this,
                                                         uniqueID, reference);
            if (item) {
                childItems.insert(position, item);
            } else {
                retVal = false;
            }
        }
    }
    return retVal;
}

bool TreeItem::insertColumns(int position, int columns)
{
    if (position < 0 || position > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.insert(position, QVariant());

    foreach (TreeItem *child, childItems)
        child->insertColumns(position, columns);

    return true;
}

TreeItem *TreeItem::parent() const
{
    return parentItem;
}

bool TreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        // References are deleted in the object destructors
        delete childItems.takeAt(position);
    }
    return true;
}


bool TreeItem::removeColumns(int position, int columns)
{
    if (position < 0 || position + columns > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.remove(position);

    foreach (TreeItem *child, childItems)
        child->removeColumns(position, columns);

    return true;
}

bool TreeItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= itemData.size())
        return false;

    itemData[column] = value;
    return true;
}

// -----------------------------------------------------------------------------

QStringList TreeItem::convertTreeToStringList() const
{
    // Skip root level, reduce identation level by 1
    QStringList lines;
    for (const auto& childItem : childItems) {
       childItem->recurseTreeToSaveFileList(lines);
    }
    return lines;
}

void TreeItem::recurseTreeToSaveFileList(QStringList &lines, int indentationLevel)
{
    // Print each item line and then recurse through its children
    QString line;
    for (int i = 0; i < indentationLevel; ++i) {
        line += "    ";
    }
    for (int i = 0; i < columnCount(); ++i) {
        line += data(i).value<QString>() + "\t";
    }
    // Last, the unique ID is added which does not appear in the model / view
    line += QString("%1").arg(internalUniqueID);
    lines.push_back(line);

    ++indentationLevel;
    for (const auto& childItem : childItems) {
       childItem->recurseTreeToSaveFileList(lines, indentationLevel);
    }
}

// -----------------------------------------------------------------------------

QStringList TreeItem::convertTreeToMarkdownList() const
{
    // Skip root level
    QStringList lines;
    for (const auto& childItem : childItems) {
       childItem->recurseTreeToMarkdownList(lines);
    }
    return lines;
}

void TreeItem::recurseTreeToMarkdownList(QStringList &lines)
{
    // Allow each class to deal with its own formatting
    for (QString feature : specialMarkdownFormat()) {
        lines.push_back(feature);
    }

    for (const auto& childItem : childItems) {
        // Allow for extra indentation of requirements where appropriate
        if (childItem->isRequirement()) {
            if (isBullettedFormat()) {
                lines.push_back(QString("  "));
            } else {
                lines.push_back(QString("\n"));
            }
        }
        childItem->recurseTreeToMarkdownList(lines);
    }
}

QStringList TreeItem::specialMarkdownFormat()
{
    QStringList format;
    QString line = "\n" + getMarkdownFormatString() + data(0).toString() + "\n";
    format.push_back(line);
    return format;
}

// -----------------------------------------------------------------------------

void TreeItem::recurseTreeToReferencingList(QList<TreeItem*> &items)
{
    if (isReferenceType()) {
        items.push_back(this);
    }
    for (auto child : childItems) {
        child->recurseTreeToReferencingList(items);
    }
}

// -----------------------------------------------------------------------------

// This function is for inserting children into the root, where the child type
// cannot be automatically determined, and is passed to the function
bool TreeItem::insertSpecial(TreeItem * item)
{
    if (item == nullptr)
        return false;

    childItems.insert(childCount(), item);
    return true;
}

// -----------------------------------------------------------------------------
void TreeItem::sortChildrenByReferenceNumbers()
{
    // The sort is performed first lexically on the stem , i.e : CA- is < UCA-
    // then numerically on the number ie UCA-10 is > UCA-2
    std::sort(childItems.begin(),
              childItems.end(),
              [] (TreeItem *left, TreeItem *right) {
                    bool lessThan;
                    int initialResult = QString::compare(left->getReferenceStem(), right->getReferenceStem());
                    if (initialResult == 0) {
                        lessThan = left->getReferenceNumber() < right->getReferenceNumber();
                    } else {
                        lessThan = (initialResult < 0);
                    }
                    return lessThan;
              });
}

// -----------------------------------------------------------------------------
bool TreeItem::checkHasContents() const
{
    bool hasContents = true;
    for (QString marker : emptyMarkers) {
        // Compare returns zero on equal
        if (!QString::compare(marker, data(0).toString(), Qt::CaseInsensitive)) {
            hasContents = false;
            break;
        }
    }
    return hasContents;
}

// -----------------------------------------------------------------------------
// These functions are used in the tree structure to format output
bool TreeItem::getParentBannerState() const
{
    bool result = false;
    if (parent()) {
        result = parent()->getParentalBannerState();
    }
    return result;
}

void TreeItem::setParentBannerState(bool state)
{
    if (parent()) {
        parent()->setParentalBannerState(state);
    }
}

bool TreeItem::treeHighlightStates(const QString& type, bool state)
{
    bool result;
    if (Palette::instance()->validType(type)) {
        recurseTreeHighlightStates(type, state);
        result = true;
    } else {
        result = false;
    }
    return result;
}

void TreeItem::recurseTreeHighlightStates(const QString& type, bool state)
{
    if (getReference().type == type) {
        setHighlighted(state);
    }
    for (auto child : childItems) {
        child->recurseTreeHighlightStates(type, state);
    }
}

// ----------------------------------------------------------------------------

// Trivial implementations for functions overridden in Reference class

bool TreeItem::renumberReference(Reference newReference)
{
    Q_UNUSED(newReference);
    return false;
}
bool TreeItem::renumberReference(int newReference)
{
    Q_UNUSED(newReference);
    return false;
}
bool TreeItem::createLink(TreeItem* linkTarget)
{
    Q_UNUSED(linkTarget);
    return false;
}


// ----------------------------------------------------------------------------
// Trivial implementations for functions overridden in CrossReference class
OptionsMap TreeItem::getReferenceTargetOptions()
{
    return QMap<QString, TreeItem *>
            {{QString("Not implemented as a referencing type"), nullptr}};
}
OptionsMap TreeItem::getReferencesMade(bool asOptions) const
{
    Q_UNUSED(asOptions);
    return QMap<QString, TreeItem *>
            {{QString("Not implemented as a referencing type"), nullptr}};
}

OptionsMap TreeItem::getPeerReferences(const QString &type)
{
    Q_UNUSED(type);
    return QMap<QString, TreeItem *>{{QString("Not implemented as a reference target"),
                                      nullptr}};
}
