﻿/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

/* treeitem.h
 * ----------------------------------------------------------------------------
 * The TreeItem class is the basis of the data storage model for the
 * application. It is used by the model, but makes no reference to the model.
 *
 * The TreeItem represents an individual row in one of the model's tables.
 *
 * The TreeItem can possess three maps to be used by its children. If the item
 * is constructed with a 'nullptr' parent, then it will be a 'root' and possess
 * the maps. If constructed with a TreeItem* as parent, it will refer to the
 * root item to access the maps. This avoid the use of static maps, and allows
 * multiple tree structures to exist simultaneously without conflict.
 *
 * The basic access functions are presented first, here, to access a parent,
 * child, or the data of that row.
 *
 * All the functions used by the model must be callable on all objects in the
 * tree structure, regardless of type. So functionality is implemented as virtual
 * functions in the inheriting classes ReferenceItem and CrossReference.
 *
 * The insert / edit overrides allow the model to determine whether an object
 * can be changed, what type it should or can insert using the factory, etc.
 * New items of base type TreeItem are created by the Factory class.
 *
 * The reference virtual functions contain no implementation. Their overrides in
 * ReferenceItem allows getting / setting references, renumbering, getting
 * link options, and making and breaking linkages. Also, these allow references
 * in terms of parent, and update of child references when the parent changes.
 *
 * The Cross Reference virtual funcitons allow cross referencing. This includes
 * making (multiple) x-references, removing x-references, retrieving x-references,
 * and retrieving x-reference options, as well as updating x-references
 * made when an item is renumbered.
 *
*/

#ifndef TREEITEM_H
#define TREEITEM_H

#include <QList>
#include <QVariant>
#include <QVector>
#include <QMultiMap>
#include <QColor>

#include "reference.h"

class TreeItem;
using OptionsMap = QMap<QString, TreeItem *>;

class TreeItem
{
public:

    explicit TreeItem(const QVector<QVariant> &data,
                      TreeItem *parent,
                      int uniqueID = 0,
                      int reference = 0);
    virtual ~TreeItem();

    // ---------------------------------------------------------------------
    // CORE TreeItem functionality

    TreeItem *child(int number);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    bool insertColumns(int position, int columns);
    TreeItem *parent() const;
    bool removeChildren(int position, int count);
    bool removeColumns(int position, int columns);
    int childNumber() const;

    // ---------------------------------------------------------------------
    // STPA implementation

    // --------------- ITEM, REFERENCE AND CROSS-REFERENCE MAPS ------------

    // Maps for the storage of the References and XReferences are held in root
    using ItemMap = QMap<int, TreeItem*>;
    using ReferenceMap = QMultiMap<Reference, TreeItem*>;
    using CrossRefMap = QMultiMap<TreeItem*, TreeItem*>;

    virtual ItemMap *getItemMap();
    virtual ReferenceMap *getReferenceMap();
    virtual CrossRefMap *getXRefMap() const;

    int getUniqueID() const {return internalUniqueID;}

    // ------------------- INSERT, EDIT, AND PRESENT DATA ----------------

    // Any of these markers will be considered as empty - case insensitive
    static inline const QStringList emptyMarkers = {QString(),
                                                    "[No data]",
                                                    "blank",
                                                    "[Belief is not required here]",
                                                    "[How is not required here]"};

    virtual bool setData(int column, const QVariant &value);

    // These functions allow expanded GUI functionality
    virtual bool deleteable() {return true;}
    virtual bool isEditable() {return true;}
    virtual QString celltype() {return QString("Generic");}

    // Interpret certain phrases as 'empty'
    virtual bool checkHasContents() const;

    // Used in validating insert types and inserting child items
    virtual QString parentOf() {return QString();}
    virtual QStringList insertsTypes() {return {QString("GEN")};}
    virtual QString defaultChildType() {return QString("GEN");}

    bool insertSpecial(TreeItem *item = nullptr);
    virtual bool insertChildren(int position, int count, int columns,
                                QString type = QString(),
                                int reference = 0, int uniqueID = 0);

    // Unsafe Control Action versus Control Action is dealt with as a special case
    virtual bool getIsControlAction() const {return false;}
    virtual bool getInsertsControlAction() const {return false;}

    // Markdown functions and save / load helpers
    QStringList convertTreeToStringList() const;
    QStringList convertTreeToMarkdownList() const;
    void recurseTreeToReferencingList(QList<TreeItem *> &items);

    virtual QString getMarkdownFormatString() const {return QString();}
    virtual QStringList specialMarkdownFormat();
    virtual bool isBullettedFormat() {return false;}
    virtual bool isRequirement() {return false;}

    virtual bool getParentalBannerState() const {return bannerStateAsParent;}
    virtual void setParentalBannerState(bool state) {bannerStateAsParent = state;}

    // Cell specific highlight functionality, colours implemented in 'Palette'
    virtual bool isHighlighted() const {return highlighted;}
    virtual void setHighlighted(bool state) {highlighted = state;}
    bool treeHighlightStates(const QString &type, bool state);

    bool getParentBannerState() const;
    void setParentBannerState(bool state);

    static QString classType() {return QString("GEN");}

    // ----------------------- REFERENCE / LINKS -------------------------------
    // All the functionality for creating references and linking item

    virtual bool isReferenceType() const {return false;}
    virtual Reference getReference() const {return Reference();}
    virtual const QString getReferenceAsString(bool brackets = true) const {return QString();}
    virtual int getReferenceNumber() const {return 0;}
    virtual bool setupReferencesOnParent() const {return false;}
    virtual void setItemReferenceNumber(Reference key) {}
    virtual void setItemReferenceNumber(int newNumber) {}

    virtual QString getReferenceAsOption() {return QString();}

    virtual bool renumberReference(Reference newReference);
    virtual bool renumberReference(int newReference);

    virtual void updateChildReferenceOnParent() {}
    virtual void sortChildrenByReferenceNumbers();

    virtual void renumberAllReferences() {}

    virtual QString getReferenceStem() const {return getReferenceType() + "-";}
    virtual QString getReferenceType() const {return classType();}

    virtual bool isLinkable() {return false;}
    virtual bool isLinked() {return false;}
    virtual bool createLink(TreeItem* linkTarget);
    virtual bool severLink() {return false;}

    // ----------------------- CROSS REFERENCES-------------------------------
    // All the functionality from making X-references between items
    // These are implemented in CrossReference

    virtual bool isReferencingItem() {return false;}

    virtual QStringList permittedReferenceTargets() {return QStringList();}
    virtual QString defaultReferenceType() {return QString();}

    virtual void makeReference(TreeItem *referenceTarget) {Q_UNUSED(referenceTarget)}
    virtual void makeReference(Reference referenceTarget) {Q_UNUSED(referenceTarget);}

    virtual void deleteReference(TreeItem *referenceTarget) {Q_UNUSED(referenceTarget)}
    virtual void deleteAllReferences() {}

    virtual QString getReferencesMadeAsString() {return QString();}
    virtual OptionsMap getReferencesMade(bool asOptions = false) const;

    virtual OptionsMap getPeerReferences(const QString &type = QString());
    virtual OptionsMap getReferenceTargetOptions();

    virtual void updateReferencesInModel() {}
    virtual bool isValidreferenceTarget() {return false;}
    virtual void refreshReferences() {}

    static inline QString classCellType() {return "Generic Cell";}
protected:
    // Type of insertation is dependent on the derived
    // class, therefore this is not private
    QList<TreeItem*> childItems;
    TreeItem *root = nullptr;

private:
    void recurseTreeToSaveFileList(QStringList &lines, int indentationLevel = 0);
    void recurseTreeToMarkdownList(QStringList &lines);
    void recurseTreeHighlightStates(const QString &type, bool state);

    QVector<QVariant> itemData;
    TreeItem *parentItem;
    int internalUniqueID;

    bool bannerStateAsParent = false;
    bool highlighted = false;

    // Maps - these will be initialized for root items only
    ItemMap* itemMap = nullptr;
    ReferenceMap* referenceMap = nullptr;
    CrossRefMap* xRefMap = nullptr;
};

#endif // TREEITEM_H
