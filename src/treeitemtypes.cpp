﻿/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include "treeitemtypes.h"

// ----------------------------------------------------------------------------
Loss::Loss(const QVector<QVariant> &data, TreeItem *parent,
           int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

QStringList Loss::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = getMarkdownFormatString()
                    + getReferenceAsString(false) + ":"
                    + data(0).toString().trimmed() + "\n";
    format.push_back(line);
    return format;
}

// ----------------------------------------------------------------------------
Hazard::Hazard(const QVector<QVariant> &data, TreeItem *parent,
               int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

QStringList Hazard::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = getMarkdownFormatString() + getReferenceAsString(false)
            + ": " + data(0).toString().trimmed()
            + " " + getReferencesMadeAsString()+ "\n";
    format.push_back(line);
    return format;
}

// ----------------------------------------------------------------------------
Control::Control(const QVector<QVariant> &data, TreeItem *parent,
                 int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          ReferenceItem (data, classType(), parent, reference,
                         classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

// ----------------------------------------------------------------------------
UCA::UCA(const QVector<QVariant> &data, TreeItem *parent,
         int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

// Because getFullReferenceStem is specialized in UCA, the virtual override
// that calls it must be outline
QString UCA::getMarkdownFormatString() const
{
    return markdownPrefix + getFullReferenceStem()
            + QString("%1").arg(getReferenceNumber()) + ": ";
}

QStringList UCA::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = "\n" + getMarkdownFormatString() + data(0).toString().trimmed()
                    + getReferencesMadeAsString() + "\n";
    format.push_back(line);
    return format;
}

// ----------------------------------------------------------------------------
ControlAction::ControlAction(const QVector<QVariant> &data, TreeItem *parent,
         int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

// Because getFullReferenceStem is specialized in UCA, the virtual override
// that calls it must be outline
QString ControlAction::getMarkdownFormatString() const
{
    return markdownPrefix + getFullReferenceStem()
            + QString("%1").arg(getReferenceNumber()) + ": ";
}

QStringList ControlAction::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = "\n" + getMarkdownFormatString() + data(0).toString().trimmed()
                    + "\n";
    format.push_back(line);
    return format;
}

// ----------------------------------------------------------------------------
TrueStatement::TrueStatement(const QVector<QVariant> &data, TreeItem *parent,
                             int uniqueID, int reference)
    : TreeItem(data, parent, uniqueID),
      CrossReference (data, classType(), parent, reference,
                      classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

QStringList TrueStatement::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = "\n" + getMarkdownFormatString()
            + data(0).toString().trimmed() + getMarkdownFormatString() + "\n";
    format.push_back(line);
    return format;
}

// ----------------------------------------------------------------------------
Belief::Belief(const QVector<QVariant> &data, TreeItem *parent,
               int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

QStringList Belief::specialMarkdownFormat()
{
    QStringList format;
    setParentalBannerState(false);
    if (checkHasContents()) {
        format.push_back("\nBelief:\n\n");
        QString line = getMarkdownFormatString() + data(0).toString().trimmed()
                + getMarkdownFormatString() + "\n\n";
        format.push_back(line);
    }
    return format;
}

// ----------------------------------------------------------------------------
Cause::Cause(const QVector<QVariant> &data, TreeItem *parent,
             int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

QStringList Cause::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString dataString = data(0).toString();

    QString mainLine;

    int index = dataString.indexOf(':');
    if (index > 0) {
        QString scenarioType = dataString.left(index + 1);
        QString scenarioLine = QString("\n%1\n\n").arg(scenarioType);
        format.push_back(scenarioLine);

        QString mainData = dataString.mid(index + 1);
        mainLine = getMarkdownFormatString() + mainData + "\n";
    } else {
        mainLine = "\n" + getMarkdownFormatString() + data(0).toString().trimmed() + "\n";
    }
    format.push_back(mainLine);
    return format;
}


// ----------------------------------------------------------------------------
InformationReceived::InformationReceived(const QVector<QVariant> &data,
                                         TreeItem *parent,
                                         int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}


QStringList InformationReceived::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = "\n" + getMarkdownFormatString()
                    + data(0).toString().trimmed() + "\n";
    format.push_back(line);
    return format;
}


// ----------------------------------------------------------------------------
HowThisCouldOccur::HowThisCouldOccur(const QVector<QVariant> &data,
                                     TreeItem *parent,
                                     int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    // Nothing to do
}

QStringList HowThisCouldOccur::specialMarkdownFormat()
{
    QStringList format;
    if (checkHasContents()) {
        setParentalBannerState(false);
        QString line = "\n" + getMarkdownFormatString() + data(0).toString().trimmed() + "\n";
        format.push_back(line);
    }
    return format;
}

// ----------------------------------------------------------------------------
Requirement
    ::Requirement(const QVector<QVariant> &data,
                               TreeItem *parent,
                               int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          CrossReference (data, classType(), parent, reference,
                          classIsLinkable(), classReferencesParent())
{
    Q_UNUSED(reference);
    // Nothing to do
}

QStringList Requirement::specialMarkdownFormat()
{
    setParentalBannerState(false);
    QStringList format;
    QString line = getMarkdownFormatString() + "**REQUIREMENT "
            + getReferenceAsString(false) + ": " + data(0).toString().trimmed()
            + "**" + "\n\n";
    format.push_back(line);
    return format;
}

QStringList Requirement::specialRequirementsFormat()
{
    // This essentially just omits the markdown indent
    setParentalBannerState(false);
    QStringList format;
    QString line = "\n**REQUIREMENT "
            + getReferenceAsString(false) + ": " + data(0).toString().trimmed()
            + "**" + "\n\n";
    format.push_back(line);
    return format;
}

// Leave this as static for now, should probably be changed to a member call on root,
// however, that is exactly the info sent as a parameter
QStringList Requirement::publishRequirements(const ReferenceMap *map)
{
    QStringList results;
    OptionsMap options;
    Reference typeKey = Reference(classType(), 0, 0);

    auto lowerIt = map->lowerBound(typeKey);
    for (auto it = lowerIt; it != map->end(); ++it) {
        if (Reference::typeComp(typeKey, it.key())) {
            break;
        }
        // The items found are Requirement*, even though they are TreeItem*
        auto *requirement = dynamic_cast<Requirement*>(it.value());
        if (requirement) {
            results << requirement->specialRequirementsFormat();
        }
    }
    return results;
}

// ----------------------------------------------------------------------------
