/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#ifndef TREEITEMTYPES_H
#define TREEITEMTYPES_H

#include "treeitem.h"
#include "referenceitem.h"
#include "crossreference.h"

#include <QColor>

#include <QMultiMap>

/* treeitemtypes.h
 * -------------------------------------------------------------------------------
 * The types defined here are used in the data tree: they all inherit TreeItem.
 *
 * The existence of these derived types allows for control over the properties of
 * these key types of cell - that is to say, which types that they insert, whether
 * they are linkable, referencing, and if so, what types they can target.
 *
 * Information required by the Reference / CrossReference constructors can be
 * defined here and passed upward via the base class initializers.
 *
 * (Ultimately, it would be possible to encapsulate most of the information here
 * in some kind of map to allow for a truly generic class. Then, the number of
 * classes defined here could be dramatically reduced.)
 *
 */

// Forward declarations to keep the class list in the right order
class Loss;
class Hazard;
class UCA;
class ControlAction;
class TrueStatement;
class Belief;
class Cause;
class InformationReceived;
class HowThisCouldOccur;
class Requirement;

// -------------------------------------------------------------------------------
// The three root types are all completely identical apart from their insert type

// -------------------------------------------------------------------------------
// This class is used only at tree construction, with T = Loss, Hazard, or Control
template <typename T>
class RootItem : public TreeItem
{
public:
    RootItem(const QVector<QVariant> &data, TreeItem *parent, int uniqueID)
        : TreeItem(data, parent, uniqueID) {}

    bool deleteable() override {return false;}
    bool isEditable() override {return false;}

    QStringList insertsTypes() override {return QStringList{T::classType()};}
    QString defaultChildType() override {return QString(T::classType());}

    // inherits non-referencing, non-linkable, non-referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return ("Root item");}

    bool isLinkable() override {return classIsLinkable();}
    static bool classIsLinkable() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}

    // The factory normally sits in ReferenceItem<T> but root classes
    // do not use references
    static TreeItem *manufacture(const QVector<QVariant> &data,
                          TreeItem *parent,
                          int uniqueID);
    static QString classParentOf()
    {
        QString type = T::classType();
        return type;
    }
    QString parentOf() override
    {
        return classParentOf();
    }

private:
    static inline QString markdownPrefix
                    = QString("## ");
};

template<typename T>
TreeItem *RootItem<T>::manufacture(const QVector<QVariant> &data,
                                        TreeItem *parent,
                                        int uniqueID)
{
    return new RootItem<T>(data, parent, uniqueID);
}

// -------------------------------------------------------------------------------
class Loss : public CrossReference
{
public:
    Loss(const QVector<QVariant> &data, TreeItem *parent,
         int uniqueID, int reference = 0);

    // Losses are deletable, editable, non-referencing, non-linkable, referable
    static QString classCellType() {return QString("Loss");}
    static QString classType() {return "L";}
    static bool classReferencesParent() {return false;}

    QString celltype() override {return classCellType();}

    QStringList insertsTypes() override {return QStringList();}
    QString defaultChildType() override {return QString();}

    bool isReferencingItem() override {return false;}
    QStringList permittedReferenceTargets() override {return QStringList();}
    QString defaultReferenceType() override {return QString();}

    static bool classIsLinkable() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat();

private:
    static inline QString markdownPrefix
                    = QString("  - ");
};

// -------------------------------------------------------------------------------
class Hazard : public CrossReference
{
public:
    Hazard(const QVector<QVariant> &data, TreeItem *parent,
           int uniqueID, int reference = 0);

    // Hazards are deletable, editable, referencing, non-linkable
    // referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Hazard");}
    static QString classType() {return "H";}
    static bool classReferencesParent() {return false;}

    QStringList insertsTypes() override {return QStringList();}
    QString defaultChildType() override {return QString();}

    QStringList permittedReferenceTargets() override {return QStringList{"L"};}
    QString defaultReferenceType() override {return QString("L");}

    static bool classIsLinkable() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat();

private:
    static inline QString markdownPrefix
                    = QString("  - ");
};

// -------------------------------------------------------------------------------
class Control : public ReferenceItem
{
public:
    Control(const QVector<QVariant> &data, TreeItem *parent,
            int uniqueID, int reference = 0);


    // Controls are deletable, editable, non-referencing, non-linkable
    // non-referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Control");}

    static QString classType() {return "C";}
    static bool classReferencesParent() {return false;}

    QStringList insertsTypes() override {return QStringList{"UCA", "CA"};}
    QString defaultChildType() override {return QString("UCA");}

    static bool classIsLinkable() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}

    bool getInsertsControlAction() const override {return true;}

private:

    static inline QString markdownPrefix
                    = QString("### ");
};

// -------------------------------------------------------------------------------
class UCA : public CrossReference
{
public:
    UCA(const QVector<QVariant> &data, TreeItem *parent,
        int uniqueID, int reference = 0);


    // UCA are deletable, editable, referencing, non-linkable, referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Unsafe Control Action");}

    static QString classType() {return QString("UCA");}
    static bool classReferencesParent() {return true;}

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("TRUE");}

    QStringList permittedReferenceTargets() override {return QStringList{"H"};}
    QString defaultReferenceType() override {return QString("H");}

    static bool classIsLinkable() {return false;}

    QString getMarkdownFormatString() const;
    QStringList specialMarkdownFormat() override;

    virtual bool getIsControlAction() const override {return true;}

private:
    static inline QString markdownPrefix
                    = QString("#### ");
};

// -------------------------------------------------------------------------------
class ControlAction : public CrossReference
{
public:
    ControlAction(const QVector<QVariant> &data, TreeItem *parent,
        int uniqueID, int reference = 0);

    // UCA are deletable, editable, referencing, non-linkable, referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Control Action");}

    static QString classType() {return QString("CA");}
    static bool classReferencesParent() {return true;}

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("TRUE");}

    QStringList permittedReferenceTargets() override {return QStringList{"H"};}
    QString defaultReferenceType() override {return QString("H");}

    static bool classIsLinkable() {return false;}

    QString getMarkdownFormatString() const override;
    virtual QStringList specialMarkdownFormat() override;

    virtual bool getIsControlAction() const override {return true;}

private:
    static inline QString markdownPrefix
                    = QString("#### ");
};

// -------------------------------------------------------------------------------
class TrueStatement : public CrossReference
{
public:
    TrueStatement(const QVector<QVariant> &data, TreeItem *parent,
                  int uniqueID, int reference = 0);


    // TrueStatement is deletable, editable, referencing, linkable, non-referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("True Statement");}
    static QString classType() {return QString("TRUE");}
    static bool classReferencesParent() {return false;}

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("BEL");}

    QStringList permittedReferenceTargets() override {return QStringList{"REQ"};}
    QString defaultReferenceType() override {return QString();}

    static bool classIsLinkable() {return true;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat() override;

private:
    static inline QString markdownPrefix
                    = QString("**");
};

// -------------------------------------------------------------------------------
class Belief : public CrossReference
{
public:
    Belief(const QVector<QVariant> &data, TreeItem *parent,
           int uniqueID, int reference = 0);


    // Belief is deletable, editable, non-referencing, linkable, non-referable
    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Belief");}
    static QString classType() {return QString("BEL");}
    static bool classReferencesParent() {return false;}

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("CS");}

    QStringList permittedReferenceTargets() override {return QStringList{"TRUE", "FB", "HOW"};}
    QString defaultReferenceType() override {return QString();}

    static bool classIsLinkable() {return true;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat() override;

private:
    static inline QString markdownPrefix = QString("*");
};

// -------------------------------------------------------------------------------
class Cause : public CrossReference
{
public:
    // Potential special case, there are only about 7 or 8 unique causes
    Cause(const QVector<QVariant> &data, TreeItem *parent,
          int uniqueID, int reference = 0);


    // Hazards is deletable, editable, non-referencing, linkable, non-referable

    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Cause");}
    static QString classType() {return QString("CS");}
    static bool classReferencesParent() {return false;}

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("FB");}
    QStringList permittedReferenceTargets() override {return QStringList{"TRUE", "FB", "HOW"};}
    QString defaultReferenceType() override {return QString();}

    static bool classIsLinkable() {return true;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat() override;
    bool isBullettedFormat() override {return true;}

private:
    static inline QString markdownPrefix = QString(" - Cause: ");
};

// -------------------------------------------------------------------------------
class InformationReceived : public CrossReference
{
public:
    // Also known as feedback
    InformationReceived(const QVector<QVariant> &data, TreeItem *parent,
                        int uniqueID, int reference = 0);

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("HOW");}

    QStringList permittedReferenceTargets() override {return QStringList{"TRUE", "BEL", "CS",
                                                                         "HOW"};}
    QString defaultReferenceType() override {return QString();}

    // deletable, editable, non-referencing, linkable, non-referable
    static bool classIsLinkable() {return true;}

    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Information / Feedback Received");}
    static QString classType() {return QString("FB");}
    static bool classReferencesParent() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat() override;
    bool isBullettedFormat() override {return true;}

private:
    static inline QString markdownPrefix = QString(" - Detail: ");
};

// -------------------------------------------------------------------------------
class HowThisCouldOccur : public CrossReference
{
public:
    HowThisCouldOccur(const QVector<QVariant> &data, TreeItem *parent,
                      int uniqueID, int reference = 0);

    QStringList insertsTypes() override {return QStringList{"TRUE", "BEL", "CS",
                                                            "FB", "HOW", "REQ"};}
    QString defaultChildType() override {return QString("REQ");}
    QStringList permittedReferenceTargets() override {return QStringList{"TRUE", "BEL", "CS",
                                                                         "FB"};}
    QString defaultReferenceType() override {return QString();}

    // Hazards is deletable, editable, referencing, linkable, non-referable
    static bool classIsLinkable() {return true;}

    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("How This Could Occur");}
    static QString classType() {return QString("HOW");}
    static bool classReferencesParent() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat() override;
    bool isBullettedFormat() override {return true;}

private:
    static inline QString markdownPrefix = QString(" - How: ");

};

// -------------------------------------------------------------------------------
class Requirement : public CrossReference
{
public:
    Requirement(const QVector<QVariant> &data, TreeItem *parent,
                             int uniqueID, int reference = 0);

    QStringList insertsTypes() override {return QStringList();}
    QString defaultChildType() override {return QString();}
    QStringList permittedReferenceTargets() override {return QStringList{"UCA", "TRUE", "BEL", "CS",
                                                                         "FB", "HOW"};}
    QString defaultReferenceType() override {return QString();}

    // Hazards are deletable, editable, non-referencing, linkable, non-referable
    static bool classIsLinkable() {return true;}

    QString celltype() override {return classCellType();}
    static QString classCellType() {return QString("Requirement");}
    static QString classType() {return QString("REQ");}
    static bool classReferencesParent() {return false;}

    QString getMarkdownFormatString() const override {return markdownPrefix;}
    QStringList specialMarkdownFormat() override;
    bool isBullettedFormat() override {return true;}
    bool isRequirement() override {return true;}

    QStringList specialRequirementsFormat();
    static QStringList publishRequirements(const ReferenceMap *map);
private:
    static inline QString markdownPrefix = QString("  - ");

};

#endif // TREEITEMTYPES_H
