/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include "treemodel.h"

#include <QtWidgets>
#include <QDebug>
#include <string>
#include <treeitemtypes.h>

// It is not desirable to include the derived class header in the base class -
// this namespace wrapper allows access to the one function required
namespace TreeModelYaml {
void writeYamlSaveFile(TreeItem *root, const QString &filename,
                       QWidget *widgetParent);
}

TreeModel::TreeModel(const QStringList &headers, QObject *parent)
    : QAbstractItemModel(parent)
{
    QVector<QVariant> rootData;
    foreach (QString header, headers)
        rootData << header;
    factory = Factory::factory();
    root = new TreeItem(rootData, nullptr);
}

TreeModel::~TreeModel()
{
    delete root;
    // There is no reason to decommission the Factory - other models may exist
}

int TreeModel::columnCount(const QModelIndex & /* parent */) const
{
    return root->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    TreeItem *item = getItem(index);

    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return nullptr;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

TreeItem *TreeModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        auto *item = static_cast<TreeItem*>(index.internalPointer());
        if (item)
            return item;
    }
    return root;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return root->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    TreeItem *parentItem = getItem(parent);
    TreeItem *childItem = parentItem->child(row);
    return childItem ? createIndex(row, column, childItem) : QModelIndex();
}

bool TreeModel::insertColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;

    beginInsertColumns(parent, position, position + columns - 1);
    success = root->insertColumns(position, columns);
    endInsertColumns();

    return success;
}

bool TreeModel::insertRowByType(int arow, const QModelIndex &aparent, QString type)
{
    return insertRowsByType(arow, 1, aparent, type);
}

bool TreeModel::insertRowsByType(int position, int rows, const QModelIndex &parent, QString type)
{
    TreeItem *parentItem = getItem(parent);
    bool success;

    // While using the editor, reference and ID values are generated automatically
    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows, root->columnCount(), type);
    endInsertRows();
    return success;
}

// This is the original override
bool TreeModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    bool success;

    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows, root->columnCount());
    endInsertRows();
    return success;
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = getItem(index);
    TreeItem *parentItem = childItem->parent();

    if (parentItem == root)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

bool TreeModel::removeColumns(int position, int columns, const QModelIndex &parent)
{
    bool success;

    beginRemoveColumns(parent, position, position + columns - 1);
    success = root->removeColumns(position, columns);
    endRemoveColumns();

    if (root->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    TreeItem *parentItem = getItem(parent);
    bool success = true;

    // The function needs to operate on row indices (parent), but also underlying
    //  data in the TreeItems - it now checks the item for deletability
    if (getItem(index(position, 0, parent))->deleteable()) {
        beginRemoveRows(parent, position, position + rows - 1);
        success = parentItem->removeChildren(position, rows);
        endRemoveRows();
    } else {
        success = false;
    }
    return success;
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem = getItem(parent);

    return parentItem->childCount();
}
/*
 * The presence of non-writable columns, non-writable rows and special columns
 * must all be checked when the view tries to pass data to the model.
*/
bool TreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole)
        return false;

    TreeItem *item = getItem(index);
    if (!index.isValid()) {
        return false;
    }

    // Not all items may be edited - impose a check here
    bool result = false;
    if (item->isEditable()) {
        // The data absolutely must not contains tab characters
        QString modifiedData = value.toString();
        QVariant newValue(modifiedData.remove('\t'));

        switch (index.column()) {
        case 0:
        case 3:
            // Data and comment columns may be freely edited
            result = item->setData(index.column(), newValue);
            if (result)
                emit dataChanged(index, index);
            break;
        case 1:
            // Allow reference Numbers to be edited for permitted classes
            if (!item->isReferenceType()) {
                break;
            }
            int reference = parseReferences(newValue.toString(),
                                           QStringList{item->getReferenceType()}).number;
            if (reference != 0) {
                // Retrieve the row and column
                // This will renumber all references with the same number
                result = item->renumberReference(reference);
                if (result) {
                    // This will update all the corresponding cells
                    QVariant valueToSet(item->getReferenceAsString());
                    result = item->setData(index.column(), valueToSet);
                }
                layoutChanged();
                // The current index points still points at the item renumbered
                // but the index row is out of date
                QModelIndex newIndex = createIndex(item->childNumber(), 1, item);
                emit updateIndex(newIndex);
            }
            break;
        // There is no case 2: because Refers to cannot be edited manually
        }
    }
    return result;
}

bool TreeModel::setHeaderData(int section, Qt::Orientation orientation,
                              const QVariant &value, int role)
{
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    bool result = root->setData(section, value);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}
// -----------------------------------------------------------------------------
/*
 This function converts something like [UCA-6.2] into Reference{"UCA", 6, 2}
 Non parent-specific references like [L-1] converts to Reference{"L", 0, 1}
 L-1 is converted, likewise, without the brackets.

 An integer is converted into a type-less reference 4 --> Reference{"", 0, 4}

 This function is used both in data entry and in file loading
*/
Reference TreeModel::parseReferences(QString value,
                                     QStringList allowedTypes)
{
    // This will attempt to populate all the fields that it can - if just
    // a number is entered then the parent and type fields will be blank

    Reference reference;
    bool ok = false;

    for (const auto &type : allowedTypes) {
        // First try a straightforward convert to int
        reference.number = value.toInt(&ok);
        if (!ok) {
            ok = true;
            // else strip reference type and try again
            if (value.contains(type)) {
                reference.type = type;
                int removeIndex = 0;
                value.remove(type);
                value.remove("[");
                value.remove("]");
                value = value.trimmed();
                // If a dot is present, then seek a parent number
                if (value.contains('.')) {
                    removeIndex = value.indexOf('.');
                    QString parentString = value.left(removeIndex);
                    parentString.remove("-");
                    reference.parentNumber = parentString.toInt(&ok);
                }
                // If a parent was found, or was not sought, ok will still be true
                if (ok) {
                    removeIndex = value.indexOf('-');
                    value.remove(0, removeIndex + 1);
                    reference.number = value.toInt(&ok);
                }
                break;
            }
        }
    }

    if (!ok) {
        reference = Reference();
    }
    return reference;
}

// -----------------------------------------------------------------------------
// Miscellaneous - helper functions for the views highlighting ability

// This is used by MyTreeView
int TreeModel::getIndentationDepth(const QModelIndex &index) const
{
    int depth = 0;
    QModelIndex depthIndex = index;
    while (depthIndex.parent().isValid()) {
        ++depth;
        depthIndex = depthIndex.parent();
    }
    return depth;
}

void TreeModel::setHighlightForType(const QString &type)
{
    root->treeHighlightStates(type, true);
}

void TreeModel::clearHighlightForType(const QString &type)
{
    root->treeHighlightStates(type, false);
}

// -----------------------------------------------------------------------------
// Functions used for saving

// Legacy save
QStringList TreeModel::convertModelDataToStrings()
{
    // We shall recurse through the tree from the root, which the function will skip
    return root->convertTreeToStringList();
}

// Yaml Save
void TreeModel::createYamlSaveFile(const QString &filename, QWidget *widgetParent)
{
    // The task itself can be devolved to a derived class or factory, but
    // all TreeModels must be able to perform a yaml save
    TreeModelYaml::writeYamlSaveFile(root, filename, widgetParent);
}

// -----------------------------------------------------------------------------
// Pass through functions: the model provides an interface to the underlying data

QString TreeModel::getItemReferenceAsOption(const QModelIndex &index)
{
    return getItem(index)->getReferenceAsOption();
}

QMap<QString, TreeItem *> TreeModel::referenceOptionsInThisCategory(const QModelIndex &index)
{
    return getItem(index)->getPeerReferences(getItemType(index));
}

QMap<QString, TreeItem *> TreeModel::referenceTargetOptions(const QModelIndex &index)
{
    return getItem(index)->getReferenceTargetOptions();
}

QMap<QString, TreeItem *> TreeModel::currentReferenceTargets(const QModelIndex &index)
{
    return getItem(index)->getReferencesMade(true);
}

void TreeModel::makeReference(const QModelIndex &index, TreeItem *referenceTarget)
{
    getItem(index)->makeReference(referenceTarget);
    emit dataChanged(index, index);
}

void TreeModel::removeReference(const QModelIndex &index, TreeItem *referenceTarget)
{
    getItem(index)->deleteReference(referenceTarget);
    emit dataChanged(index, index);
}

void TreeModel::createLink(const QModelIndex &index, TreeItem *linkTarget)
{
    getItem(index)->createLink(linkTarget);
}

bool TreeModel::severLinkOnRow(const QModelIndex &index) const
{
    return getItem(index)->severLink();
}

QString TreeModel::itemType(const QModelIndex &index) const
{
    return getItem(index)->celltype();
}

bool TreeModel::isReferencingItemAt(const QModelIndex &index) const
{
    return getItem(index)->isReferencingItem();
}

bool TreeModel::isLinkableItem(const QModelIndex &index) const
{
    return getItem(index)->isLinkable();
}

void TreeModel::sortChildrenForRow(const QModelIndex &index) const
{
    return getItem(index)->sortChildrenByReferenceNumbers();
}

bool TreeModel::isParentCapable(const QModelIndex &index) const
{
    return !getItem(index)->insertsTypes().isEmpty();
}

QString TreeModel::defaultChildType(const QModelIndex &index) const
{
    return getItem(index)->defaultChildType();
}

bool TreeModel::isEditable(const QModelIndex &index) const
{
    return getItem(index)->isEditable();
}

bool TreeModel::isDeletable(const QModelIndex &index) const
{
    return getItem(index)->deleteable();
}

int TreeModel::getUnique(const QModelIndex &index) const
{
    return getItem(index)->getUniqueID();
}

QStringList TreeModel::createMarkdownStringsFromModel()
{
    return root->convertTreeToMarkdownList();
}

QStringList TreeModel::createMarkdownRequirementsFromModel()
{
    return Requirement::publishRequirements(root->getReferenceMap());
}

bool TreeModel::insertsChildControlAction(const QModelIndex &index) const
{
    return getItem(index)->getInsertsControlAction();
}

bool TreeModel::isControlAction(const QModelIndex &index) const
{
    return getItem(index)->getIsControlAction();
}

void TreeModel::renumumberAllReferencesThisCategory(const QModelIndex &index)
{
    return getItem(index)->renumberAllReferences();
}

QString TreeModel::getItemType(const QModelIndex &index)
{
    return getItem(index)->getReferenceType();
}

QStringList TreeModel::getPermittedInsertTypes(const QModelIndex &index)
{
    return getItem(index)->insertsTypes();
}

QString TreeModel::getDefaultInsertType(const QModelIndex &index)
{
    return getItem(index)->defaultChildType();
}
