/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2016 The Qt Company Ltd.
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

/*
 * treemodel.h
 * ********************************************************************************************
 * This unit provides the model for the view. For the model to work with one of the Qt
 * Framework view classes, it has to be of the right type - in this case QAbstractItemModel.
 *
 * The view expects certain overrides to be implemented so that the view can call these
 * overrides to insert, update, or retrieve data from the model. Where and how the model stores
 * the data is down to the model, in this case it owns an underlying tree data structure built
 * of type TreeItem, but the view does not need to know that. Thus, the model is a structure
 * agnostic interface between the view and the 'datastore'.
 *
 * The actual layout of the model uses QModelIndex, and is a table of tables. Each item is a
 * 'table' where its children represent rows of data, and the child's nth data() element is
 * the nth column of that row. But, the child may also have its own children - in which case
 * the child is a table, with rows and columns, and so forth.
 *
 * -- References and Links --
 * Making references, linking, and retrieving options for these functions are non-standard
 * from a view point of view. These are triggered from context / drop-down, where this
 * model class also provides the data manipulation entry point for the controller.
 *
 * The controller will call a check function (here) on the currently selected index to see whether
 * a given function is available for the object. (The controler is able to determine the
 * currently selected item because it receives a signal / slot callback from the view)
 *
 * The controller callback will then enable / disable the QActions, etc. If it an enabled
 * function is selected, then the appropriate options are retrieved (from here) as
 * an OptionsMap, and the controller will display them in the List Widget.
 *
 * Depending on the currently active options, a double click or selection made in that window
 * will call a function here.
*/

#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QItemSelectionModel>

#include "factory.h"

class TreeItem;

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    TreeModel(const QStringList &headers,
              QObject *parent = nullptr);
    ~TreeModel() override;

    typedef QMap<QString, TreeItem *> OptionsMap;

    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role = Qt::EditRole) override;

    bool insertColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;

    // Customised functions to allow the controller to make arbitrary inserts
    bool insertRowByType(int arow, const QModelIndex &aparent,
                         QString type = QString());
    bool insertRowsByType(int position, int rows, const QModelIndex &parent, QString type);
    void sortChildrenForRow(const QModelIndex &index) const;

    // Type and type checks - used by UI to enable options and display info
    bool isReferencingItemAt(const QModelIndex &index) const;
    bool isLinkableItem(const QModelIndex &index) const;
    QString itemType(const QModelIndex &index) const;
    bool isParentCapable(const QModelIndex &index) const;
    bool isDeletable(const QModelIndex &index) const;
    bool isControlAction(const QModelIndex &index) const;
    bool isEditable(const QModelIndex &index) const;
    QString defaultChildType(const QModelIndex &index) const;
    bool insertsChildControlAction(const QModelIndex &index) const;
    QString getItemType(const QModelIndex &index);

    // Used to show link / reference options
    OptionsMap referenceOptionsInThisCategory(const QModelIndex &index);
    OptionsMap referenceTargetOptions(const QModelIndex &index);
    OptionsMap currentReferenceTargets(const QModelIndex &index);
    QString getItemReferenceAsOption(const QModelIndex &index);

    // Used to link items - either dynamically or by reference
    void makeReference(const QModelIndex &index, TreeItem *referencingItem);
    void removeReference(const QModelIndex &index, TreeItem *referenceTarget);
    void createLink(const QModelIndex &index, TreeItem *linkTarget);
    bool severLinkOnRow(const QModelIndex &index) const;

    // Helper functions for the view
    int getIndentationDepth(const QModelIndex &index) const;
    int getUnique(const QModelIndex &index) const;

    // View Highlighting
    void setHighlightForType(const QString &type);
    void clearHighlightForType(const QString &type);
    int getHighlightForDepth() const;

    // Category based renumber function
    void renumumberAllReferencesThisCategory(const QModelIndex &index);

    // Save
    void createYamlSaveFile(const QString &filename, QWidget *widgetParent);
    QStringList convertModelDataToStrings();

    // Output
    QStringList createMarkdownStringsFromModel();
    QStringList createMarkdownRequirementsFromModel();

    QStringList getPermittedInsertTypes(const QModelIndex &index);
    QString getDefaultInsertType(const QModelIndex &index);
protected:
    TreeItem *getItem(const QModelIndex &index) const;

    TreeItem *insertItemTypeByHierarchy(TreeItem *parent, const QString& data, QString type,
                                        int reference, int uniqueID);
    TreeItem *deriveParentFromIdentationLevel(int position,
                                         QList<int> &indentations,
                                              QList<TreeItem*>& parents);
    Reference parseReferences(QString value, QStringList allowedTypes);

    TreeItem *root = nullptr;
    int highlightLevel = 0;
    friend class ModelTest;
    Factory *factory = nullptr;

private:
    // Used by the model to re-contstruct reference linkages


signals:
    void updateIndex(const QModelIndex& newIndex);
};


#endif // TREEMODEL_H
