/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#include "treemodeltypes.h"
#include "treeitemtypes.h"
#include <QString>
#include <QStringList>

#include <QFile>
#include <QMessageBox>

#include <map>
#include <vector>

#include <QDebug>

// ---------------------------------------------------------------------------------
// Overload to simpligy creation of data from the tree structure
YAML::Emitter& operator << (YAML::Emitter& out, const TreeItem& item);

TreeModelYaml::TreeModelYaml(const QStringList &headers,
                             const QString &filename,
                             QWidget *parent)
        : TreeModel(headers, parent),
          parentWidget(parent)
{
    setupModelData(filename);
}

// Inbound data is set up by the constructor

bool TreeModelYaml::setupModelData(const QString &filename)
{
    qDebug() << "Filename for yaml conversion is: " << filename.toLocal8Bit().data();

    // This function will use std::string objects etc, so will make conversions to these types
    YAML::Node BaseNode = YAML::LoadFile(filename.toStdString());
    if (BaseNode.IsNull()) {
        return false;
    }
    auto version = BaseNode["Metadata"]["Version"].as<std::string>();

    // Iterate over the RootItems - all the children will be inserted recursively
    referenceMap.clear();
    auto rootItems = BaseNode["RootItems"];
    for (YAML::const_iterator it = rootItems.begin();
         it != rootItems.end(); ++it) {
        processTreeNode(it->second);
    }

    // Reconstruct the reference links in the tree
    for (auto treeItem : referenceMap.uniqueKeys()) {
        for (const auto &target : referenceMap.values(treeItem)) {
            Reference targetReference = parseReferences(target, treeItem->permittedReferenceTargets());
            if (targetReference != Reference()) {
                treeItem->makeReference(targetReference);
            }
        }
    }
    return true;
}

// -----------------------------------------------------------------------------

// For children we will use a recursion technique

void TreeModelYaml::processTreeNode(const YAML::Node &treeNode, TreeItem *parent)
{
    TreeItem* newItem = nullptr;
    QVector<QVariant> dataVector(root->columnCount());

    if (treeNode.IsNull()) {
        // throw invalid data
    } else if (!treeNode["Data"].IsMap()) {
        // This will throw if Data is absent
        // Throw if "Data" is not a map
    } else {
        YAML::Node data = treeNode["Data"];
        int uniqueID = 0;
        QString coreData;

        // Text is compulsory! This will throw if data is not present and scalar
        if (data["Text"].IsScalar()) {
            coreData = QString::fromStdString(data["Text"].as<std::string>());
        }
        if (data["ItemID"].IsScalar()) {
            uniqueID = data["ItemID"].as<int>();
        }
        if (!parent) {
            parent = root;
        }

        // RootItems else treeItem
        if (parent == root) {
            QString parentOf = QString::fromStdString(data["RootOf"].as<std::string>());
            dataVector = { coreData };
            newItem = factory->generateRoot(parentOf, dataVector, root, uniqueID);
        } else {
            int reference = 0;
            QString referencesMade;
            QString comment;

            // type data is non-negotiable and will not be substituted if the value is missing
            QString type = QString::fromStdString(data["Type"].as<std::string>());

            if (data["ReferenceNumber"] && data["ReferenceNumber"].IsScalar()) {
                reference = data["ReferenceNumber"].as<int>();
            }
            if (data["Comment"] && data["Comment"].IsScalar()) {
                comment = QString::fromStdString(data["Comment"].as<std::string>());
            }
            dataVector = { coreData, QString(), referencesMade, comment};
            newItem = factory->create(type, dataVector, parent, uniqueID, reference);
        }

        // All types items are inserted in the same way - and all have regular tree type children
        if (newItem) {
            parent->insertSpecial(newItem);
            YAML::Node children = treeNode["Children"];
            if (children) {
                for (YAML::const_iterator it = children.begin();
                     it != children.end(); ++it) {
                    processTreeNode(it->second, newItem);
                }
            }
        }

        // Now that an object exists, we can store the references made by it
        if (data["ReferencesMade"] && data["ReferencesMade"].IsSequence()) {
            for (YAML::const_iterator it = data["ReferencesMade"].begin();
                 it != data["ReferencesMade"].end(); ++it) {
                referenceMap.insert(newItem, QString::fromStdString(it->as<std::string>()));
            }
        }
    }
}

// -----------------------------------------------------------------------------

void TreeModelYaml::writeYamlSaveFile(TreeItem *root, const QString &filename,
                                      QWidget * widgetParent)
{
    // This method uses the YAML emitter directly, rather than building nodes.

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(widgetParent, tr("Unable to open file"),
                                 file.errorString());
    } else {
        QTextStream fileStream(&file);
        YAML::Emitter out;
        // Create a map, the first key will be "Metadata"
        out << YAML::BeginMap;

        // The next map is the value for the key "Metadata"
        out << YAML::Key << "Metadata" << YAML::BeginMap;
        out << YAML::Key << "Version" << YAML::Value << PROGRAM_VERSION;
        out << YAML::Key << "yaml-cppVersion" << YAML::Value << YAML_VERSION;
        out << YAML::Key << "saveFileFormat"<< YAML::Value << SAVE_FILE_FORMAT;
        out << YAML::EndMap; // End Metadata

        // Now output the Rootitems - the value is a map
        out << YAML::Key << "RootItems" << YAML::BeginMap;
        for (int i = 0; i < root->childCount(); ++i) {
            TreeItem *rootItem = root->child(i);
            // For root items, use the same data for the node name as the actual data
            std::string category = rootItem->data(0).toString().toStdString();

            // Enters the key 'category' in RootItem's map, the value is also a map
            out << YAML::Key << category << YAML::BeginMap;

            // The Data and Children are entered at this node - first data...
            out << YAML::Key << "Data" << YAML::BeginMap;

            // Here are the Key / Value pairs that make up the map associated with Data
            out << YAML::Key << "ItemID" << YAML::Value << rootItem->getUniqueID();
            out << YAML::Key << "RootOf" << YAML::Value << rootItem->parentOf().toStdString();
            out << YAML::Key << "Text" << YAML::Value << category;
            out << YAML::EndMap;

            // Output the children (will recursively output all descendents)
            exportChildrenToYaml(out, rootItem);
            out << YAML::EndMap; // Closes out this rootItem
        }
        out << YAML::EndMap; // End Root Items

        // The full tree is now constructed - output the data to file
        fileStream << out.c_str();
        file.close();
    }
}

// -----------------------------------------------------------------------------

void TreeModelYaml::exportChildrenToYaml(YAML::Emitter &out, TreeItem *node)
{
    out << YAML::Key << "Children";
    if (node->childCount() > 0) {
        out << YAML::BeginMap; // Open map of CH nodes
        for (int i = 0; i < node->childCount(); ++i) {
            std::string childNodeName = "CH";
            childNodeName += std::to_string(i);
            out << YAML::Key << childNodeName << YAML::Value << YAML::BeginMap;

            // The the value is a map containing "Data" and "Children"
            if (node->child(i)) {
                out << *(node->child(i)); // Creates node "Data"
                exportChildrenToYaml(out, node->child(i)); // Creates node "Children"
            }
            out << YAML::EndMap; // close child node
        }
        out << YAML::EndMap; // Close map of CH nodes
    } else {
        out << YAML::Null;
    }
}

YAML::Emitter& operator << (YAML::Emitter& out, const TreeItem& item) {

    // This section outputs data as strings - when parsed back in the
    // relevant sections will be converted back to T = int etc
    out << YAML::Key << "Data" << YAML::BeginMap;

    std::string type = item.getReferenceStem().toStdString();
    type.pop_back();
    out << YAML::Key << "ItemID"
        << YAML::Value << std::to_string(item.getUniqueID());
    out << YAML::Key << "Type"
        << YAML::Value << type;
    out << YAML::Key << "Text"
        << YAML::Value << item.data(0).toString().toStdString();
    out << YAML::Key << "ReferenceNumber"
        << YAML::Value << std::to_string(item.getReferenceNumber());
    out << YAML::Key << "Comment"
        << YAML::Value << item.data(3).toString().toStdString();

    // The referenced items from getReferencesMade are in LEXICAL order
    std::vector<std::string> referencesMade;
    QMap<int, QString> numericallyOrderedReferences;
    for (const TreeItem* target : item.getReferencesMade()) {
        if (target) {
            numericallyOrderedReferences.insert(target->getReferenceNumber(),
                                                target->getReferenceAsString(false));
        }
    }
    for (const QString &reference : numericallyOrderedReferences) {
        referencesMade.push_back(reference.toStdString());
    }
    // Because referencesMade is a vector, YAML::BeginSeq is implied
    out << YAML::Key <<  "ReferencesMade"
        << YAML::Value << YAML::Flow << referencesMade;
    out << YAML::EndMap;

    return out;
}

// -----------------------------------------------------------------------------

// The Functions below here deal with the legacy save file format

// -----------------------------------------------------------------------------

TreeModelLegacy::TreeModelLegacy(const QStringList &headers,
                                 const QString &filename,
                                 QWidget *parent)
        : TreeModel(headers, parent),
          parentWidget(parent)
{
    setupModelData(filename);
}

bool TreeModelLegacy::setupModelData(const QString &filename)
{
    qDebug() << "Filename for legacy conversion is: " << filename.toLocal8Bit().data();

    // Open the file
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(parentWidget, tr("Unable to open file"),
                                 file.errorString());
    }
    QString filedata = file.readAll();
    QStringList lines = filedata.split(QString("\n"));

    // This is the legacy model setup based on identations alone
    QList<TreeItem*> parents;
    QList<int> indentations;

    // The root of the tree is a TreeItem and is placed at the top of the list
    parents << root;
    indentations << 0;
    int number = 0;

    // Process all lines into the model
    while (number < lines.count()) {
        // Reads in spaces to determine the identation level of the line
        // Owing to this, we must use spaces to imply / infer the hierarchy depth
        int position = 0;
        while (position < lines[number].length()) {
            if (lines[number].at(position) != ' ')
                break;
            ++position;
        }
        // Process current line
        QString lineData = lines[number].mid(position).trimmed();
        QStringList columnStrings;
        if (!lineData.isEmpty()) {
            columnStrings = lineData.split("\t", QString::KeepEmptyParts);

            // Derive hierarchy position from identations
            TreeItem *parent = deriveParentFromIdentationLevel(position,
                                                               indentations,
                                                               parents);
            // parse reference first - these default values will be used if
            // nothing is found
            Reference reference;
            if (columnStrings.size() > 1) {
                reference = parseReferences(columnStrings[1],
                                            parent->insertsTypes());
            }

            // Strip off comments and other fields not of legacy format
            int uniqueID = 0;
            while (columnStrings.size() > 3) {
                columnStrings.pop_back();
            }
            TreeItem *newChild = insertItemTypeByHierarchy(parent,
                                                           columnStrings[0],
                                                           reference.type,
                                                           reference.number,
                                                           uniqueID);

            // Convert the line data to QVariants and store in the tree structure
            if (newChild != nullptr) {
                for (int column = 0; column < columnStrings.count(); ++column) {
                    if (column == 1) {
                        continue;
                    }
                    newChild->setData(column, QVariant(columnStrings[column]));
                }
            }
        }
        ++number;
    }
    // Reference targets are unique items and may be looked up using the 'existing'
    // table of the class
    QList<TreeItem*> items;
    root->recurseTreeToReferencingList(items);
    for (auto item : items) {
        QString targets = item->data(2).toString();
        QStringList listOfTargets = targets.split(" ", QString::SkipEmptyParts);
        for (const auto &target : listOfTargets) {
            Reference targetReference = parseReferences(target, item->permittedReferenceTargets());
            if (targetReference != Reference()) {
                item->makeReference(targetReference);
            }
        }
    }
    return true;
}

// -----------------------------------------------------------------------------

// This functions tracks hierarchy level from the current and previous identation levels,
// and returns the correct parent to which the new item should be added
TreeItem* TreeModelLegacy::deriveParentFromIdentationLevel(int position,
                                                QList<int>& indentations,
                                                QList<TreeItem*>& parents ) {

    // Any amount of identation greater than the last is a new level of hierarchy
    if (position > indentations.last()) {
        // The last child of the current parent is now the new parent, unless the parent is root
        if (parents.last()->childCount() > 0) {
            parents << parents.last()->child(parents.last()->childCount()-1);
            indentations << position;
        }
    } else {
        // Unwinds to the correct hierarchy level, based on the number of leading spaces
        while (position < indentations.last() && parents.count() > 0) {
            parents.pop_back();
            indentations.pop_back();
        }
    }
    return parents.last();
}

// -----------------------------------------------------------------------------

// This function inserts a new blank tree Item of the correct type
TreeItem* TreeModelLegacy::insertItemTypeByHierarchy(TreeItem* parent,
                                               const QString& data,
                                               QString type,
                                               int reference,
                                               int uniqueID)
{
    bool insertOk = false;
    TreeItem* newItem = nullptr;

    // These three types cannot be automatically created by generic parent -
    //  they must be created here
    if (parent == root) {
        QVector<QVariant> dataVector(root->columnCount());
        if ( data == QString("Losses")) {
            newItem = new RootItem<Loss>(dataVector, root, uniqueID);
        } else if (data == QString("Hazards")) {
            newItem = new RootItem<Hazard>(dataVector, root, uniqueID);
        } else if (data == QString("Controls")){
            newItem = new RootItem<Control>(dataVector, root, uniqueID);
        }
        insertOk = root->insertSpecial(newItem);
        if (!insertOk) {
            delete newItem;
            newItem = nullptr;
        }
    } else {
        // Append a new item to the current parent's list of children, the insert child overrride
        // will ensure that the correct type of child is inserted
        insertOk = parent->insertChildren(parent->childCount(), 1, root->columnCount(),
                                          type, reference, uniqueID);
        if (insertOk) {
            newItem = parent->child(parent->childCount() - 1);
        }
    }
    return newItem;
}
