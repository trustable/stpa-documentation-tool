/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#ifndef TREEMODELTYPES_H
#define TREEMODELTYPES_H

#include "treemodel.h"
#include <QtWidgets>
#include "yaml-cpp/yaml.h"
#include "version.h"


// The purpose is to remove the burden of file parsing from tree model
// - inheritance has been used here, but a factory class would also be suitable

class TreeModelYaml: public TreeModel
{
    Q_OBJECT

public:
    TreeModelYaml(const QStringList &headers,
                  const QString &filename,
                  QWidget *parent = nullptr);
    void processTreeNode(const YAML::Node &treeNode, TreeItem *parent = nullptr);

    // Has to be static because any TreeModel can call it
    static void writeYamlSaveFile(TreeItem *root, const QString &filename,
                                  QWidget *widgetParent);
    void writeYamlSaveFile(TreeItem *root, QString filename, QWidget *widgetParent);
private:
    QMultiMap<TreeItem*, QString> referenceMap;
    bool setupModelData(const QString &filename);
    QWidget *parentWidget;

    static void exportChildrenToYaml(YAML::Emitter &out, TreeItem *node);
};



class TreeModelLegacy : public TreeModel
{
    Q_OBJECT

public:
    TreeModelLegacy(const QStringList &headers,
                    const QString &filename,
                    QWidget *parent = nullptr);
private:
    QWidget *parentWidget;
    bool setupModelData(const QString &filename);
    TreeItem *insertItemTypeByHierarchy(TreeItem *parent, const QString &data,
                                        QString type, int reference, int uniqueID);
    TreeItem *deriveParentFromIdentationLevel(int position, QList<int> &indentations,
                                              QList<TreeItem *> &parents);
};

#endif // TREEMODELTYPES_H
