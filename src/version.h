/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software
 *
*/

#ifndef VERSION_H
#define VERSION_H

#define PROGRAM_VERSION "1.0"
#define YAML_VERSION "YAML_H_62B23520_7C8E_11DE_8A39_0800200C9A66"
#define SAVE_FILE_FORMAT "Tree Formatted YAML 1.0"


#endif // VERSION_H
