/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include <QTest>
#include "treeitem.h"
#include <memory>
#include "referenceitem.h"
#include "crossreference.h"

// --- Principal of cross referencing ---

// The 'associations' map of the target stores an entry for each reference:
//  a) Each 'target' key may have more than one 'referrer' value
//  b) Multiple 'target' keys may hold the same 'referrer' value.
//  c) If a 'target' has no referrers, it does not appear in the map at all
//  d) Each key, value pair is unique



class TestXrefMaker;
class TestXrefTarget;

// -----------------------------------------------------------------------------
// Test reference maker
// -----------------------------------------------------------------------------

class TestXrefMaker : public CrossReference
{
public:
    TestXrefMaker(const QVector<QVariant> &data, TreeItem *parent,
                    int uniqueID = 0, int reference = 0);
    QString celltype() override;
    static QString classReferenceStem() {return "MAKER-";}
    static QString classType() {return "MAKER";}
    static bool classReferencesParent() {return false;}
    static bool classIsLinkable() {return false;}

    QStringList permittedReferenceTargets() override {return QStringList{"TEST"};}
    QString defaultReferenceType() override {return QString("TEST");}

private:

};

TestXrefMaker::TestXrefMaker(const QVector<QVariant> &data, TreeItem *parent,
                               int uniqueID, int reference)
                : TreeItem(data, parent, uniqueID),
                  CrossReference (data, classType(), parent, reference, classIsLinkable(), classReferencesParent())
{
    Q_UNUSED(reference);
}

QString TestXrefMaker::celltype() {return QString("Dummy");}

// -----------------------------------------------------------------------------
// Test Reference Target
// -----------------------------------------------------------------------------

class TestXrefTarget : public CrossReference
{
public:
    TestXrefTarget(const QVector<QVariant> &data, TreeItem *parent,
                    int uniqueID = 0, int reference = 0);

    static QString classReferenceStem() {return "TEST-";}
    static QString classType() {return "TEST";}
    static bool classReferencesParent() {return false;}

    // TrueStatement is deletable, editable, referencing, linkable, non-referable
    QString celltype() override {return QString("TestClassTarget");}

    // required by reference item
    bool isLinkable() override;
    static bool classIsLinkable() {return false;}

private:

};

TestXrefTarget::TestXrefTarget(const QVector<QVariant> &data, TreeItem *parent,
                                 int uniqueID, int reference)
                : TreeItem(data, parent, uniqueID),
                  CrossReference (data, classType(), parent, reference)
{
    // Nothing to do
}

bool TestXrefTarget::isLinkable()
{
    return classIsLinkable();
}

// -----------------------------------------------------------------------------
// UNIT TESTS
// -----------------------------------------------------------------------------

class CrossReferenceTests : public QObject
{
    Q_OBJECT

private:
    QVector<QVariant> data {"dummy", 0, 0, 0};
    using ReferenceMap = QMultiMap<Reference, TreeItem*>;
    using CrossRefMap = QMultiMap<TreeItem*, TreeItem*>;

    TreeItem *root = nullptr;

    TestXrefTarget *test1 = nullptr;
    TestXrefTarget *test2 = nullptr;
    TestXrefTarget *test3 = nullptr;
    TestXrefMaker *maker1 = nullptr;
    TestXrefMaker *maker2 = nullptr;
    TestXrefMaker *maker3 = nullptr;

    QVector<QVariant> data1;
    QVector<QVariant> data2;
    QVector<QVariant> data3;

    CrossRefMap *testMap = nullptr;

    void init();
    // Shorthand: static (invariant in CrossReferenceTests)
    void setUpFiveReferrers();

private slots:
    void test1registerReference();
    void test2deregistrations();
    void test3deregisterAllReferencesToTarget();
    void test4deregisterAllReferencesFromSource();
    void test5getReferrers();
    void test6getAllReferencesFromSource();
    void test7XrefMakerMakeAndDeleteReference();
    void test8getReferencesMade();
    void test9getReferenceTargets();
};

void CrossReferenceTests::init()
{
    // The items will not be placed in a tree - cross reference amongst objects
    // is hierarchy independent
    //  - however, they will be parented on root, so that they can all use
    // the common maps (otherwise they cannot reference each other)

    delete test1;
    delete test2;
    delete test3;
    delete maker1;
    delete maker2;
    delete maker3;
    delete root;

    root = new TreeItem(data, nullptr);

    test1 = new TestXrefTarget(data, root);
    test2 = new TestXrefTarget(data, root);
    test3 = new TestXrefTarget(data, root);

    test1->setData(0, QString("Test Data1"));
    test2->setData(0, QString("Test Data2"));

    // Also create some x reference makers
    maker1 = new TestXrefMaker(data, root);
    maker2 = new TestXrefMaker(data, root);
    maker3 = new TestXrefMaker(data, root);
    testMap = root->getXRefMap();
}

void CrossReferenceTests::test1registerReference()
{
    init();

    // check the map is empty
    QCOMPARE(testMap->count(), 0);

    // Put them in the map
    test1->registerReference(test1, maker1);
    test1->registerReference(test1, maker2);
        // Check the items arrived
    QCOMPARE(testMap->count(), 2);
    QCOMPARE(testMap->contains(test1, maker1), true);
    QCOMPARE(testMap->contains(test1, maker2), true);

    // clean up the target and check that the map is empty again
    delete test1;
    QCOMPARE(testMap->count(), 0);

    // Check multiple registration is caught
    test1 = new TestXrefTarget(data, root);
    test1->registerReference(test1, maker1);
    test1->registerReference(test1, maker1);
    QCOMPARE(testMap->contains(test1, maker1), true);
    QCOMPARE(testMap->count(), 1);

    // Check registration on multiple targets is allowed
    test1->registerReference(test2, maker1);
    QCOMPARE(testMap->contains(test1, maker1), true);
    QCOMPARE(testMap->contains(test2, maker1), true);
    QCOMPARE(testMap->count(), 2);
}

void CrossReferenceTests::test2deregistrations()
{
    // Register a reference maker in a target then deregister it
    init();
    test1->registerReference(test1, maker1);
    QCOMPARE(testMap->contains(test1, maker1), true);
    test1->deregisterReference(test1, maker1);
    QCOMPARE(testMap->contains(test1, maker1), false);

    // Register a second item this time, perform multiple deregistrations
    // and verify that the other item is unaffected
    test1->registerReference(test1, maker1);
    test1->registerReference(test1, maker2);
    QCOMPARE(testMap->contains(test1, maker1), true);
    QCOMPARE(testMap->contains(test1, maker2), true);

    test1->deregisterReference(test1, maker1);
    test1->deregisterReference(test1, maker1);
    QCOMPARE(testMap->contains(test1, maker1), false);
    QCOMPARE(testMap->contains(test1, maker2), true);
}

void CrossReferenceTests::test3deregisterAllReferencesToTarget()
{
    init();
    QCOMPARE(testMap->isEmpty(), true);

    // Three items on target1, two items on target 2
    setUpFiveReferrers();

    // Remove all registrations on just one target
    test1->deregisterAllReferencesToTarget();

    // Check the results are as expected
    QCOMPARE(testMap->contains(test1, maker1), false);
    QCOMPARE(testMap->contains(test1, maker2), false);
    QCOMPARE(testMap->contains(test1, maker3), false);
    QCOMPARE(testMap->contains(test2, maker1), true);
    QCOMPARE(testMap->contains(test2, maker2), true);
    QCOMPARE(testMap->count(), 2);
}

void CrossReferenceTests::test4deregisterAllReferencesFromSource()
{
    init();
    QCOMPARE(testMap->isEmpty(), true);
    setUpFiveReferrers();

    maker1->deregisterAllReferencesFromSource(maker1);

    // Check the results are as expected
    QCOMPARE(testMap->contains(test1, maker1), false);
    QCOMPARE(testMap->contains(test1, maker2), true);
    QCOMPARE(testMap->contains(test1, maker3), true);
    QCOMPARE(testMap->contains(test2, maker1), false);
    QCOMPARE(testMap->contains(test2, maker2), true);
    QCOMPARE(testMap->count(), 3);
}

void CrossReferenceTests::test5getReferrers()
{
    init();
    QCOMPARE(testMap->isEmpty(), true);
    setUpFiveReferrers();
    auto results = test1->getReferrers();

    // test1 has three objects referring to it, check they are all there
    QCOMPARE(results.count(), 3);
    QCOMPARE(results.values().contains(maker1), true);
    QCOMPARE(results.values().contains(maker2), true);
    QCOMPARE(results.values().contains(maker3), true);

    auto results2 = test2->getReferrers();
    QCOMPARE(results2.count(), 2);
    QCOMPARE(results2.values().contains(maker1), true);
    QCOMPARE(results2.values().contains(maker2), true);

    // Check the references strings are populated correctly
    QCOMPARE(results2.contains(QString("[MAKER-1]")), true);
    QCOMPARE(results2.contains(QString("[MAKER-2]")), true);
    QCOMPARE(results2.value(QString("[MAKER-1]")), maker1);
}

void CrossReferenceTests::test6getAllReferencesFromSource()
{
    init();
    QCOMPARE(testMap->isEmpty(), true);
    setUpFiveReferrers();
    auto results = maker1->getAllReferencesFromSource(maker1);

    QCOMPARE(results.count(), 2);
    QCOMPARE(results.values().contains(test1), true);
    QCOMPARE(results.values().contains(test2), true);
    QCOMPARE(results.values().contains(test3), false);

    // Put an extra target on rerence maker1
    maker1->registerReference(test3, maker1);
    results = maker1->getAllReferencesFromSource(maker1);
    QCOMPARE(results.count(), 3);
    QCOMPARE(results.values().contains(test1), true);
    QCOMPARE(results.values().contains(test2), true);
    QCOMPARE(results.values().contains(test3), true);

    // Check the expected text format exists as a key with the
    // expected object (which were generated monotonically from ref 1)
    QCOMPARE(results.contains(QString("[TEST-1]")), true);
    QCOMPARE(results.value(QString("[TEST-1]")), test1);

    // Check the asOption option - the text needs to be correct in the key,
    // and have the correct object as the value

    results = maker1->getAllReferencesFromSource(maker1, true);
    QCOMPARE(results.contains(QString("[TEST-1] Test Data1")), true);
    QCOMPARE(results.value(QString("[TEST-1] Test Data1")), test1);
    QCOMPARE(results.contains(QString("[TEST-2] Test Data2")), true);
    QCOMPARE(results.value(QString("[TEST-2] Test Data2")), test2);
    QCOMPARE(results.contains(QString("[TEST-3] dummy")), true);
    QCOMPARE(results.value(QString("[TEST-3] dummy")), test3);
}

// ----------------------------------------------------------------------------
// Reference maker template tests
// ----------------------------------------------------------------------------

void CrossReferenceTests::test7XrefMakerMakeAndDeleteReference()
{
    init();
    // Start with no references - make them using the methods of
    // referring class
    maker1->makeReference(test1);
    maker2->makeReference(test1);
    maker2->makeReference(test2);
    auto testMap = maker1->getXRefMap();

    QCOMPARE(testMap->count(), 3);
    QCOMPARE(testMap->contains(test1, maker1), true);
    QCOMPARE(testMap->contains(test1, maker2), true);
    QCOMPARE(testMap->contains(test2, maker2), true);


    // test that deleting a maker removes the association
    delete maker2;
    maker2 = nullptr;
    QCOMPARE(testMap->count(), 1);
    QCOMPARE(testMap->contains(test1, maker2), false);
    QCOMPARE(testMap->contains(test2, maker2), false);

    // Make a reference using number alone
    Reference referenceTarget("TEST", 0, 1 );

    maker3->makeReference(referenceTarget);
    QCOMPARE(testMap->count(), 2);
    QCOMPARE(testMap->contains(test1, maker3), true);

    // delete reference
    maker3->deleteReference(test1);
    QCOMPARE(testMap->contains(test1, maker3), false);
    QCOMPARE(testMap->count(), 1);

    // Verify no issues with double delete
    maker3->deleteReference(test1);
    QCOMPARE(testMap->count(), 1);

    // Add more references for maker1, and verify no issues with double insert
    maker1->makeReference(test1);
    maker1->makeReference(test2);
    maker1->makeReference(test3);

    // Also verify the action of updateReferencesInModel
    QCOMPARE(maker1->data(2).toString(), QString("[TEST-1] [TEST-2] [TEST-3]"));
    maker1->deleteAllReferences();
    QCOMPARE(testMap->isEmpty(), true);
    QCOMPARE(maker1->data(2).toString(), QString());
}

void CrossReferenceTests::test8getReferencesMade()
{
    init();
    setUpFiveReferrers();
    auto results = maker1->getReferencesMade(true);
    QCOMPARE(results.contains(QString("[TEST-1] Test Data1")), true);
    QCOMPARE(results.value(QString("[TEST-1] Test Data1")), test1);
    QCOMPARE(results.contains(QString("[TEST-2] Test Data2")), true);
    QCOMPARE(results.value(QString("[TEST-2] Test Data2")), test2);

    // Verify text performance
    results = maker1->getReferencesMade();
    QCOMPARE(results.contains(QString("[TEST-1]")), true);

    maker1->makeReference(test3);
    results = maker1->getReferencesMade();
    QVector<TestXrefTarget*> expected = {test1, test2, test3};
    for (auto item : expected) {
        QCOMPARE(results.values().contains(item), true);
    }
}

void CrossReferenceTests::test9getReferenceTargets()
{
    // All targets means every reference that is in the target class
    init();
    setUpFiveReferrers();

    // Regardless of how many references are made by each Xref maker,
    // the number of options is simply the number of targets that exist
    auto results = maker1->getReferenceTargetOptions();
    QCOMPARE(results.count(), 3);

    results = maker1->getReferenceTargetOptions();
    QCOMPARE(results.count(), 3);

    // The target options will be returned in ascending order of reference no.
    auto expected = QList<TreeItem*>{test1, test2, test3};
    QCOMPARE(results.values(), expected);
}

void CrossReferenceTests::setUpFiveReferrers()
{
    // Register the same referrers under multiple targets
    maker1->makeReference(test1);
    maker2->makeReference(test1);
    maker3->makeReference(test1);
    maker1->makeReference(test2);
    maker2->makeReference(test2);

    QCOMPARE(testMap->count(), 5);
}

QTEST_MAIN(CrossReferenceTests)
#include "cross_reference_tests.moc"
