/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

// It will be necessary to test the actions of the functions

#include <QTest>
#include "treemodeltypes.h"
#include "treeitemtypes.h"
#include <QTreeView>

#include <QTemporaryDir>
#include <QTemporaryFile>

#include <QStringList>
#include <QDebug>

// We will inherit TreeModel so that we can test it's protected and private members
// (This is much more encapsulated than a friend declaration)
class ModelTest : public QObject
{
    Q_OBJECT

    int insertColumnsTestHelper(TreeItem *item, QVector<int> &noOfColumns);

private slots:
    // This also includes the core helper functions like getItem
    void testQAbstractItemModelOverrides();

    // this is for testing functions responsible for decoding saved data into the model,
    // as well as the parsing functions that e.g. convert references to integers
    void testLoadFunctions();
};

// The first test shall be to check that the model is instantiated with the correct number
// of columns, etc
void ModelTest::testQAbstractItemModelOverrides()
{
    // The tree model takes a QStringList of headers, and then just a massive string, which
    // is the data file.
    QStringList testHeaders{"Header 1", "Second Header", "Header Time"};
    QStringList level1Data;

    // Create a temporary file, open it, drop in the dummy file data, and close it ready for the model builder
    QTemporaryFile file;
    file.open();
    QTextStream fileStream(&file);
    QString dummyData("Losses\n"
                      "Hazards\n"
                      "Controls\n"
                      "  ObjectLevel1Item1\tL1_1_column2\tL1_1column3\n"
                      "    UnsafeControlAction\n"
                      "      TrueStatement1\n"
                      "      TrueStatement2\n"
                      "  ObjectLevel1Item2\nSecondObjectLevel1");
    fileStream << dummyData << endl;
    file.close();

    qInfo() << "Temporary test file name is " << file.fileName().toUtf8().data();
    // The item will receive a reference if a reference is not parsed, it will be created
    level1Data << "ObjectLevel1Item1" << "[C-1]" << "L1_1column3";

    auto *modelPointer = new TreeModelLegacy(testHeaders, file.fileName(), nullptr);

    QVector<QVariant> headers = { modelPointer->headerData(0, Qt::Horizontal),
                                  modelPointer->headerData(1, Qt::Horizontal),
                                  modelPointer->headerData(2, Qt::Horizontal) };

    auto listIterator = testHeaders.constBegin();
    for (const auto &header : headers) {
        QCOMPARE(header.toString(), *listIterator++);
    }

    // Get an index to the root item

    QModelIndex rootIndex = modelPointer->createIndex(0, 0, modelPointer->root);
    QVector<QModelIndex> rootIndices = { rootIndex,
                                         modelPointer->createIndex(0, 1, modelPointer->root),
                                         modelPointer->createIndex(0, 2, modelPointer->root)};

    // Verify the data() function
    listIterator = testHeaders.constBegin();
    for (const auto &indexForColumn : rootIndices) {
        QCOMPARE(modelPointer->data(indexForColumn, Qt::DisplayRole).toString(), *listIterator++);
    }

    // Use that index to pull some children in the model - check the data inside it is correct
    QModelIndex controlsIndex = modelPointer->index(2, 0, rootIndex);
    QVector<QModelIndex> controlIndices = { modelPointer->index(0, 0, controlsIndex),
                                          modelPointer->index(0, 1, controlsIndex),
                                          modelPointer->index(0, 2, controlsIndex)};
    listIterator = level1Data.constBegin();
    for (const auto &indexForColumn : controlIndices) {
        auto result = modelPointer->data(indexForColumn, Qt::DisplayRole).toString();
        auto expected = *listIterator++;
        QCOMPARE(result, expected);
    }

    // Insert columns - Here we aim to check that the correct number of columns
    // are inserted throughout the tree hierarchy
    QVector<int> noOfColumnsInTreeBefore;
    int noOfItemsBefore = insertColumnsTestHelper(modelPointer->getItem(QModelIndex()), noOfColumnsInTreeBefore);
    modelPointer->insertColumns(1, 3, QModelIndex());
    QVector<int> noOfColumnsInTreeAfter;
    int noOfItemsAfter = insertColumnsTestHelper(modelPointer->getItem(QModelIndex()), noOfColumnsInTreeAfter);

    // Check that the number of items in the tree has not changed - but each has 3 new columns
    QCOMPARE(noOfItemsBefore, noOfItemsAfter );
    auto iteratorColumnCountAfter = noOfColumnsInTreeAfter.constBegin();
    for (auto columnCountBefore : noOfColumnsInTreeBefore) {
        QCOMPARE(columnCountBefore + 3, *iteratorColumnCountAfter++ );
    }

    // There is no need to test the 'stem' specific functionality of this code
    // as that is covered under treeinsert.cpp

    auto controlsItem = modelPointer->getItem(controlsIndex);
    QCOMPARE(controlsItem->childCount(), 2);
    auto originalChildZero = controlsItem->child(0);
    auto originalChildOne = controlsItem->child(1);

    // Verify that the items around have / not moved
    modelPointer->insertRowByType(1, controlsIndex, "C");
    QCOMPARE(modelPointer->rowCount(controlsIndex), 3);
    QCOMPARE(controlsItem->child(0), originalChildZero);
    QCOMPARE(controlsItem->child(2), originalChildOne);

    // Verify the type inserted, should still be a 'control'
    QCOMPARE(controlsItem->child(1)->celltype(), "Control");

    // insertRows - similar method to above
    modelPointer->insertRows(1, 3, controlsIndex);
    QCOMPARE(modelPointer->rowCount(controlsIndex), 6);
    QCOMPARE(controlsItem->child(0), originalChildZero);
    QCOMPARE(controlsItem->child(5), originalChildOne);

    // Verify parent indices - the childIndices were created from the controlsIndex
    QCOMPARE(controlIndices[0].parent(), controlsIndex);

    // The model will not allow access to the root via the root index
    QCOMPARE(controlsIndex.parent(), QModelIndex());

    // We can now remove the columns that we inserted earlier in this test
    noOfColumnsInTreeAfter.clear();
    noOfColumnsInTreeBefore.clear();
    noOfItemsBefore = insertColumnsTestHelper(modelPointer->getItem(QModelIndex()), noOfColumnsInTreeBefore);
    modelPointer->removeColumns(1, 3);
    noOfItemsAfter = insertColumnsTestHelper(modelPointer->getItem(QModelIndex()), noOfColumnsInTreeAfter);

    QCOMPARE(noOfItemsBefore, noOfItemsAfter );
    iteratorColumnCountAfter = noOfColumnsInTreeAfter.constBegin();
    for (auto columnCountBefore : noOfColumnsInTreeBefore) {
        QCOMPARE(columnCountBefore - 3, *iteratorColumnCountAfter++ );
    }

    // Remove rows - should be able to check that everything goes back to where it was before
    QCOMPARE(modelPointer->rowCount(controlsIndex), 6);
    modelPointer->removeRows(1, 4, controlsIndex); // Previously 1 + 3 rows were inserted
    QCOMPARE(modelPointer->rowCount(controlsIndex), 2);
    QCOMPARE(controlsItem->child(0), originalChildZero);
    QCOMPARE(controlsItem->child(1), originalChildOne);

    // Set some data and read it back to verify that the composite function works
    // first, reset these childindices (after inserts and deletes, an index may not be valid)
    controlIndices = { modelPointer->index(0, 0, controlsIndex),
                     modelPointer->index(0, 1, controlsIndex)};
    QVariant data1 = QString("testData1");

    // check initial value
    auto value(modelPointer->data(controlIndices[0], Qt::DisplayRole));
    QCOMPARE(value, "ObjectLevel1Item1");
    modelPointer->setData(controlIndices[0], data1, Qt::EditRole);
    // check value after setData
    value = modelPointer->data(controlIndices[0], Qt::DisplayRole);
    QCOMPARE(value, data1);

    // For the column 1 check, we need a linkable class. ControlsIndex refers to the
    // banner 'Controls', whereas controlIndices[] are actual controls. Beneath them are
    // Unsafe control actions, and after that True Statments. This is the first
    // linkable class in the hierarchy

    // First, get an index and ensure it is the correct type.
    auto ucaIndex = controlIndices[0].child(0, 0);
    auto trueIndex0 = ucaIndex.child(0, 1);
    auto trueIndex1 = ucaIndex.child(1, 1);
    QCOMPARE(modelPointer->data(trueIndex0, Qt::DisplayRole), "[TRUE-1]");
    QCOMPARE(modelPointer->data(trueIndex1, Qt::DisplayRole), "[TRUE-2]");

    // The next job is to insert a new true statement that is linked to another
    // Insert it: position 2, 1 off, 3 columns, TRUE stem, reference 1
    modelPointer->getItem(ucaIndex)->insertChildren(2, 1, 3, "TRUE", 1);
    // Check the reference number came out correctly
    auto trueIndex2 = ucaIndex.child(2, 1);
    QCOMPARE(modelPointer->data(trueIndex2, Qt::DisplayRole), "[TRUE-1]");

    // Now change the data with set data; check that BOTH values update
    QVariant data2 = QString("[TRUE-3]");
    modelPointer->setData(trueIndex0, data2, Qt::EditRole);
    QCOMPARE(modelPointer->data(trueIndex0, Qt::DisplayRole), "[TRUE-3]");
    QCOMPARE(modelPointer->data(trueIndex2, Qt::DisplayRole), "[TRUE-3]");

    // Check that the edit role is required
    QCOMPARE(modelPointer->setData(trueIndex0, "[TRUE-4]", Qt::DisplayRole), false);
    QCOMPARE(modelPointer->data(trueIndex0, Qt::DisplayRole), "[TRUE-3]");

    // Finally, use the setHeaderData function to write banner, and then
    // read it back
    QVariant newData(QString("New Header"));
    modelPointer->setHeaderData(0, Qt::Horizontal, newData, Qt::EditRole);
    auto headerDataSet = modelPointer->headerData(0, Qt::Horizontal);
    QCOMPARE(headerDataSet, newData);

    delete modelPointer;
    modelPointer = nullptr;

}

void ModelTest::testLoadFunctions()
{

}

// This function iterates the tree, calling each of its children. It pushes the number of columns
// for the item into a vector as it goes. Each child reports the number of children (including itself),
// when it returns - thus if it is called on root it reports the total number of items in the tree.
int ModelTest::insertColumnsTestHelper(TreeItem *item, QVector<int> &noOfColumns)
{
    int newItems = 1;
    noOfColumns.push_back(item->columnCount());
    for (int i = 0; i < item->childCount(); ++i) {
        newItems += insertColumnsTestHelper(item->child(i), noOfColumns);
    }
    return newItems;
}

QTEST_MAIN(ModelTest)
#include "model_tests.moc"
