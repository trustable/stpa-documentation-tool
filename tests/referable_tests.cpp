/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include <QTest>
#include "treeitem.h"
#include <memory>
#include "referenceitem.h"

using ReferenceMap = TreeItem::ReferenceMap;

// In order to test the ReferableItem a test surrogate must be invoked

// The pattern is curious template, ie: T : Referableitem<T>

// This type of class would normally contain the overrides used in other base classes,
// however, we only need to include the overrides / functions required by
// ReferableItem<T>

class TestClass : public ReferenceItem
{
public:
    // We can permit a default unique ID which would normaly be supplied by
    // the insertChildren function
    TestClass(const QVector<QVariant> &data, TreeItem *parent,
              int uniqueID = 0, int reference = 0);

    static QString classReferenceStem() {return "TEST-";}
    static QString classReferenceType() {return "TEST";}
    static bool classReferencesParent() {return false;}

    bool isLinkable() override;
    static inline bool staticLinkability = false;
    static bool classIsLinkable() {return staticLinkability;}
    static void setLinkability(bool linkability) {staticLinkability = linkability;}
    QString getMarkdownFormatString() const override {return markdownPrefix;}

private:

    static inline QString markdownPrefix
                    = QString("### ");
};

bool TestClass::isLinkable() {return classIsLinkable();}

// Constructor follows standard pattern used by all derived classes
TestClass::TestClass(const QVector<QVariant> &data, TreeItem *parent,
                     int uniqueID, int reference)
        : TreeItem(data, parent, uniqueID),
          ReferenceItem(data, classReferenceType(), parent,
                        reference, classIsLinkable(), false)
{
    // Nothing to do
}


class ReferableTests : public QObject
{
    Q_OBJECT

private:
    QVector<QVariant> data {0, 0, 0, 0};
    TreeItem * root = nullptr;

    // Include some smart pointers for tracking objects
    TestClass *test1 = nullptr;
    TestClass *test2 = nullptr;
    TestClass *test3 = nullptr;
    QVector<QVariant> data1;
    QVector<QVariant> data2;
    QVector<QVariant> data3;

    void setUpThreeVectors();

    void init();
private slots:
    void test1ReferenceNumberCreations();
    void test2NumberCreationOutlayerCases();
    void test3TestRemovalOnDelete();
    void test4GetReferenceDetails();
    void test5SetReferenceNumber();
    void test6SetDataLinkage();
    void test7RenumberReference();
    void test8SeverLinkTests();
};

void ReferableTests::init() {
    delete root;
    root = new TreeItem(data, nullptr);
}

void ReferableTests::test1ReferenceNumberCreations()
{
    TestClass::setLinkability(false);
    init();

    // First test the empty case with reference number zero
    TestClass test1(data, root);
    QCOMPARE(test1.getReferenceNumber(), 1);
    QCOMPARE(test1.getReference().type, QString("TEST"));
    QCOMPARE(test1.getReference().parentNumber, 0);
    QCOMPARE(test1.getReference().number, 1);
    QCOMPARE(test1.getReferenceMap()->count(), 1);

    // Verify that numbering carries on sequentially
    TestClass test2(data, root);
    TestClass test3(data, root);

    QCOMPARE(test1.getReferenceMap()->count(), 3);
    QCOMPARE(test2.getReferenceNumber(), 2);
    QCOMPARE(test3.getReferenceNumber(), 3);

    // Verify that the multiple entries with the same number are not allowed
    // in a non-linkable class
    TestClass test4(data, root, 0, 3);
    QCOMPARE(test4.getReferenceNumber(), 4);

    // .. when linkable multiple entries are allowed
    TestClass::setLinkability(true);
    TestClass test5(data, root, 0, 3);
    QCOMPARE(test5.getReferenceNumber(), 3);

    TestClass::setLinkability(false);
    // Insert a value of higher than maxStdMapIndex
    TestClass test6(data, root, 0, 10001);
    QCOMPARE(test6.getReferenceNumber(), 10001);

    // Verify that the lowest available number <=10000 is found
    TestClass test7(data, root, 0, 3);
    QCOMPARE(test7.getReferenceNumber(), 5);

    // Allowed:
    TestClass test8(data, root, 0, 10000);
    QCOMPARE(test8.getReferenceNumber(), 10000);

    // New default numbers now have to go in the 'special' range
    TestClass test9(data, root);
    QCOMPARE(test9.getReferenceNumber(), 10002);

}

void ReferableTests::test2NumberCreationOutlayerCases()
{
    // Tests insertation when only special > max references exist
    // If the number attempted was > 10000, it should be added to the end
    //  in the non-linkable case

    TestClass::setLinkability(false);
    init();

    TestClass test1(data, root, 0, 10001);
    QCOMPARE(test1.getReferenceNumber(), 10001);

    TestClass test2(data, root, 0, 10001);
    QCOMPARE(test2.getReferenceNumber(), 10002);

    TestClass test3(data, root, 0, 10001);
    QCOMPARE(test3.getReferenceNumber(), 10003);

    // But otherwise, it becomes the new first value
    TestClass test4(data, root);
    QCOMPARE(test4.getReferenceNumber(), 1);

}

void ReferableTests::test3TestRemovalOnDelete()
{
    init();
    // Create an item
    auto test1 = std::make_unique<TestClass>(data, root);

    ReferenceMap *map = test1->getReferenceMap();

    // Check it's there
    QCOMPARE(map->isEmpty(), false);

    // Check it's gone
    test1.reset();
    QCOMPARE(map->isEmpty(), true);
}

void ReferableTests::test4GetReferenceDetails()
{
    init();

    auto test1 = std::make_unique<TestClass>(data, root);
    QCOMPARE(test1->getReferenceAsString(), QString("[TEST-1]"));
    QCOMPARE(test1->getReferenceAsString(false), QString("TEST-1"));
    QCOMPARE(test1->getReferenceStem(), QString("TEST-"));

    // Test application of data and retrieval as 'option text'
    test1->TreeItem::setData(0, QString("Test Text"));
    QCOMPARE(test1->getReferenceAsOption(), QString("[TEST-1] Test Text"));

}

void ReferableTests::test5SetReferenceNumber()
{
    init();

    // This is used by constructor, link, unlink, renumber - friend used to call
    auto test1 = std::make_unique<TestClass>(data, root);
    int number = test1->getReferenceNumber();
    QCOMPARE(number, 1);
    test1->setItemReferenceNumber(10);

    number = test1->getReferenceNumber();
    QCOMPARE(number, 10);
    QString reference = test1->getReferenceAsString();
    QString expected("[TEST-10]");
    QCOMPARE(reference, expected);
    QCOMPARE(test1->data(1).toString(), expected);

}

void ReferableTests::test6SetDataLinkage()
{
    TestClass::setLinkability(true);
    setUpThreeVectors();

    QCOMPARE(test1->isLinked(), false);
    QCOMPARE(test2->isLinked(), false);
    QCOMPARE(test3->isLinked(), false);

    // Now test linkage - check that the item being linked item adopts the others text
    test2->createLink(test1);
    QCOMPARE(test2->getReferenceNumber(), 1);
    QCOMPARE(test2->data(0), data1[0]);

    QCOMPARE(test1->isLinked(), true);
    QCOMPARE(test2->isLinked(), true);
    QCOMPARE(test3->isLinked(), false);

    // Apply data to second, and check that the first changes
    QString newValue("New value");
    test2->setData(0, newValue);
    QCOMPARE(test1->data(0), newValue);

    // Add item three to the group by linking on item 2, but them remove item 2
    test3->createLink(test2);
    test2->severLink();

    QCOMPARE(test1->isLinked(), true);
    QCOMPARE(test2->isLinked(), false);
    QCOMPARE(test3->isLinked(), true);

    // Test2 had a value prior to linkage, so it will revert to it, 3 adopts newValue
    QCOMPARE(test1->data(0), newValue);
    QCOMPARE(test2->data(0), data2[0]);
    QCOMPARE(test3->data(0), newValue);

    // Use this data to test getReferencesInThisClass
    QString option1 = "[TEST-1] " + newValue;
    QString option2 = "[TEST-2] " + data2[0].toString();

    // The options in the map will be unique - new inserts replace old ones - but maps return
    // the latest insert first - so the last item in 'existing' is the oldest with that key
    // and is inserted into the optionsMap most recently
    OptionsMap expected {{option1, test1}, {option2, test2}};
//    QCOMPARE(test1->getReferencesInThisClass(), expected);
}

void ReferableTests::test7RenumberReference()
{
    // Because of the existance of linkage, a renumber command actually
    // renumbers every item with the same original number
    TestClass::setLinkability(true);
    setUpThreeVectors();
    test3->createLink(test1);
    test1->renumberReference(4);
    QCOMPARE(test1->getReferenceNumber(), 4);
    QCOMPARE(test3->getReferenceNumber(), 4);
    QCOMPARE(test2->getReferenceNumber(), 2);

    // The ordering of the items in the parent should now also be changed
    QCOMPARE(root->child(0)->getReferenceNumber(), 2);
    QCOMPARE(root->child(1)->getReferenceNumber(), 4);
    QCOMPARE(root->child(2)->getReferenceNumber(), 4);

    // Sever the link on 3, it adopts its previous number since it is free
    test3->severLink();
    QCOMPARE(test3->getReferenceNumber(), 3);
    test2->renumberReference(7);

    QCOMPARE(test1->getReferenceNumber(), 4);
    QCOMPARE(test2->getReferenceNumber(), 7);
    QCOMPARE(test3->getReferenceNumber(), 3);

    // Test2, Test3 are now out-of-sequence from the start, and will remain so
    //  - the renumber function slides the items down into consecutive order
    //    from what they are numbered now, not what they started off as.
    test1->renumberAllReferences();

    // Object ordering is unambiguous because each has a different number
    QCOMPARE(root->child(0), test3);
    QCOMPARE(root->child(1), test1);
    QCOMPARE(root->child(2), test2);

    QCOMPARE(test3->getReferenceNumber(), 1);
    QCOMPARE(test1->getReferenceNumber(), 2);
    QCOMPARE(test2->getReferenceNumber(), 3);
}

void ReferableTests::test8SeverLinkTests()
{
    setUpThreeVectors();
    // If the previous reference number that an object had prior to linkage is
    // still available, then the data and the reference number are restored on sever.
    // If not -> for consistency the data is reverted to blank and a new number assigned
    test3->createLink(test1);
    QCOMPARE(test3->data(0), data1[0]);

    // The original ref num is available, so the number and data revert
    test3->severLink();
    QCOMPARE(test3->data(0), data3[0]);
    QCOMPARE(test3->getReferenceNumber(), 3);

    test3->createLink(test1);
    QCOMPARE(test3->data(0), data1[0]);

    // Take up the reference number 3
    QCOMPARE(test2->renumberReference(3), true);

    test3->severLink();
    QCOMPARE(test3->data(0), QString("[No data]"));
    QCOMPARE(test3->getReferenceNumber(), 4);

}

void ReferableTests::setUpThreeVectors()
{
    // deleting root actually deletes test1, test2, test3 in this case
    delete root;

    root = new TreeItem(data, nullptr);

    // Set up three objects, non of them linked to start with
    data1 = {QString("First"), QString(), 0, 0};
    data2 = {QString("Second"), QString(), 0, 0};
    data3 = {QString("Third"), QString(), 0, 0};

    test1 = new TestClass(data1, root);
    test2 = new TestClass(data2, root);
    test3 = new TestClass(data3, root);

    root->insertSpecial(test1);
    root->insertSpecial(test2);
    root->insertSpecial(test3);

    QCOMPARE(test1->getReferenceNumber(), 1);
    QCOMPARE(test2->getReferenceNumber(), 2);
    QCOMPARE(test3->getReferenceNumber(), 3);
}

QTEST_MAIN(ReferableTests)
#include "referable_tests.moc"
