﻿/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include <QTest>
#include "treeitem.h"
#include <memory>

#include "factory.h"

// forward declarations for unit test types
class TestItemLevel1;
class TestItemLevel2;
class TestItemLevel3;

class TreeItemTests : public QObject
{
    Q_OBJECT

private:
    void initialisation();
    void populateTestTree();
    QVector<QVariant> data {0, 0, 0, 0};
    TreeItem * root = nullptr;
    TreeItem * layerOneOne = nullptr;
    TreeItem * layerTwoOne = nullptr;
    TreeItem * layerTwoTwo = nullptr;
    TreeItem * layerThreeOne = nullptr;

private slots:
    void testUniqueIds();
    void childFunctions();
    void removeVsDelete();
    void columnTests();
    void dataTests();
    void saveFileFunctions();
    void markdownTests();
    void referenceRecursionTest();
    void insertSpecialTest();
    void testSortFunction();
    void testCheckHasContents();
};

// -----------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------

// TEST ITEMS: these items exist only to facilitate unit testing
//  - the TreeItem .cpp functionality will be tested on this simple hierarchy
// -----------------------------------------------------------------------------------

class TestItemLevel1 : public TreeItem
{
public:
    TestItemLevel1(const QVector<QVariant> &data, TreeItem *parent)
        : TreeItem(data, parent) {}
    QString getMarkdownFormatString() const override;
};

QString TestItemLevel1::getMarkdownFormatString() const {return "#Level1: ";}
// -----------------------------------------------------------------------------------

class TestItemLevel2 : public TreeItem
{
public:
    TestItemLevel2(const QVector<QVariant> &data, TreeItem *parent)
        : TreeItem(data, parent) {}
    QString getMarkdownFormatString() const override;

    // Introduced to test generic functionality of treeitem.cpp
    bool isReferenceType() const override {return true;}
    QString getReferenceStem() const override {return QString("ALPHA-");}
    int getReferenceNumber() const override {return referenceNumber;}
    void setReferenceNumber(int number) {referenceNumber = number;}
private:
    int referenceNumber = 0;
};

QString TestItemLevel2::getMarkdownFormatString() const
{
    return "##Level2: ";
}

// -----------------------------------------------------------------------------------

class TestItemLevel3 : public TreeItem
{
public:
    TestItemLevel3(const QVector<QVariant> &data, TreeItem *parent)
        : TreeItem(data, parent) {}
    QString getMarkdownFormatString() const override;

    // Introduced to test generic functionality of treeitem.cpp
    bool isReferenceType() const override {return true;}
    QString getReferenceStem() const override {return QString("BETA-");}
    int getReferenceNumber() const override {return referenceNumber;}
    void setReferenceNumber(int number) {referenceNumber = number;}
private:
    int referenceNumber = 0;

};

QString TestItemLevel3::getMarkdownFormatString() const {return "###Level3: ";}

// -----------------------------------------------------------------------------------
// UNIT TESTS
// -----------------------------------------------------------------------------------

void TreeItemTests::testUniqueIds()
{
    initialisation();
    // Check that new items are given unique IDs
    auto * item1 = new TreeItem(data, root);
    auto * item2 = new TreeItem(data, root);
    QCOMPARE(root->getUniqueID(), 0);
    QCOMPARE(item1->getUniqueID(), 1);
    QVERIFY(item1->getUniqueID() != item2->getUniqueID());

    // Check a unique ID cannot be overwritten
    auto * item3 = new TreeItem(data, root, 1);
    QCOMPARE(item3->getUniqueID(), 3);

    // Check that once an ID is erased, it can be reused
    delete item1;
    auto * item4 = new TreeItem(data, root, 1);
    QCOMPARE(item4->getUniqueID(), 1);
    delete item2;
    delete item3;
    delete item4;
}

void TreeItemTests::childFunctions()
{
    initialisation();
    root->insertChildren(0, 1, 3);
    // Check we can retrieve the new child, and it has no children yet
    TreeItem * item1 = root->child(0);
    QCOMPARE(root->childCount(), 1);
    QCOMPARE(item1->childCount(), 0);
    QCOMPARE(item1->columnCount(), 3);

    // Check insert actually inserts, and doesn't overwrite
    QCOMPARE(root->insertChildren(0, 2, 4), true);
    QCOMPARE(root->childCount(), 3);
    QCOMPARE(root->child(2), item1);
    QCOMPARE(item1->childNumber(), 2);
    QCOMPARE(root->child(0)->columnCount(), 4);

    // Negative test on insert - cannot insert /delete at non-existant position
    QCOMPARE(root->insertChildren(10, 1, 1), false);
    QCOMPARE(root->removeChildren(10, 1), false);

    // Check parent
    QCOMPARE(root->child(0)->parent(), root);

    QCOMPARE(root->removeChildren(0, root->childCount()), true);
    QCOMPARE(root->childCount(), 0);
}

void TreeItemTests::removeVsDelete()
{
    initialisation();
    TreeItem * item1 = new TreeItem(data, root);

    // Just because the item is parented on root does not mean that root
    // has inserted it - parent controls the insertation
    QCOMPARE(root->childCount(), 0);
    delete item1;

    // position 0, 1 child, 3 columns
    root->insertChildren(0, 1, 3);
    item1 = root->child(0);

    // position 0, 2 children, each with 3 columns
    item1->insertChildren(0, 2, 3);
    QCOMPARE(item1->childCount(), 2);

    TreeItem * child2 = item1->child(1);

    // Test remove child, check parent has correct item at top of list
    item1->removeChildren(0, 1);
    QCOMPARE(item1->childCount(), 1);
    QCOMPARE(item1->child(0), child2);

    // Test parent update on item direct delete
    delete child2;
    QCOMPARE(item1->childCount(), 0);

    delete item1;
    QCOMPARE(root->childCount(), 0);
}

void TreeItemTests::columnTests()
{
    // The insert columns function only inserts columns for itself and children
    initialisation(); // root starts off with 4 columns
    root->insertChildren(0, 2, 3); // Children only have 3
    root->insertColumns(0, 4);

    TreeItem * child1 = root->child(0);
    TreeItem * child2 = root->child(1);

    // Negative test
    QCOMPARE(child1->insertColumns(10, 1), false);

    // Check column numbers are correct
    QCOMPARE(root->columnCount(), 8);
    QCOMPARE(child1->columnCount(), 7);

    // Negative test
    QCOMPARE(child1->removeColumns(10, 1), false);

    // over removal - results in no columns removed from child1
    child2->insertColumns(0, 5);
    root->removeColumns(0, 8);
    QCOMPARE(root->columnCount(), 0);
    QCOMPARE(child1->columnCount(), 7);
    QCOMPARE(child2->columnCount(), 4);
}


void TreeItemTests::dataTests()
{
    initialisation();
    // Set data as int
    for (int i = 0; i < root->columnCount(); ++i) {
        // Implicit conversion from int to QVariant
        QCOMPARE(root->setData(i, i), true);
    }
    // Negative test
    QCOMPARE(root->setData(4, 0), false);

    // test storage of ints
    for (int i = 0; i < root->columnCount(); ++i) {
        QCOMPARE(root->data(i).toInt(), i);
    }
    QString testString("test string");
    root->setData(0, testString);
    QCOMPARE(root->data(0).toString(), testString);
}

void TreeItemTests::saveFileFunctions()
{
    // This will require a string list to be created for the relevant tree
    // - this is not the file format, the point here is to test the function
    // and not the integration - the purpose of the underlying function is
    // to indent based on child level
    QStringList expectedOutput{"DataLevel1\t[L-1-1]\t1",
                               "    DataLevel2\t[L-2-1]\t2",
                               "    DataLevel2\t[L-2-2]\t3",
                               "        DataLevel3\t[L-3-1]\t4"};
    populateTestTree();

    QStringList results = root->convertTreeToStringList();
    QCOMPARE(results, expectedOutput);

}

void TreeItemTests::markdownTests()
{
    populateTestTree();
    // The format this time will will insert newline characters before and aft, with the data
    // prefixed by a format string. However, in the default case, the format string is empty
    QStringList expectedOutput{"\n#Level1: DataLevel1\n",
                               "\n##Level2: DataLevel2\n",
                               "\n##Level2: DataLevel2\n",
                               "\n###Level3: DataLevel3\n"};
    QStringList results = root->convertTreeToMarkdownList();
    QCOMPARE(results, expectedOutput);
}

void TreeItemTests::referenceRecursionTest()
{
    // All items in the tree are recursed, but only those with 'isReferenceType' are returned in 'tree order'
    populateTestTree();
    QList<TreeItem*> testResults;
    QList<TreeItem*> expectedResults{layerTwoOne, layerTwoTwo, layerThreeOne};
    root->recurseTreeToReferencingList(testResults);
    QCOMPARE(testResults, expectedResults);
}

void TreeItemTests::insertSpecialTest()
{
    // Check that an item is added to the end of the list
    initialisation();
    TreeItem * testChild = new TestItemLevel1(data, root);
    root->insertSpecial(testChild);
    QCOMPARE(root->child(0), testChild);
}

void TreeItemTests::testSortFunction()
{
    // In this test, all items with stem ALPHA- should be placed in front of all items with
    // stem BETA-, but ALPHA-1 and ALPHA-2 should be placed before ALPHA-10.
    // The sort function is neither lexical nor numeric, but rather a combination of the two.
    initialisation();

    // TestItemLevel2 returns the stem 'ALPHA-'
    TestItemLevel2 *alpha1 = new TestItemLevel2(data, root);
    alpha1->setReferenceNumber(1);
    TestItemLevel2 *alpha2 = new TestItemLevel2(data, root);
    alpha2->setReferenceNumber(2);
    TestItemLevel2 *alpha10 = new TestItemLevel2(data, root);
    alpha10->setReferenceNumber(10);

    // TestItemLevel3 returns the stem 'BETA-'
    TestItemLevel3 *beta1 = new TestItemLevel3(data, root);
    beta1->setReferenceNumber(1);
    TestItemLevel3 *beta2 = new TestItemLevel3(data, root);
    beta2->setReferenceNumber(2);
    TestItemLevel3 *beta10 = new TestItemLevel3(data, root);
    beta10->setReferenceNumber(10);

    // Insert the objects in a transparently out-of-sequence order
    root->insertSpecial(beta10);
    root->insertSpecial(alpha1);
    root->insertSpecial(beta2);
    root->insertSpecial(beta1);
    root->insertSpecial(alpha10);
    root->insertSpecial(alpha2);

    // Initial ordering
    QVector<TreeItem*> incorrectOrder{beta10, alpha1, beta2, beta1, alpha10, alpha2};

    // How they should end up
    QVector<TreeItem*> expectedOrder{alpha1, alpha2, alpha10, beta1, beta2, beta10};

    // Verify that the order is, in fact, initially incorrect
    QCOMPARE(root->childCount(), 6);
    for (int i = 0; i < root->childCount(); ++i) {
        QCOMPARE(root->child(i), incorrectOrder.at(i));
    }

    // Sort them and verify that the order is now correct
    root->sortChildrenByReferenceNumbers();
    QCOMPARE(root->childCount(), 6);
    for (int i = 0; i < root->childCount(); ++i) {
        QCOMPARE(root->child(i), expectedOrder.at(i));
    }
}

void TreeItemTests::testCheckHasContents()
{
    // set up 5 items
    QVector<TreeItem*> testItems;

    // Any string that is in emptyMarkers should return as 'hasContents == false'
    for (auto string : TreeItem::emptyMarkers) {
        QVector<QVariant> testData {string, 0, 0, 0};
        auto testItem = std::make_unique<TreeItem>(testData, root);
        QCOMPARE(testItem->checkHasContents(), false);
    }

    // But strings with extra contents should not
    for (auto string : TreeItem::emptyMarkers) {
        string += " Something good";
        QVector<QVariant> testData {string, 0, 0, 0};
        auto testItem = std::make_unique<TreeItem>(testData, root);
        QCOMPARE(testItem->checkHasContents(), true);
    }
}


// -----------------------------------------------------------------------------------
// Test helper functions
// -----------------------------------------------------------------------------------

void TreeItemTests::initialisation()
{
    delete root;
    root = new TreeItem(data, nullptr);
    Factory::factory()->registerType<TreeItem>();
}

void TreeItemTests::populateTestTree() {
    // Create a tree - it will be filled with items inheriting from TreeItem, as
    // in the actual application - the core functionality should be proven to work
    // regardless of the use of inheritance
    if (root)
        delete root;

    // Synthetically build up a tree without reference to the insert functions or
    // class hierarchy - data will be two elements wide
    QVector<QVariant> data {0, 0};
    root = new TreeItem(data, nullptr);
    layerOneOne = new TestItemLevel1(data, root);
    layerTwoOne = new TestItemLevel2(data, layerOneOne);
    layerTwoTwo = new TestItemLevel2(data, layerOneOne);
    layerThreeOne = new TestItemLevel3(data, layerTwoTwo);

    root->insertSpecial(layerOneOne);
    layerOneOne->insertSpecial(layerTwoOne);
    layerOneOne->insertSpecial(layerTwoTwo);
    layerTwoTwo->insertSpecial(layerThreeOne);

    // Put data in the tree items
    layerOneOne->setData(0, "DataLevel1");
    layerTwoOne->setData(0, "DataLevel2");
    layerTwoTwo->setData(0, "DataLevel2");
    layerThreeOne->setData(0, "DataLevel3");

    layerOneOne->setData(1, "[L-1-1]");
    layerTwoOne->setData(1, "[L-2-1]");
    layerTwoTwo->setData(1, "[L-2-2]");
    layerThreeOne->setData(1, "[L-3-1]");
}

QTEST_MAIN(TreeItemTests)
#include "tree_item_tester.moc"
