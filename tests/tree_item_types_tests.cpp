/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

// The purpose of these tests is to check for accidental changes to the key parts of the type
// interface, not to burden changes to the format with extra work.
// Also tested is the uniformity of the interface

#include <QTest>
#include "treeitemtypes.h"

#include <QVector>
#include <QVariant>
#include <QDebug>

QVector<QVariant> blankData = { 0, 0, 0, 0 };

class TreeItemTypesTests: public QObject
{
    Q_OBJECT

private slots:

    void test1();
};

void TreeItemTypesTests::test1()
{
    // Get some items - one of each non-root type
    Loss itemLoss(blankData, nullptr, 0);
    Hazard itemHazard(blankData, nullptr, 0);
    UCA itemUCA(blankData, nullptr, 0);
    ControlAction itemControlAction(blankData, nullptr, 0);
    TrueStatement itemTrueStatement(blankData, nullptr, 0);
    Belief itemBelief(blankData, nullptr, 0);
    Cause itemCause(blankData, nullptr, 0);
    InformationReceived itemInformationReceived(blankData, nullptr, 0);
    HowThisCouldOccur itemHowThisCouldOccur(blankData, nullptr, 0);
    Requirement itemRequirement(blankData, nullptr, 0);
    RootItem<Loss> itemRootLoss(blankData, nullptr, 0);
    RootItem<Hazard> itemRootHazard(blankData, nullptr, 0);
    RootItem<Control> itemRootControl(blankData, nullptr, 0);

    QVector<TreeItem*> testItems = {  &itemRootLoss,
                                      &itemRootHazard,
                                      &itemRootControl,
                                      &itemLoss,
                                      &itemHazard,
                                      &itemUCA,
                                      &itemControlAction,
                                      &itemTrueStatement,
                                      &itemBelief,
                                      &itemCause,
                                      &itemInformationReceived,
                                      &itemHowThisCouldOccur,
                                      &itemRequirement};

    // We will now test the fundamental properties of these item types
    QVector<bool> expectedDeletable = { false, false, false, true, true, true, true, true, true, true, true, true, true };

    QVector<bool> expectedIsControlAction = { false, false, false, false, false, true, true, false, false, false, false, false, false };

    QVector<bool> expectedIsReferenceType = { false, false, false, true, true, true, true, true, true, true, true, true, true };

    QVector<bool> expectedIsLinkable = { false, false, false, false, false, false, false, true, true, true, true, true, true, true};

    QVector<bool> isReferencingItem = { false, false, false, false, true, true, true, true, true, true, true, true, true};

    for (int i = 0; i < testItems.count(); ++i) {
        qInfo() << "Test item type (index " << i << "): " << testItems.at(i)->celltype();
        QCOMPARE(testItems.at(i)->deleteable(), expectedDeletable.at(i));
        QCOMPARE(testItems.at(i)->getIsControlAction(), expectedIsControlAction.at(i));
        QCOMPARE(testItems.at(i)->isReferenceType(), expectedIsReferenceType.at(i));
        QCOMPARE(testItems.at(i)->isLinkable(), expectedIsLinkable.at(i));
        QCOMPARE(testItems.at(i)->isReferencingItem(), isReferencingItem.at(i));
    }
}


QTEST_MAIN(TreeItemTypesTests)
#include "tree_item_types_tests.moc"
