/*
 * BSD 3-Clause License
 *
 * Copyright (C) 2019 Codethink Ltd.
 *
 * All rights reserved.
 *
 * Please refer to the LICENSE file for a full description of licensing terms of this software.
 *
*/

#include <QTest>
#include <QVector>
#include <QVariant>

#include "factory.h"

// ----------------------------------------------------------------------------
// These tests are updated from a time when the insertion functionality was
// handled by a template class. This functionality is now in the base and
// Factory classes. The functionality is the same, only now it is handled with
// variables found in the derived classes.

// For a Class T to work with the factory it must simply supply (static) functions
// 1) T::manufacture
// 2) T:classReferenceType
// ----------------------------------------------------------------------------


// This class inserts nothing - the non-inserting functionality
// must be tested
class TestClass1 : public TreeItem
{
public:
    TestClass1(const QVector<QVariant> &data, TreeItem *parent,
                               int uniqueID, int reference = 0)
            :  TreeItem(data, parent, uniqueID)
    {
        // Nothing to do
        Q_UNUSED(reference);
    }

    QStringList insertsTypes() override {return QStringList();}
    QString defaultChildType() override {return QString();}

    static QString classReferenceStem() {return "TEST1-";}
    static QString classType() {return "TEST1";}
    static QString classCellType() {return QString("Test type 1");}
    QString celltype() override {return classCellType();}
};

// This class inserts the other test class - without reference to
// other types defined in the code base
class TestClass2 : public TreeItem
{
public:
    TestClass2(const QVector<QVariant> &data, TreeItem *parent,
               int uniqueID, int reference = 0)
            :  TreeItem(data, parent, uniqueID)
    {
        // Nothing to do
        Q_UNUSED(reference);
    }

    QStringList insertsTypes() override {return QStringList{"TEST1"};}
    QString defaultChildType() override {return QString("TEST1");}

    static QString classReferenceStem() {return "TEST2-";}
    static QString classType() {return "TEST2";}
    QString celltype() override {return QString("Test type 2");}
};

// ----------------------------------------------------------------------------


class TreeInsertTests : public QObject
{
    Q_OBJECT

private slots:
    void testBannerFunctions();
    void testInsertTypes();
    void testInsertChildren();
};

void TreeInsertTests::testBannerFunctions()
{
    // The parent functionality here depends on the parent sent in the constructor -
    // this does not require the insert function to be used. However, the parent
    // class must inherit from TreeItemInsert, otherwise the parental banner functionality
    // is not implemented (i.e. The virtual base function in TreeItem does not implement it)
    QVector<QVariant> data = { 0, 0, 0, 0};
    TestClass1 testObject1(data, nullptr, 1);
    TestClass1 testObject2(data, &testObject1, 2);
    TestClass1 testObject3(data, &testObject1, 3);

    TestClass1 testObject4(data, &testObject2, 4);

    // First, test the trivial set / get functions
    testObject1.setParentalBannerState(true);
    QCOMPARE(testObject1.getParentalBannerState(), true);

    testObject1.setParentalBannerState(false);
    QCOMPARE(testObject1.getParentalBannerState(), false);


    // Check functionality across children
    //void testInsertChildren();

    testObject2.setParentBannerState(true);
    QCOMPARE(testObject3.getParentBannerState(), true);
    testObject3.setParentBannerState(false);
    QCOMPARE(testObject2.getParentBannerState(), false);

    // ..Not not-children
    testObject4.setParentBannerState(true);
    QCOMPARE(testObject2.getParentBannerState(), false);

    // Test no-segfault on null
    testObject1.setParentBannerState(true);
    testObject1.getParentBannerState();
}

// ----------------------------------------------------------------------------

void TreeInsertTests::testInsertTypes()
{
    // Use nullptr in this test - the inserted type depends on the class
    // hierarchy, not the tree structure hierarchy
    QVector<QVariant> data = { 0, 0, 0, 0};
    TestClass1 testObject1(data, nullptr, 1);
    TestClass2 testObject2(data, nullptr, 2);
    QStringList listOfInsertedTypes1 = testObject1.insertsTypes();
    QStringList listOfInsertedTypes2 = testObject2.insertsTypes();

    // In the dummy class definitions, TestClass1 inserts void (nothing)
    // - whereas TestClass2 inserts TestClass1
    QCOMPARE(listOfInsertedTypes1.size(), 0);
    QCOMPARE(listOfInsertedTypes2[0], QString("TEST1"));
}

void TreeInsertTests::testInsertChildren()
{
    // For this we must ensure that the factory is working and the
    // test types are registered

    Factory::factory()->registerType<TestClass1>();
    Factory::factory()->registerType<TestClass2>();

    // nullptr: Working with class hierarchy, not tree structure
    QVector<QVariant> data1 = { QString("Initial") , 0, 0, 0};
    TestClass1 testObject1(data1, nullptr, -10);
    TestClass2 testObject2(data1, &testObject1, 1);

    // Verify the unique ids - cannot set zero, but it is assigned in an empty map
    QCOMPARE(testObject1.getUniqueID(), 0);
    QCOMPARE(testObject2.getUniqueID(), 1);

    // Child to test that no items are inserted
    QCOMPARE(testObject1.childCount(), 0);
    testObject1.insertChildren (0, 1, 4);
    QCOMPARE(testObject1.childCount(), 0);

    // Now check TestClass2 inserts TestClass1
    QCOMPARE(testObject2.childCount(), 0);
    testObject2.insertChildren (0, 1, 4, "TEST1"); // unique id 2
    QCOMPARE(testObject2.childCount(), 1);
    QCOMPARE(testObject2.child(0)->celltype(), QString("Test type 1"));

    // Insert an object at 0 again, then 1
    testObject2.insertChildren (0, 1, 4, "TEST1"); // unique id 3
    testObject2.insertChildren (1, 1, 4, "TEST1"); // unique id 4

    // So the objects' ids in the test object should be order 3, 4, 2
    QVector<int> idResults = {3, 4, 2};
    for (int i = 0; i < testObject2.childCount(); ++i) {
        QCOMPARE(testObject2.child(i)->getUniqueID(), idResults[i]);
    }
}


QTEST_MAIN(TreeInsertTests)
#include "treeinsert_tests.moc"
